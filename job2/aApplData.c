/* _HT */
/*#*****************************************************************#*/
/*#        File name   : %M%  aApplData.c                           #*/
/*#        Summary     :アプリケーション共通グローバルデータ        #*/
/*#        Author      :                                            #*/
/*#        Revision    : %I%                                        #*/
/*#        Date        : %D% %T% 23-AUG-1994                        #*/
/*#                                                                 #*/
/*#        Host        : nssun                                      #*/
/*#        Directory   : /home/Prober/code/appli                    #*/
/*#                                                                 #*/
/*#        Copyright (c) Tokyo Electron Limited.                    #*/
/*#                All rights reserved.                             #*/
/*#*****************************************************************#*/

/*#**************************************************************************#*/
/*# Modification:															 #*/
/*# V0.12: 16-OCT-1994 StartVerifyF 追加									 #*/
/*# ------- Ｒ００１ー０３以降 ---------------------------------------		 #*/
/*# V1.20: 12-DEC-1994 CFailStat 追加										 #*/
/*# V1.30: 18-Jan-1995 針先研磨用バッファー追加								 #*/
/*# ------- R003.02 以降 ---------------------------------------------		 #*/
/*# V1.31: 14-FEB-1995 インスペクション用アプリデータ追加					 #*/
/*# R3.05: 21-Feb-1995 セットアップモード用									 #*/
/*# ------- R003.04 以降 ---------------------------------------------		 #*/
/*# V1.34: 25-Feb-1995 ＩＤＣフラグ追加										 #*/
/*# ------- R004.05 以降 ---------------------------------------------		 #*/
/*# V4.00: 30-Mar-1995 ＰＭＩフラグ追加										 #*/
/*# V4.01: 03-Apr-1995 MakeMapF 追加										 #*/
/*# ------- R005.04 以降 ---------------------------------------------		 #*/
/*# V1.55: 30-Apr-1995 チップ指定ＰＭＩ追加									 #*/
/*# V1.56: 12-May-1995 ＰＭＩ実行ウェーハ間隔用カウンター追加				 #*/
/*# ------- R006.02 以降 ---------------------------------------------		 #*/
/*# V1.62: 23-May-1995 LotStartMsg[] 追加									 #*/
/*# R6.04: 29-May-1995 エージング用パラメータをコモンに移動 				 #*/
/*# R7.00: 31-May-1995 プローブファイルリードエリア							 #*/
/*# 	スキップファイルリードエリア追加									 #*/
/*# ------- R006.04 以降 --------------------------------------------		 #*/
/*# V1.65: 02-Jun-1995 ＩＤＩ用パラメータ追加								 #*/
/*# V1.66: 08-Jun-1995 チップ座標検索方向指定パラメータ追加					 #*/
/*#        10-Jun-1995 マークカウント処理フラグ追加							 #*/
/*#        10-Jun-1995 ＩＤＩタイムアップチェック用タイムワーク追加			 #*/
/*# ------- R007.05 以降													 #*/
/*# V1.75: 24-Jul-1995 ネットワークタスクとのコミュニケーションフラグ追加	 #*/
/*# R7.11: 09-Sep-1995 14:00 測定時間印字の為のエリア追加					 #*/
/*# V8.02: 21-Nov-1995 任意ウェーハ搬送処理追加								 #*/
/*# V8.33: 26-Feb-1996 ロットスタート時のアプリケーション用データ追加		 #*/
/*# V8.51: 05-Apr-1996 GP & RDP Task ID 格納変数追加						 #*/
/*# R9.00: 22-Apr-1996 12:00 includeにAPPL_wafTest.h追加					 #*/
/*# V9.00: 06-May-1996 サンプル測定用データ追加								 #*/
/*# R9.00: 07-May-1996 PciStatF追加											 #*/
/*# V9.01: 15-May-1996 CBlowCnt, WBlowCnt 追加								 #*/
/*# V9.01: 20-<ay-1996 BCFailStat 追加										 #*/
/*# R9.00N: 06-Jun-1996 スプーリング関係データ追加 kawaragi@tsp 			 #*/
/*# V9.22: 深澤:10-Jul-1996：ＩＮＴＥＬ関連データ追加						 #*/
/*# V9.23: 深澤:10-Jul-1996: ConNetFStat2追加								 #*/
/*# R9.00-11:shino 06-Aug-1996												 #*/
/*# 	カード、ウェーハブローの実行タイミングを修正Ｐ０２７４				 #*/
/*#        09-Aug-1996 ＧＰＩＢタスクからのメッセージを格納しておくエリア	 #*/
/*# 	StopGpMsg[]追加  M.Fukui(TSP)										 #*/
/*# R9.02M: 07-Oct-1996 MIP追加(TSP Takahashi)								 #*/
/*# R10.01:17-Oct-1996 shino PmiChipCount削除								 #*/
/*# R9.02:  11-Nov-1996 ContLotSpoolFlag, Sv1stLotName 追加 %6b11 hkaw		 #*/
/*# R10 %6b22mfuk AutoInkF,doAutoInk追加(ＩＤＣ自動インキング処理)			 #*/
/*# R9.02: %7116tara TSMC FTP 追加											 #*/
/*# R9.02G: 05-Feb-1997 NCRSD の カセット番号セーブエリア追加				 #*/
/*# R11.01 1997-Jul-03 : 由井 : @7703tyui-01								 #*/
/*# 	ＦＭＩマップデータファイル名追加									 #*/
/*# R10.02 : 1997-Jul-10 : 由井 : @7710tyui-05								 #*/
/*# 	三菱高知ロットエンドフラグ追加										 #*/
/*# R11.01 : 1997-Jul-16 : 田中（俊）@7716ttan-01							 #*/
/*# 	三星殿向けＩＤＩ実行ＧＰＩＢコマンド’ａＩＩ’						 #*/
/*# 	結果判定用フラグ追加												 #*/
/*# R11.01 : 1997-Jul-31 : 福井 : @7731mfuk									 #*/
/*# 	ＨＩＳＥＥ向け測定前のＰＭＩ機能追加								 #*/
/*# R11.01 : 1997-Jul-31 : 藤原 : @7731mfuj									 #*/
/*# 	東芝３色アラームモニタ機能追加										 #*/
/*# R10.04 : 1997-Jul-30 : 荒木 : @7730tara									 #*/
/*# 	ＦＭＩ：ＦＴＰパラメータ追加										 #*/
/*# R011.02: 1997-Oct-08 : 阿部 : @7a08habe-01								 #*/
/*# 	ＫＬＡ：CellSpoolMode 追加。										 #*/
/*# R011.01 : 1997-OCT-21 :  : @7a21hkaw									 #*/
/*# 	NetGatewayAddr[] 追加												 #*/
/*# R12.00 : 1997-Oct-29 : 藤原 : @7a29mfuj 								 #*/
/*# 	動パラ ランプコントロール追加										 #*/
/*# R011.05 : 1997-DEC-21 : Fukasawa  @7c21yfuk-01							 #*/
/*# 	delete ProbAreaData[], SkipAreaData[]								 #*/
/*# R012.02 : 1998-Feb-17 : M.Fukui :  @8217mfuk							 #*/
/*# 	Add : Contact check prior to test for NEC.							 #*/
/*# R012.03 : 1998-Apr-01 : H.Abe : @8401habe-01							 #*/
/*# 	For FUJITSU, Add DataLogModeF.										 #*/
/*# R012.03 : 1998-Apr-03 : M.Shinohara : @7818mshi-02						 #*/
/*#		Rewrote Comments.													 #*/
/*# R012.04-7 : 1998-Dec-11 : M.Fukui :  @8c11mfuk                           #*/
/*#     Add : Display Stop Menu After PMI Judgment Error for SONY.           #*/
/*# R011.10-2 : 1999-Feb-05 : J.Sato/TSP : @9205jsat-03                      #*/
/*#     HITACHI MIP, Inker Limit Warning Assist.                             #*/
/*# R012.09 : 1999-Mar-24 : T.Araki : @9324tara-01                           #*/
/*#     Added : GPIB 'p' command using polish wafer.                         #*/
/*# R012.09 : 1999-MAR-29 : H.Kawaragi : @9329hkaw-1334                      #*/
/*#     For TI, add received parameter by H command                          #*/
/*# R012.09 : 1999-Mar-30 : J.Sato/TSP : @9330jsat-1723                      #*/
/*#     Add TOSHIBA, GPIB C-Fail CheckBack                                   #*/
/*# Pxx00-R012.11 : 1999-May-11 : M.Shinohara : @9426mshi-347                #*/
/*#     Added Chuck Blow functionality for P-8XL SGS Catania.                #*/
/*#     Also added in Pap00-R011.13-3,Pap00-R012.05-9.                       #*/
/*# R012.05 : 1999-May-28 : M.Fukui : @9528mfuk                              #*/
/*#     Add : STKeyChange flag for KLA.                                      #*/
/*# R012.11 : 1999-Jun-23 : J.Sato/TSP : @9623jsat-0820                      #*/
/*#     For AMD, Continuity Plate Check End Flag                             #*/
/*# R012.07-6 : 1999-Jun-25 : T.Matsuzawa : @9625tmat-899                    #*/
/*#     Add FtpLogUpdir for Siemens network function                         #*/
/*# R012.12 : 1999-Sep-17 : M.Fukui : @9917mfuk-563                          #*/
/*#     Added PmiUnldStopF for PMI STEP2 ( Special Option ).                 #*/
/*# R012.11-x : 1999-Nov-11 : J,Sato/TSP : @9b11jsat-2269                    #*/
/*#     Cope with probe card bending because of heat.                        #*/
/*# R012.11-Sharp : 1999-Nov-26 : M.Fukui : @9b26mfuk-1961                   #*/
/*#     Add Mark & IDI before Marking for SHARP.                             #*/
/*# R012.11 : 1999-Dec-20 : T.Araki : @9c20tara-2130                         #*/
/*#     Added pass-net function for TOSHIBA.                                 #*/
/*# R012.13 : 2000-Jan-04 : M.Fujiwara : @00104mfuj-3240                     #*/
/*#     Bug fixed. Polish wafer loading sequence.                            #*/
/*# R012.11-MICRONAS : 2000-Jan-17 : J.Sato/TSP : @00117jsat-2962            #*/
/*#     Added "First Die Preheat Function" Parameter.                        #*/
/*# R012.13 : 2000-Feb-04 : T.Araki : @00204tara                             #*/
/*#          Add GEM spec.                                                   #*/
/*# R012.04 : 2000-Mar-08 : J.Sato/TSP : @00308jsat-3211                     #*/
/*#     Added "Consecutive Fail Check Back Limit Function" for MATSUSHITA.   #*/
/*# R012.12-7 : 2000-Mar-28 : M.Fukui : @00328mfuk-3771                      #*/
/*#     Added TTL Fail-More func. for TOSHIBA.                               #*/
/*# R012.11 : 2000-Jan-13 : M.Fukui : @00113mfuk-2737                        #*/
/*#     Added category check function for TOSHIBA.                           #*/
/*# Pxx01-R012.11-b : 2000-May-16 : J.Sato/TSP : @00516jsat-4013             #*/
/*#     Bug fixed, Wafer File Auto Receive for ST.                           #*/
/*# Pbg00-R012.12-E : 2000-Jun-13 : T.Satou/TSP : @00613tsat-3445            #*/
/*#     Added recount ink dot after IDC verify check for SEIKO-EPSON.        #*/
/*# R012.14 : 2000-Jul-05 : H.Abe : @9c21habe-2659                           #*/
/*#     For SANSEI    Add SANSEI "FC" Comamnd spec.                          #*/
/*# Axx04-R012.12 : 2000-Sep-21 : M.Fukui : @00921mfuk-3067                  #*/
/*#     For TSMC, Added all-pad PMI at first chip.                           #*/
/*# R014.00 : 2000-Oct-02 : M.Fujiwara : @00a02mfuj-4633                     #*/
/*#     re-design polish wafer quick tarnsfer                                #*/
/*# R014.01, R012.11 : 2000-10-18 : H.Abe : @9701win-14                      #*/
/*#     Add IBM RDP command Processing                                       #*/
/*# Xxx00-R014.02 : 2000-Oct-25 : J.Sato/TSP : @00a25jsat-3848               #*/
/*#     Special Option "Small Die".                                          #*/
/*# Xxx00-R014.02 : 2000-Nov-02 : M.Shinohara : @00a13mshi-4987              #*/
/*#     Added INTEL GPIB Phase 2. BLP,CME,?aMD,aMD,RTW.                      #*/
/*#     Also added in Version Ral01-R012.15                                  #*/
/*# R014.03 : 2001-Jan-18 : R.Manabe : @01118rman-4490                       #*/
/*#     Added "Bin Limit Stop" for STATS.                                    #*/
/*# Xxx00-R014.03 : 2001-Mar-15 : T.Satou(TSP) : @01315tsat-3282             #*/
/*#     PC network connection for HITACHI.                                   #*/
/*# Xxx00-R014.04 : 2001-Apl-19 : T.Takiguchi/TSP : @01413ttak-5649          #*/
/*#     Changed restart position after C-Fail Checkback complete.            #*/
/*# Xxx00-R014.04 : 2001-Jun-07 : T.Takiguchi/TSP : @01530ttak-6386          #*/
/*#     Bug Fixed. C-Fail count at retest die after Checkback complete.      #*/
/*# Xxx00-R014.04 : 2001-Jul-16 : H.Tanaka : @01716mshi-6627                 #*/
/*#     Added Suspend Wafer Count for YNEC.                                  #*/
/*#		Also added in Version Axx02-R012.12-9.								 #*/
/*# Xxx00-R014.05 : 2001-Oct-11 : T.Takiguchi/TSP : @01820ttak-5211          #*/
/*#     Added PC Data Communication ( RS ) for NEC.                          #*/
/*# R014.05, Xxx08-R012.13-8 : 2001-Oct-31 : K.Hirose : @01a15khir-6452      #*/
/*#     Added OCR upload directory parameter FtpOcrUpDir for MITSUBUSHI.     #*/
/*# Xxx00-R014.05 : 2002-Jan-24 : M.Shinohara : @02124mshi-7649              #*/
/*#     Bug fix of Gpib 'T' returns SRQ51H even if Z is contact.             #*/
/*#     Also fixed in Version Xxx00-R014.04-3,Xxx01-R014.02A1(B),            #*/
/*#     Xxx09-R012.15-D.                                                     #*/
/*# Xxx00-R014.05   : 2002-Jan-28 : M.Watanabe : @02121masw-6910             #*/
/*#     Added Card Info send for G/C.(for OKI)                               #*/
/*# Xxx00-R014.05, Xxx05-R012.14-T : 2002-Mar-22 : H.Abe : @02225habe-7452   #*/
/*#     Add Pop server RS232C communication for TOSHIBA.                     #*/
/*# Rxx08-R014.01-J, Xxx00-R014.05 : 2002-Jun-04 : H.Abe : @02604habe-7981   #*/
/*#     For IBM,                                                             #*/
/*#       1. ?MDxS0 command can be used for 2nd load port.                   #*/
/*#       2. The load port number of the following commands depend on the    #*/
/*#          load port number of ?MSxS0 command.                             #*/
/*# R014.04-ASE : 2002-Feb-27 : W.Motion : @02227wmot-6955                   #*/
/*#     Lot name [32] area for ASE.                                          #*/
/*# Xxx00-R014.05 : 2002-Oct-17 : R.Nisihimura : @02a17rnis-6077             #*/
/*#     Added cassette status display for HITACHI consecutive lot.           #*/
/*# Xxx00-R014.05 : 2002-Oct-29 : M.Shinohara : @01309mshi-4925              #*/
/*#     Added TSMC Carrier Management System function for Agilent 4070       #*/
/*#     series Tester into P-12XL GEM System.                                #*/
/*#     Also added in version Rxx05-R014.01-5,Rxx08-R014.01-9,Ral02-R014.03-7#*/
/*#     and Xxx02-R014.04-E.                                                 #*/
/*# Xxx00-R014.05 : 2002-Nov-26 : T.Sato(TELST) : @01621hkaw-5522            #*/
/*#     moved setup file switching flag from appli/aNetComm.c                #*/
/*# Xxx00-R014.06 : 2003-Apr-18 : M.Yamamoto : @03124myam-8139               #*/
/*#     Continuity Check with Light WAPP.                                    #*/
/*# Xxx00-R014.06 : 2003-May-22 : M.Tsukishima : @01828yita-6387             #*/
/*#     Added Ftp Net function.                                              #*/
/*# Xxx01-R014.02-? : 2003-Apr-13 : M.Watanabe :  @03413mwat-9082            #*/
/*#     Mark Data verify check.                                              #*/
/*# Xxx00-R014.06 : 2003-May-23 : M.Tsukishima : @01828yita-6387             #*/
/*#     Added Spooling Function.                                             #*/
/*# Xxx00-R014.06 : 2003-May-22 : M.Yamamoto : @03327myam-8248               #*/
/*#     Enhanced WAPP 2 Sheets Function                                      #*/
/*# Xxx00-R014.06 : 2003-May-29 : Y.Itagaki/TELST : @02b18yita-8196          #*/
/*#     Marking Prober for PSC.                                              #*/
/*# Xxx00-R014.06 : 2003-Jun-09 : S.Kojima : @03527skoj-9478                 #*/
/*#     Unload stop parameter was added.                                     #*/
/*#     "Unload Stop On The Second To The Last Wafer."                       #*/
/*# Xxx00-R014.06 : 2003-Jun-19 : A.Matsu/TELST : @02806tkob-8199            #*/
/*#     Add PSC GPIB 'UD' Command.                                           #*/
/*# Xxx00-R014.06 : 2003-Jun-19 : A.Matsu/TELST : @02806amat-8200            #*/
/*#     Add PSC Auto Recovery Procedure.                                     #*/
/*# Xxx00-R014.06 : 2003-Jun-26 : T.Sato(TELST) : @02409myam-6959            #*/
/*#     Re-Test BIN designation method for ASE Test(All Taiwan)              #*/
/*# Xxx00-R014.06 : 2003-Jul-24 : J.Sato/TELST : @02a24hkaw-8850             #*/
/*#                                              @03724jsat-8850             #*/
/*#     added setup files' information table for FTP.                        #*/
/*# Xxx00-R014.06 : 2003-Aug-14 : T.Sato(TELST) : @03715tsat-9180            #*/
/*#     HITACHI skip-P-ken with GEM.                                         #*/
/*# Xxx00-R014.06 : 2003-Sep-29 : CJ.Heo : @03909cheo-8268                   #*/
/*#     Added GpPciResultF flag ( PCI Result by aPC Command )                #*/
/*# Xxx00-R014.06 : 2003-Oct-07 : S.Kojima : @03808skoj-10746                #*/
/*#     Bug fixed for needle polish function during measurement by Wafer.    #*/
/*#     NPWinTest & NPWPolAfterStop is moved from aApplPara.c.               #*/
/*# Xxx00-R014.06 : 2003-Nov-11 : K.Hirose : @03a23khir-9970                 #*/ 
/*#     Added Image grab FTP Net function.                                   #*/
/*# Xxx00-R014.06 : 2003-Dec-25 : T.Sato(TELST) : @03922tsat-10226           #*/
/*#     Upgrade of Skip-P-Ken that is RENESAS(HITACHI) retest specification. #*/
/*# Rxx00-R014.06 : 2004-Mar-08 : CJ.Heo : @03909skoj-8858                   #*/
/*#     Added "ISS" Spec.                                                    #*/
/*# Xxx00-R014.06 : 2004-Apr-29 : T.Sato(TELST) : @02716tsat-8466            #*/
/*#     Added .is file that is setup file of Insight 1700 ocr reader         #*/
/*# Xxx00-R014.06 : 2004-May-27 : T.Takiguchi/TELST : @04527ttak-4895        #*/
/*#     Added re-test mode function with WITS for NEC.                       #*/
/*# Xxx00-R014.06 : 2004-Jun-16 : S.Kojima : @04604skoj-12578                #*/
/*#     Added CHIP OUT function for OKI PN300 Spec.                          #*/
/*# Xxx00-R014.06 : 2004-Jul-13 : n.watanabe : @04610nwat-12202              #*/
/*#     Modified IDC sequence , for NEC & KNEC                               #*/
/*# Xxx00-R014.06 : 2004-Aug-03 : n.watanabe : @04707nwat-11854              #*/
/*#     Modify GMS sequence , at receive REJECT signal , for TOUKOU          #*/
/*# Xxx00-R014.06 : 2004-Jul-08 : N.Oota   : @04708noot-12591                #*/
/*#     Added the function Multi Parts for TOYODIODE.                        #*/
/*# Xxx00-R014.06 : 2004-Sep-15 : M.Saito : @04810msai-12039                 #*/
/*#     Added File Transmission and Reception and FD copy                    #*/
/*# Xxx00-R014.06-1 : 2004-Dec-24 : T.Sato(TELST) : @04c24tsat-12601         #*/
/*#     Skip-P-Ken for RENESAS.                                              #*/
/*# Xxx00-R014.06-1 : 2004-Dec-27 : R.Nishimura : @04827rnis-11563	    	 #*/
/*#	    Add tyBrushLimitOver                                                 #*/
/*# Xxx00-R014.06-1 : 2004-Dec-27 : R.Nishimura : @04827rnis-12778           #*/
/*#	    Add PolLimitOverMedia.                                               #*/
/*# Xxx00-R014.06-3 : 2005-Feb-18 : A.Matsumura : @04809tsat-13010           #*/
/*#     Improve to use both of OCR(reject) and polish wafer.                 #*/
/*# Xxx00-R014.06-3 : 2005-Feb-25 : CJ.Heo      : @04b24cheo-12206           #*/
/*#     Added Online Table Trasfer Flag.                                     #*/
/*# Xxx00-R014.07 : 2005-Mar-15 : K.Hirose : @05222khir-13731                #*/
/*#    Added Log Management FTP parameters memry area.                       #*/
/*# Xxx00-R014.07 : 2005-Mar-22 : R.Nishimura : @04b25yya2-12167             #*/
/*#     Added function, WAPP planarity adjustment check.                     #*/
/*# Xxx00-R014.07 : 2005-Mar-22 : R.Nishimura : @05221myam-14244             #*/
/*#     P-8XL WAPP Flat Correction by Switch probe.                          #*/
/*# Xxx00-R014.07-1 : 2005-Apl-11 : J.Saito   : @05330cheo-14051             #*/
/*#     Added GPIB Contact Check Stop Spec.                                  #*/
/*# Xxx00-R014.07-1 : 2005-Apr-12 : M.Fukui : @05412mfuk-14351               #*/
/*#     Bug fixed. Check contact prior to test did not work well.            #*/
/*# Xxx00-R014.07-1 : 2005-Apr-21 : R.Manabe : @05421rman-14496              #*/
/*#     Added polish log.                                                    #*/
/*# Xxx00-R014.07-2 : 2005-May-19 : A.Yoshida : @05426ayos-13410             #*/
/*#     PCI log save/send to FD/Network                                      #*/
/*# Xxx00-R014.07-* : 2005-Apr-22 : T.Kajihara : @04b04tkaj-11633            #*/
/*#     Add BOT file                                                         #*/
/*# Xxx00-R014.07-* : 2005-Apr-22: T.Kanemaru : @04c05tkan-11633             #*/
/*#     Added conversion of BIN and 2048 BIN.                                #*/
/*# Xxx00-R014.07-2 : 2005-May-19 : M.Saito : @05519msai-13217               #*/
/*#     Added contct log.                                                    #*/
/*# Xxx00-R014.07-3 : 2005-Jun-03 : Y.Yamagami : @05526yya2-15119            #*/
/*#     WAPP different-species polish sheet combined use function            #*/
/*# Xxx00-R014.07-* : 2005-Jun-28 : T.Ishiyama : @04a14mwat-12917            #*/
/*#    Add. 2nd PMI.                                                         #*/
/*# Xxx00-R014.07 : 2005-May-13 : A.Matsumura/TELST : @03604mtog-10273       #*/
/*#     Add Alignment before ReProbe                                         #*/
/*# Xxx00-R014.07-5 : 2005-Jul-25 : T.Matsuda/TELST : @04b05ttak-12952-2     #*/
/*#    Removed all-pads PMI at first die for TSMC (@00921mfuk-3067).         #*/
/*# Xxx00-R014.07-5 : 2005-Jul-02 : T.Honda(TELST) : @04417thon-11917        #*/
/*#     Added Change Probe Alignment Timing after Polish.                    #*/
/*# Xxx00-R014.07-5 : 2005-Jul-19 : Y.Itagaki/TELST : @04318yita-11618       #*/
/*#     added WAFER DATA SEND/RECEIVE 2.                                     #*/
/*# Xxx00-R014.07-5 : 2005-Jul-19 : N.Hirota(TELST) @05314nhir-13537         #*/
/*#     Continuous lot for NEC.                                              #*/
/*# Xxx00-R014.07-5 : 2005-Jul-25 : Matsumura/TELST : @04603thon-12499       #*/
/*#     Improvement of TSMC Automation.                                      #*/
/*# Xxx00-R014.07-5 : 2005-Jul-25 : S.Sasaki(TELST) : @04514nhir-12481       #*/
/*#     Re-Probe after Sample Test                                           #*/
/*# Xxx00-R014.07-5 : 2005-Jul-25 : S.Sasaki(TELST) : @05720ssas-14575       #*/
/*#     Stop at last die function enhance                                    #*/
/*# Xxx00-R014.07-5 : 2005-Jul-26 : S.Sasaki(TELST) : @04616tsat-12714       #*/
/*#     Don't write GMS result map twice in one wafer.                       #*/
/*# Xxx00-R014.08 : 2005-Sep-21 : Y.Kasai : @05208tkam-13460                 #*/
/*# Merge Job : 15762                                                        #*/
/*#     Piece Chip Tray                                                      #*/
/*# Xxx00-R014.08-1 : 2005-Oct-07 : Kawaragi,H : @04525mwat-11923            #*/
/*#     add PADS system (PTPA).                                              #*/
/*#     This modification is registered as JOB15743.                         #*/
/*# Xxx00-R014.08-1 : 2005-Oct-07 : kawaragi,H : @04721hkaw-FPMI-01          #*/
/*#     add FPMIStatF                                                        #*/
/*#     This modification is registered as JOB15743.                         #*/
/*# Xxx00-R014.08-1 : 2005-Oct-07 : Kawaragi,H : @05627hkaw-FPMI             #*/
/*#     add FPMIRetryFlag                                                    #*/
/*#     This modification is registered as JOB15743.                         #*/
/*# Xxx00-R014.08-2 : 2005-Oct-25 : T.Takiguchi/TELST : @05a12ttak-16320     #*/
/*#     Fixed the bug that First Die PreHeat worked at illegal position.     #*/
/*# Xxx00-R014.08-4 : 2005-Dec-06 : T.Kamijou : @05c03tkam-16795             #*/
/*#     Delete PieceChipEndCounter.                                          #*/
/*# Xxx00-R014.08-4 : 2005-Dec-20 : R.Nishimura : @05a07rnis-15623           #*/
/*#     Added SWProbeLimitOverStatus.                                        #*/
/*# Xxx00-R014.08-4 : 2005-Dec-22 : N.Hirota/TELST : @05c22nhir-15591        #*/
/*#    GJG batch setting func for TOSHIBA.                                   #*/
/*# Xxx00-R014.08-6 : 2006-Jan-16 : Y.Yamagami : @05c05yya2-13334            #*/
/*#     Added Polish sheet surface height check function for Z-WAPP.         #*/
/*# Rxx00-R014.08-6 : 2006-Jan-19 : N.Hirota(TELST) @06119nhir-7986          #*/
/*#     Pause/Resume wafer testing for NEC.                                  #*/
/*# Xxx00-R014.08-6 : 2006-Jan-26 : S.Sasaki(TELST) : @06126ssas-16443       #*/
/*#     Bug fixed. Alarm lamp doesn't work according to setting.             #*/
/*#  Xxx00-R014.08-6 : 2005-Jan-18 : M.Saito : @06118msai-13753              #*/
/*#      Added IDC Recount function.                                         #*/
/*# Xxx00-R014-08-6 : 2006-Jan-31 : CJ.Heo            : @06123cheo-15822     #*/
/*#     Added Mulit FailMore processing.( Only TTL )                         #*/
/*# Xxx00-R014.08-6 : 2006-Jan-25 : T.Watanabe : @06125twat-15759            #*/
/*#     Remove WARNING for vxw55                                             #*/
/*# Xxx00-R014.08-D : 2006-May-8 : CJ.Heo : @06424cheo-17752                 #*/
/*#     Probe Card Inspection by GPIB aPCS Command                           #*/
/*# Xxx01-R014.08J2 : 2006-Aug-07 : A.Yokouchi : @06621ayok-17390            #*/
/*#     Simultaneous Contact Polish Wafer Function for TSBATC                #*/
/*# Xxx00-R014.08-G : 2006-Jun-23 : n.watanabe : @06620nwat-17000            #*/
/*#     Added  n-chip align,PC-BENDING by STEP .                             #*/
/*# Xxx00-R014.08-G : 2006-Jun-20 : T.Takiguchi/TELST : @06620ttak-17003-4   #*/
/*#     Modified First Die PreHeat. (Removed "StartPreHeatDo".)              #*/
/*# Xxx00-R014.08-H : 2006-Jul-05 : J.Nishiyama : @06705jnis-15963           #*/
/*#     Added 50mmPolisher Change Function.                                  #*/
/*# Xxx00-R014.08-H : 2006-Jun-21 : CJ.Heo     :  @06607cheo-16559           #*/
/*#     Added GPIB Contact Check Skip Spec.                                  #*/
/*# Xxx00-P014.08-TSBY : 2006-Jul-21 : H.Ashizawa : @06721hash-18117         #*/
/*#     Added Network Parameter for Skip Map Operation                       #*/
/*# Xxx00-R014.08-TSBY : 2006-07-26 : T.Yui : @06718tyui-18117               #*/
/*#     Added Skip map for TOSHIBA Precio.                                   #*/
/*# Xxx01-R014.08J2 : 2006-Aug-07 : Y.Yamagami : @03a27rman-10690            #*/
/*#     For using two polish wafer.                                          #*/
/*# Xxx01-R014.08J2 : 2006-Aug-07 : Y.Yamagami : @04123rman-11450            #*/
/*#     For using two polish wafer plus.                                     #*/
/*# Xxx01-R014.08J2 : 2006-Aug-07 : Y.Yamagami : @06808yya2-17390            #*/
/*#     Added functions for using two polish wafer.                          #*/
/*# Xxx01-R014.08J2 : 2006-Aug-07 : R.Nishimura : @06807rnis-17390           #*/
/*#     Simultaneous Contact Polish Wafer Function for TSBATC                #*/
/*# Xxx00-R014.08-TSBY : 2006-Jul-21 : CJ.Heo  : @06712cheo-17433            #*/
/*#     Added Contact Position Check Spec.                                   #*/
/*# Xxx00-R014.08-? : 2006-Aug-22 : CJ.Heo         : @06818cheo-18615        #*/
/*#     Timing Change by Manual PMI.                                         #*/
/*# Xxx00-R014.08L3 : 2006-Oct-17 : T.Kawasaki     : @06a17tkaw-18029        #*/
/*#     Added Auto Z Up for P12TOSHIBA.                                      #*/
/*# Xxx00-R014.08-M : 2006-Oct-20 : Y.Tamura : @06a20ytam-17047              #*/
/*#     Added Preheat before First Contact function                          #*/
/*# Xxx00-R014.08-N : 2006-Nov-22 : J.Hoshihara  @06b22jhos-18738            #*/
/*#     Bug fixed  GPIB Marking                                              #*/
/*# Xxx00-R014.08-N : 2006-Dec-14 : J.Nishiyama : @06c14jnis-18667           #*/
/*#     Bridge Inker installation height offset change for SANYO.            #*/
/*# Xxx00-R014.08-P : 2006-Dec-26 : F.Morisawa(MW) : @06c26fmor-19443        #*/
/*#     Bug fixed. Not end file stat S2F85 for FTP file transfter.           #*/
/*# Xxx00-X014.08-P : 2007-Jan-18 : K.Hirose : @07116khir-18660              #*/
/*#     Added First Contact Overdrive Condition.                             #*/
/*# Xxx00-R014.08-P : 2007-Feb-01 : T.Ishiyama : @07130tish-17201            #*/
/*#     Added Wait Before Unload.                                            #*/
/*# Xxx00-R014.08-P : 2007-Feb-28 : T.Yamamoto : @07225tyam-19300            #*/
/*#     Add Waiting for end of PADS result data delete on TELPADS.           #*/
/*# Xxx00-R014.08-P : 2007-Mar-23 : T.Matsuda : @07323tma2-17122             #*/
/*#     polish Parameters Improvement Setting                                #*/
/*# Xxx00-R014.08-P : 2007-Mar-26 : J.Kamisawada : @07322jkam-19300          #*/
/*#     Masked PADS PTPO switch.                                             #*/
/*# Xxx00-R014.08-Q : 2007-Apr-23 : A.Omata   : @07419aoma-18791             #*/
/*#     Add Display Install State.                                           #*/
/*# Xxx00-X014.08-* : 2007-Apr-05 : A.Omata  : @07509aoma-17457              #*/
/*#     Add Funciton Ink Particle Check.                                     #*/
/*# Xxx00-R014.08-Q : 2007-May-25 : M.Fairouse :  @07525mfai-17549           #*/
/*#    GPIB command for polish wafer.                                        #*/
/*# Xxx00-R014.08-R : 2007-Aug-20 : M.Fairouse : @07718mfai-20122            #*/
/*#     Polish before PC Bending.                                            #*/
/*# Xxx00-R014.08Q4 : 2007-Aug-20 : S.Sasaki(TELST) : @07810ssas-16511       #*/
/*#     Tn Testing support sample die testing.                               #*/
/*# Xxx00-R014.08-R : 2007-Aug-20 : M.Fairouse : @07718mfai-20122            #*/
/*#     Polish before PC Bending.                                            #*/
/*# Xxx00-R014.08-R : 2007-Sep-05 : F.Morisawa(MW) : @07713fmor-19508        #*/
/*#     Added Prober Server Network communications at rot start.             #*/
/*# Xxx00-R014.08-R : 2007-Sep-05 : Y.Yamagami : @07820yya2-17811            #*/
/*#     Added function for Inker drive to WAPP.                              #*/
/*# Xxx00-X014.08-R : 2008-Feb-29 : T.Annen : @07531kaki-20028               #*/
/*#     Added Sending Operate Status Event for NEC.                          #*/
/*# Xxx00-R014.08-S : 2008-Apr-04 : CJ.Heo  : @08304cheo-21942               #*/
/*#     Special TTL Spec. For FUJITSU Media Device.                          #*/
/*# Xxx00-R014.08-S : 2008-Mar-05 : T.Ishiyama : @08305tish-21942            #*/
/*#     Special TTL Spec. For FUJITSU Media Device.                          #*/
/*# Xxx00-R014.08E4 : 2008-Apr-05 : T.Yui : @08408tyui-22485                 #*/
/*#     Added Auto Function for FUJITSU.                                     #*/
/*# Xxx00-R014.08-S : 2008-Apr-30 : R.Nishimura : @08430rnis-22440           #*/
/*#     Improve on M0208 assist.                                             #*/
/*# Xxx00-X014.08-S : 2008-Jun-30 : N.Shisaike : @08630nshi-20961            #*/
/*#     FTP Operation Param for Maxim                                        #*/
/*# Xxx00-X014.08-S : 2008-Nov-05 : K.Ishii : @08b05kisi-23604               #*/ 
/*#     File Transfer between different IP types of Prober                   #*/
/*# Xxx00-X014.08-S : 2008-Dec-04 : T.Yamamoto : @08b19tyam-23129            #*/
/*#    Modify for FTP put Event Log file.                                    #*/
/*# Xxx00-X014.08-S : 2008-Dec-09 : Y.Yamagami : @08c10yya2-23847            #*/
/*#     Added Stage Z separate before alarm message (SPO#486 enhance)        #*/
/*# Xxx00-X014.08-T : 2009-Jan-07 : Y.Yamagami : @08b10yya2-23804            #*/
/*#     Bug fixed. ReprobeOD, Polish separate position, SoftContact preheat. #*/
/*# Xxx00-X014.08-T : 2009-Feb-04 : F.Morisawa(MW) : @09204fmor-24103        #*/
/*#     Added OCR file handling change for Prober Server.                    #*/
/*# Xxx00-X014.08S* : 2009-Feb-20 : T.Taniguchi/TELST : @09220takt-24235     #*/
/*#     add flg for accept/reject stop button on camera view in PADS insp.   #*/
/*# Xxx00-X014.08-T : 2009-Mar-24 : Y.Ashizawa(MW): @09303fmor-23133         #*/
/*#     Added&Changed Auto run function for FUJITSU.                         #*/
/*# Xxx00-X014.08-T : 2009-Apr-15 : T.Takiguchi/TELST : @08904ttak-22819     #*/
/*#     Removed PADS PTPA Function.                                          #*/
/*# Xxx00-X014.08-T : 2009-May-15 : T.Ishiyama : @08b10tish-23129            #*/
/*#     Add Advanced Event Log Collection                                    #*/
/*# Xxx00-X014.08-T : 2009-Jun-10 : T.Yamamoto/TELST : @09603tyam-24059      #*/
/*#     Pads product files import/export with Prober Wafer File FTP.         #*/
/*# Xxx00-X014.08-T : 2009-May-28 : M.Ichikawa   @09519mich-24314            #*/
/*#     Mod Create dummy GMS Wafer & Lot file at Force LotEnd after WaferEnd.#*/
/*# Xxx00-X014.08-T : 2009-Aug-28 : J.L.Jeanneau (TEF): @09828jjea-24187     #*/
/*#     Log improvement                                                      #*/
/*# Xxx00-X014.08-U : 2009-Sep-04 : S.Nakada : @09825sna2-23633              #*/
/*#     Added [ Ink Map ] in FTP network parameter setting.                  #*/
/*# Xxx00-X014.08-T : 2009-Aug-31 : S.Uematsu(MW) : @09730fmor-22426         #*/
/*#     Add BIN Table2 of Re-probe for TSMC.                                 #*/
/*# Xxx00-X014.08-W : 2009-Dec-25 : t.ikeya : @08627ttak-22119               #*/
/*#     Merged PTPO(PADS)                                                    #*/
/*# Xxx00-X015.00-1 : 2010-Jun-23 : Y.Kasai : @10623ykas-23921               #*/
/*#     Added parameter of Designated Die to function of Start Chip Preheat. #*/
/*# Xxx00-X015.00-2 : 2010-Sep-30 : T.Nakazawa : @10922tnak-26656            #*/
/*#     Bug fixed. N-Index for Each Die Alignment.                           #*/
/*# Xxx00-X015.00-2 : 2010-Oct-01 : Y.Kasai : @10a01ykas-26226               #*/
/*#     Added GPIB Contact Check when measurement is stopping.               #*/
/*# Xxx00-X015.00-D : 2011-Feb-24 : S.Nakada : @11224sna2-23950              #*/
/*#     OCR file copy function implement.                                    #*/
/*# Xxx00-X015.00-H : 2010-Jul-20 : T.Ishiyama : @10720tish-26218            #*/
/*#    Added Function "intelligence Probecard Auto Stabilizer( iPAS )".      #*/
/*# Xxx00-X015.00-8 : 2010-Jan-07 : Y.Ashizawa(MW): @10a04fmor-26334         #*/
/*#     Added Throughput improvement Continuous Lot version for NEC.         #*/
/*# Xxx00-X015.00-x : 2011-Feb-17 : K.Hirose : @11209khir-26334              #*/
/*#     Added check lot reserved/cancel function.                            #*/
/*# Xxx0s-X015.00-x : 2011-Mar-22 : K.Hirose : @11315khir-27533              #*/
/*#     Bug fix. Function is interrupted, suspended consecutive lot.         #*/
/*# Xxx00-X015.00-X : 2011-May-12 : K.Hirose : @11512khir-26334              #*/
/*#     Bug fix. P-8 undefined.                                              #*/
/*# Xxx00-X015.00-W : 2011-Sep-20 : K.Hirose : @10c16cheo-27118              #*/
/*#     Added Display M5556 when 2chip be skipped.                           #*/
/*# Xxx00-X015.00-b : 2011-Sep-30 : T.Watanabe : @11930twa3-27825            #*/
/*#     Added Step Interval in Alignment by Die                              #*/
/*# Xxx00-X015.00-d : 2011-Sep-23 : S.Kameoka : @09702skam-28442             #*/
/*#     Send FPMIRSET at StageInitial                                        #*/
/*# Xxx00-X015.00-h : 2011-Nov-29 : Y.Tsuruta : @11a24ytsu-28481             #*/
/*#     Add comments for FTP files category/ext to vaoid same bugs in future #*/
/*#     Bug - prober does not QUIT at end of FTP loop for batch send/recv    #*/
/*# Xxx00-X015.00-h : 2011-Nov-24 : CJ.Heo : @11b11cheo-28561                #*/
/*#     For seagate. GPIB recovery command add.                              #*/
/*# Xxx00-X015.00-j : 2011-Dec-09 : J.Saito : @11c09jsai-28805               #*/
/*#     Fault correction which is not Category Re-tested.                    #*/
/*# Xxx00-X015.00-j : 2011-Nov-25 : S.Uematsu(MW) : @11b17suem-28477         #*/
/*#     Added Cleaning sheet management function for RENESAS(NEC).           #*/
/*# Xxx00-X015.00-j : 2011-Nov-17 : M.Saito : @11b17msai-28477               #*/
/*#     Added polish sheet management function.                              #*/
/*# Xxx00-X015.00-s : 2012-Mar-30 : R.Shinohara : @12330rshi-29082 (tish-27639) #*/
/*#     Added Alignment log automatic operation collection function.            #*/
/*# Xxx10-X015.00-3 : 2012-Jun-14 : K.Ishihara : @12512kish-29435            #*/
/*#   Bug fixed. Don't pre-open shutter if chuck-blow mode or                #*/
/*#   displayed multi block message(S10F5).                                  #*/
/*# Xxx10-X015.00-7 : 2012-Aug-31 : T.Kuwabara : @12831tkuw-29300            #*/
/*#     WAPP exchange sequence at LOW Temperature.                           #*/
/*# Xxx10-X015.00-7 : 2012-Aug-07 : Y.Kasai : @12807ykas-29925               #*/
/*#     Added Actual Number in LF Parameter for Strip Probing on WDFDP       #*/
/*# Xxx10-X015.00-9 : 2012-Aug-29 : T.Sano : @12810tsan-29342                #*/
/*#     Added Consecutive Fail for Re-probe.                                 #*/
/*# Xxx10-X015.00-9 : 2012-Aug-29 : J.Saito : @12824jsai-29459               #*/
/*#     Add GPIB Command "aPTPO,x".                                          #*/
/*# Xxx10-X015.00-9 : 2012-Apr-30 : CJ.Heo     : @12420cheo-29458            #*/
/*#     For seagate. GPIB recovery command add. Step2                        #*/
/*# Xxx10-X015.00-A : 2012-Sep-13 : S.Nakada : @12911sna2-28058              #*/
/*#     Increase the size of log.                                            #*/
/*# Xxx00-X015.01-D : 2012-Dec-10 : Y.Kasai : @12c10ykas-29050               #*/
/*#     Added UGLY Die function.                                             #*/
/*# Xxx00-X015.01-D : 2013-Jan-21 : S.Nakada : @13121sna2-29050              #*/
/*#     Added the ability to manage the two hosts "Wafer File Management".   #*/
/*# Xxx00-X015.01-H : 2013-Feb-28 : Y.Kasai : @13228ykas-30619               #*/
/*#     Added Target Move Message Function.                                  #*/
/*# Xxx00-X015.01-J : 2013-Mar-19 : PK.Jang : @11627pjan-27357               #*/
/*#     Added gpib command that transfer the pad image.                      #*/
/*#     Merge JOB : 30162                                                    #*/
/*# Xxx00-X015.01-S : 2013-Jun-11 : S.Nakada : @13611sna2-30881              #*/
/*#     Added Auto P/M Function Step2                                        #*/
/*# Xxx00-X015.01-c : 2013-Jun-26 : Y.Kasai : @13626ykas-30517               #*/
/*#     Added function to simplify Wafer Setup.                              #*/
/*# Xxx00-X015.01-e : 2013-Jul-26 : T.Nakazawa : @13726tnak-31063            #*/
/*#     Added Continuity Plate on Precio Z-WAPP.                             #*/
/*# Xxx00-X015.01-h : 2013:Aug-26 : S.Nakada : @13826sna2-31751              #*/
/*#     tsmc CP ALL Auto Z flag in wafer parameter step2.                    #*/
/*# Xxx00-X015.01-s : 2013-Nov-11 : S.Uematsu(MW)  : @13a30suem-31250        #*/
/*#     Added, Error Log send func.                                          #*/
/*# Xxx00-X015.01-v : 2013-Nov-08 : S.Nakada : @13b08sna2-31071              #*/
/*#     Needle polish max OD change from Diagnotics to wafer                 #*/
/*# Xxx00-X015.01-y : 2013-Dec-23 : K.Ishihara : @13c03kish-30987            #*/
/*#                               : Y.Kazuno   : @13c03ykaz-30987            #*/
/*#     Corresponded to SEMI E87 and GPIB RETEST command for P-8.            #*/
/*# Xxx00-X015.02-X : 2014-Jan-15 : M.Saito : @14115msai-31693               #*/
/*#     Added the WLSLT for manual test function.                            #*/
/*# Xxx00-X015.02-B : 2014-Feb-14 : Y.Kasai : @14214ykas-31292               #*/
/*#     Extended Preheat before 'Stop at First Chip' and Re-preheat function.#*/
/*# Xxx00-X015.02-C : 2014-Feb-14 : Y.Tsuruta : @14214ytsu-31003			 #*/
/*#		Add Micron/IMFT Log FTP server information							 #*/
/*# Xxx00-X015.02-K : 2014-Mar-26 : S.Ooyama   : @14326sooy-32144            #*/
/*#     Control Map Changeover at Re-Probe of Wafer End.                     #*/
/*# Xxx00-X015.02-U : 2014-Jun-25 : D.Hara   : @14626dhar-33268              #*/
/*#     Added Bin check function.                                            #*/
/*# Xxx00-X015.02-U : 2014-Jun-30 : T.Sano :  @14618tsan-32698               #*/
/*#     Added Big Data Project.                                              #*/
/*# Xxx00-X015.02-j : 2014-Sep-29 : Y.Yabuki : @14929yyab-32143              #*/
/*#     Added SC-1311-369 tsmc CP All -                                      #*/
/*#           Save load Password protect function data by FTP mode.          #*/
/*# Xxx00-X015.02-k : 2014-Sep-18 : Y.Kasai : @14918ykas-32796               #*/
/*#     Added operation log & polish to Log Transmission function.           #*/
/*# Xxx00-X015.02-n : 2014-Oct-21 : K.Aiba  : @14a21kaib-33722               #*/
/*#    Added GPIB p3x Command for select buffer table Polish wafer.          #*/
/*# Xxx00-X015.02-r : 2014-Oct-29 : Y.Tsuruta : @14909ytsu-33262 			 #*/
/*#		Add Micron Log Auto Delete											 #*/
/*# Xxx00-X015.02-w : 2014-Jun-19 : S.Nakada : @14619sna2-31392              #*/
/*#     Add GEM ECID for Diagnostics and Operation Parameters.               #*/
/*# Xxx00-X016.01-2 : 2015-Jan-19 : T.Okuyama  : @14b10toku-33897            #*/
/*#    Added GPIB Contact Check(for STANDARD & SPO, same TSMC) and New Func. #*/
/*# Xxx00-X016.01-3 : 2015-Jan-28 : Y.Tsuruta : @14c12ytsu-32318			 #*/
/*#		Add GPIB Special Lamp Control - SRQ Send Flag						 #*/
/*# Xxx00-X016.01-5 : 2015-Jan-28 : K.Gomi : @15128kgom-34007                #*/
/*#     Removal of the original file deletion of transmission log function.  #*/
/*# Xxx00-X016.01-A : 2015-Mar-09 : T.Sano(MW) : @15303tsan-33923            #*/
/*#     Add Data server project(Polish log, Preheat Log)                     #*/
/*# Xxx00-X016.01-D : 2015-Mar-09 : S.Ooyama  : @15309sooy-34715             #*/
/*#     "DPU / GMS" switching software improvement.                          #*/
/*# Xxx00-X016.01-M : 2015-May-20 : F.Morisawa(MW) : @14c13fmor-32700        #*/
/*#     Added. Operation Parameter File Upload/Download Func for GF(US).     #*/
/*# Xxx00-X016.01-N : 2015-Jun-02 : S.Uematsu(MW)  : @15115suem-33805        #*/
/*#    Added, WAPP Management function.                                      #*/
/*# Xxx00-X016.01-Q : 2015-Mar-13 : P.C.Juan  : @15513jpoc-34663             #*/
/*#     PDMS log save,compression and transfer to FTP server.                #*/
/*# Xxx00-X016.01-U : 2015-Jul-06 : Y.Kazuno : Merge  @15326ykaz-33655       #*/
/*#     Following GPIB Commands are made available to receive                #*/
/*#     during the Test Stop.                                                #*/
/*#     Command : A/DM/S1                                                    #*/
/*# Xxx00-X016.01-Z : 2015-Jul-27 : S.Nakada : @15727sna2-33510              #*/
/*#     New Polisher excution pattern function                               #*/
/*# Xxx00-X016.01-d : 2015-Aug-10 : Y.Ashizawa(MW): @15810yash-34919         #*/
/*#     Added Matching Score Function.                                       #*/
/*# Xxx00-X016.01-e : 2015-Sep-04 : F.Morisawa(MW) : @15823fmor-32701        #*/
/*#     Added. File Handling Improvement for GF(US).                         #*/
/*# Xxx00-X016.01-e : 2015-Sep-07 : S.Uematsu(MW)  : @15415suem-32699        #*/
/*#     Added, Edit Mode Function.                                           #*/
/*# Xxx00-X016.01-k : 2015-Oct-26 : Y.Yabuki: @15a26yyab-35188               #*/
/*#     Added Set lot data download and input by barcode function.           #*/
/*# Xxx00-X016.01-m : 2015-Nov-02 : T.Mizukami: @15b02tmiz-34897             #*/
/*#     Added PAD file download function by FTP.                             #*/
/*# Xxx00-X016.01-q : 2015-Nov-3 : Dennis Hsu : @15a22hden-36083             #*/
/*#    PMI Immediate log save,compression and transfer to FTP server.        #*/
/*# Xxx00-X016.01-q : 2015-Jun-22 : W.Kenneth : @15622wken-35098             #*/
/*#    Added Wafer File Out put To txt Function , FTP setting.               #*/
/*# Xxx00-X016.01-v : 2015-Dec-15 : Y.Yabuki: @15c15yyab-32695               #*/
/*#     Download result map by different folder.                             #*/
/*# Xxx00-X016.01-* : 2016-Fub-04 : T.Hosaka : @16204thos-36627              #*/
/*#    Changed USERFUNC file access.                                         #*/
/*# Xxx00-X016.02-6 : 2016-Feb-22 : S.Nakada : @16222sna2-31214              #*/
/*#     Convert the Pin Coordinate Data into the Pad Coordinate Data.        #*/
/*# Xxx00-X016.02-9 : 2016-Mar-15 : S.Ooyama  : @16315sooy-33345             #*/
/*#     Added of Switch Probe Air Blow FUnction.                             #*/
/*# Xxx00-X016.02-S : 2016-May-13 : Y.Oosaki : @15410sooy-31544              #*/
/*#    Merge Job : 31544                       @16401yosa-36078              #*/
/*#    2nd Wafer file Host Link.                                             #*/
/*# Xxx00-X016.02-Z : 2016-Sep-05 : S.Uematsu(MW)  : @16510suem-32702        #*/
/*#     Added, Engineering Mode Function.                                    #*/
/*# Xxx00-X016.02-c : 2016-Aug-11 : P.C. Juan : @16811jpoc-36566             #*/
/*#     ASET operation parameter cumulative contact count rule change        #*/
/*# Xxx00-X016.02-f : 2016-Oct-18 : S.Uematsu(MW)  : @16915suem-37940        #*/
/*#     Added, [FOUPReplacementCheck] & [ContactCheckWithGPIB] for ENG Mode. #*/
/*# Xxx00-X016.02-n : 2016-Sep-01 : Ann.T: @16901annt-37461                  #*/
/*#                                     Louis.C: @16908lche-37461            #*/
/*#    PMI Image Transfer and Remote Command Add.                            #*/
/*# Xxx00-X016.02-v : 2017-Jan-26 : S.Ooyama  : @17126sooy-36961             #*/
/*#     Sky-View Automatic Measuring System                                  #*/
/*# Xxx00-X016.03-4 : 2016-Oct-28 : Chen Louis    : @16a28lche-35401         #*/
/*#     tsmcWat PrecioSeries P12series AutoChuck Leveling Check              #*/
/*# Xxx00-X016.03-4 : 2017-Feb-17 : J.Saito : @17217jsai-38237               #*/
/*#     Added PADS exec timing select.                                       #*/
/*# Nyz00-P016.03-6 : 2017-Mar-07 : Y.kim : @17111ykim-38258                 #*/
/*#     Added GPIB WPDN, WPUP cmd for Wafer Parameter Transfer               #*/
/*# Xxx00-X016.03-A : 2017-May-08 : T.Okuyama : @16c23toku-38112             #*/
/*#     Added Debug System (DBS Log).                                        #*/
/*#     Added DBS Wafer File Upload Server.     @17220sooy-38653             #*/
/*#     Merge JOB : 38848                                                    #*/
/*# Xxx00-X016.03-E : 2017-Feb-24 : W.Bao : @17224bwen-38567                 #*/
/*#     MR Project Automation.                                               #*/
/*# Xxx00-X016.03-E : 2017-Apr-03 : T.Watanabe : @17403twa3-36286            #*/
/*#     Added Function Auto delete wafer file at selected interval.          #*/
/*# Xxx00-X016.03-F : 2017-Feb-27 : S.Nakada : @17227sna2-38710              #*/
/*#    Calibration plate is carried out every chip.                          #*/
/*# Xxx00-X016.03-N : 2017-Jul-04 : R.Sasamoto : @17630rsas-39375            #*/
/*#     Added FTP parameter OCR Image Log for FOCUS OCR.                     #*/
/*# Xxx00-X016.03-N : 2017-Jun-26 : Jimmy Cheng : @17626jimc-39427           #*/
/*#  Fix change the wafer file lead to Mmi hang up problem when use Easy Cool#*/
/*# Xxx00-X016.03-Z : 2017-Sep-29 : Darius Berghe : @17922dari-39275         #*/
/*#    Added ODTP FTP parameter globals                                      #*/
/*# Xxx00-X016.04-7 : 2017-Aug-08 : Clare Lin : @17808clcr-39690             #*/
/*#    Added a status for RWAFERID error.                                    #*/
/*# Xxx00-X016.04-7 : 2017-Oct-05 : Bella Chen : @17a05cbel-39126            #*/
/*#     Added FTP parameter : Alarm Save All Log Data                        #*/
/*# Xxx00-X016.04-P : 2018-Jan-30 : Dennis Hsu : @18130hden-39466            #*/
/*#     PMI checking probe mark overlap function.                            #*/
/*# Xxx00-X016.05-3 : 2018-May-25 : R.Sasamoto : @18525rsas-39921            #*/
/*#     Added FTP parameter Prober File Management for HITACHI.              #*/
/*# Xxx00-X016.05-7 : 2018-May-17 : Bella Chen : @18517cbel-40839            #*/
/*#     Added FTP parameter : ECID/SVID Query Data.                          #*/
/*# Xxx00-X016.05-7 : 2018-Jun-21 : Bill Huang :@18621hbil-40289             #*/
/*#    Added DoPolBefICC flag, to judge execute Needle Polish before         #*/
/*#    WAPP 2Sheet ICC or not.                                               #*/
/*# Xxx00-X016.05-9 : 2018-Jun-07 : Joe Liou : @18607ljoe-40403              #*/
/*#     Added PMI Image Transfer FTP Parameter                               #*/
/*# Xxx00-X016.05-C : 2018-May-21 : Dennis Hsu : @18521hden-37272            #*/
/*#     Added Ftp Patameter Image Log for WAPP Sheet Type Recognized.        #*/
/*# Xxx00-X016.05-C : 2018-Sep-10 : R.Sasamoto : @18820rsas-40333            #*/
/*#    Added FTP parameter Probe Card Management Function.                   #*/
/*# Xxx00-X016.05-G : 2018-Oct-26 : K.Ishii : @18711kisi-40973               #*/
/*#    Support Special Index for Power Meter                                 #*/
/*# Xxx00-X016.05-H : 2018-Aug-01 : Jerry.Yang : @18801yjer-40681            #*/
/*#     New first contact PMI supported retest wafer.                        #*/
/*# Xxx00-X016.05-N : 2018-Jun-01 : Ann Tzen : @18601annt-40845              #*/
/*#     Added ICC Step3 : Icc Parameter Contact Check and Chip Tray.         #*/
/*# Xxx00-X016.06-5 : 2018-Nov-28 : Dennis Hsu : @18b28hden-39869            #*/
/*#    Download Probing Path function.                                       #*/
/*# Xxx00-X016.06-B : 2018-Dec-10 : Jerry Yang : @18c10yjer-41303            #*/
/*#    First Die Preheat retry function improvement.                         #*/
/*# Xxx00-X016.06-M : 2019-Mar-22 : Clare Lin : @19322clcr-41513             #*/
/*#    Continuity Inspection Function Improvement Rev.2                      #*/
/*# Xxx00-X016.06-R : 2019-Jul-05 : Y.Yabuki  : @19705yyab-41681             #*/
/*#     ePMI Phase1                                                          #*/
/*# Xxx00-X016.06-T : 2019-May-23 : Dennis Hsu : @19523hden-41561            #*/
/*#    Improvement for Download Probing Path and Disable Bad CH.             #*/
/*# Xxx00-X016.06-U : 2018-Oct-19 : Simon Wang : @18a19wsim-40021            #*/
/*#    Added Full Die Image Grabbing FTP list parameter                      #*/
/*# Xxx00-X016.07-1 : 2019-Feb-25 : H.Arai     : @19225hara-40963            #*/
/*#    Added BITCP communication specification development.                  #*/
/*# Xxx00-X016.07-2 : 2019-Sep-20 : J.L.Jeanneau   : @19920jjea-41770        #*/
/*#     Part 1: QVICS#14553  : SPO_FTP_SEND_LOGFILE(#637) :                  #*/
/*#             [All Log Compressed File] upload dir cfg creates hang-up     #*/
/*#     Part 2: Remove restriction : allow none-GEM software (Xby) to have   #*/
/*#             HSMS log when TelPronet is ON (because we need for PN300)    #*/
/*# Xxx00-X016.07-3 : 2019-Aug-14 : Dennis Hsu     : @19814hden-41686        #*/
/*#    Unload Wafer before WAPP Exchange                                     #*/
/*# Xxx00-X016.07-3 : 2019-Jul-22 : Jerry Yang : @19722yjer-41530            #*/
/*#    WAPP coplanarity measurement improvement.                             #*/
/*# Xxx00-X016.07-8 : 2019-Oct-31 : Dennis Hsu : @19a31hden-41773            #*/
/*#    Vxworks network port disable function.                                #*/
/*# Xxx00-X016.07-E : 2019-Sep-16 : Jerry Yang : @19916yjer-41153            #*/
/*#    MK4 ECO improvement.                                                  #*/
/*# Xxx00-X016.07-M : 2020-Jan-13 : John Chen  : @20113cjoh-41953            #*/
/*#     Bug fix. GPIB p3/p3x/p3xU Command                                    #*/
/*# Xxx00-X016.07-Q : 2019-Nov-25 : Robin Wu : @19b25wrob-41737              #*/
/*#    Added PMI Pad coordiate information menu.                             #*/
/*# Xxx00-X016.07-V : 2020-Mar-09 : Jerry Yang : @20309yjer-41225            #*/
/*#    PMI pad coordinate load from HOST.                                    #*/
/*# Xxx00-X016.08-2 : 2020-Apr-29 : Dennis Hsu : @20429hden-42046            #*/
/*#    New VID for Bridge/Target speed.                                      #*/
/*# Xxx00-X016.08-3 : 2020-Apr-07 : Ryan Liao : @20407lrya-41903             #*/
/*#    Virtual PAD PMI.                                                      #*/
/*# Xxx00-X016.08-7 : 2020-Apr-08 : Joe Liou : @20408ljoe-40022              #*/
/*#    Parameter Auto Download For NXP.                                      #*/
/*# Xxx00-X016.08-A : 2020-Aug-07 : T.Ishiyama   : @20807tish-42128          #*/
/*#    Added Remote Operation Enhancement Request.                           #*/
/*# Xxx00-X016.08-C : 2020-Jun-03 : Kay Lin : @20603lkay-41796               #*/
/*#    New Re-Probe Condition Request                                        #*/
/*# Xxx00-X016.09-1 : 2020-Aug-12 : Bella Chen : @20812cbel-42269            #*/
/*#    Multi Point Wafer Thickness Measurement Rev.3                         #*/
/*# Xxx00-X016.09-6 : 2020-Sep-09 : Yulin Sun : @20909syul-41647             #*/
/*#    Manual PMI Upload Image FTP                                           #*/
/*# Xxx00-X016.09-7 : 2020-Nov-20 : T.Kawaguchi : @20b20tka3-42458           #*/
/*#    Shape Model Upload/download VPP files                                 #*/
/*# Xxx00-X016.09-8 : 2020-Sep-04 : Eric Chen : @20904ceri-42108             #*/
/*#    Add Second Host Ip.                                                   #*/
/*# Xxx00-X016.09-C : 2020-Nov-19 : Justin Chang : @20b19cjus-41869          #*/
/*#    FTP communication log                                                 #*/
/*# Xxx00-X016.09-D : 2020-Sep-15 : Kay Lin : @20915lkay-41851               #*/
/*#    Wafer Alignment Stability Check Function                              #*/
/*# Xxx00-X016.09-J : 2020-Dec-10 : Y.Yabuki : @20c10yyab-41849              #*/
/*#     Added Compare PMI light adjustment                                   #*/
/*# Xxx00-X016.09-K : 2021-Feb-18 : Kay Lin : @21218lkay-41608               #*/
/*#     Align Probe Log Saving and Upload for Basic Function                 #*/
/*# Xxx00-X016.09-Q : 2021-Apr-09 : E.Takizawa : @21409etak-42611            #*/
/*#     Added Alarm Lamp 5COLOR.                                             #*/
/*# Xxx00-X016.09-Q : 2021-Mar-08 : K.Hosaka : @21308khos-42596              #*/
/*#    Parameter List XML.                                                   #*/
/*# Xxx00-X016.09-R : 2020-Oct-23 : Bella Chen : @20a23cbel-42254            #*/
/*#     N shot alignment improvement.                                        #*/
/*# Xxx00-X016.09-S : 2021-Apr-09 : E.Takizawa : @21409etak-42611            #*/
/*#     Added Alarm Lamp 5COLOR.                                             #*/
/*# Xxx00-X016.09-T : 2021-May-18 : F.Morisawa(MW) : @21119fmor-42375        #*/
/*#    Added Flanness check by multi-points(select).                         #*/
/*# Xxx00-X016.09-V : 2020-May-29 : R.Sasamoto : @20306rsas-42006            #*/
/*#    Added Conversion Cellcia GEM for Intel.                               #*/
/*# Xxx00-X016.09-Z : 2021-Jun-04 : Atwood Li : @21604latw-42732             #*/
/*#    Full Die Image Grabbing Extension                                     #*/
/*# Xxx00-X016.10-4 : 2021-Apr-09 : Justin Chang : @21409cjus-42418          #*/
/*#    Regular Log Function: ECO Log.                                        #*/
/*#**************************************************************************#*/
#include    "condition.h"       /* @9222twat-01 */
#include    "stdioProb.h"         /* @06125twat-15759 */

#if (WAFERNAME32)               /* @03724jsat-8850 */
#include    "wafername32.h"     /* @03724jsat-8850 */
#endif                          /* @03724jsat-8850 */

#if (LOTNAME32)                     /* @02227wmot-6955 */
#include    "lotname32.h"           /* @02227wmot-6955 */
/* @03724jsat-8850  #include    "wafername32.h"         @* @02409myam-6959 */
#endif                              /* @02227wmot-6955 */

#include	"vxWorks.h"
/* @06125twat-15759  #include <stdio.h>     */
#include    <semLib.h>          /* @03b07jjea-6815 */
#include	<time.h>
#include	"ECCdef.h"
#include	"APPL_main.h"
#include	"APPL_wafTest.h"	/* R9.00 */
#include    "alarm.h"           /* @7a29mfuj */
#include    "gpibDef.h"         /* @9329hkaw-1334 */
#include    "APPL_applPara.h"   /* @02409myam-6959 */
#include    "fileDir.h"         /* @02a24hkaw-8850 */
#include    "ftpFiles.h"        /* @02a24hkaw-8850 */
#include    "network_params.h"  /* @03b07jjea-6815 */
#include    "APPL_wafData.h"	/* @04827rnis-11563 */
#include    "PolishLog.h"	                              /* @05421rman-14496 */
#include    "APPL_net.h"        /* @11209khir-26334 */
#include    "netFunc.h"         /* @11209khir-26334 */
#include    "mstr.h"            /* @11209khir-26334 */
#include    "hsmsDefine.h"      /* @11b17suem-28477 */
#if ( PC300 )
#include    "binCommon.h"       /* @04c05tkan-11633 */
#endif
/* @09401ttak-22819 #if( P12XL )    @* @09220takt-24235 */
#if( PADSOPT )                      /* @09401ttak-22819 */
#include "fastPmiData.h"            /* @09220takt-24235 */
#include "applComn.h"               /* @12824jsai-29459 */
#include "APPL_wafData_GLB.h"       /* @12824jsai-29459 */
/* @09401ttak-22819 #endif (P12XL ) @* @09220takt-24235 */
#endif( PADSOPT )                   /* @09401ttak-22819 */
#include    "msgQ.h"                /* @11c09jsai-28805 */
#include	"APPL_lotData.h"		/* @11126mshi-26334 */
#include    "../option/bitcp/bitcpDef.h"/* @19225hara-40963 */
#include    <stdlib.h>                  /* @19a31hden-41773 */
#include    <string.h>                  /* @19a31hden-41773 */
#include    "ctype.h"                   /* @19a31hden-41773 */
#include    "aComnPara.h"               /* @19a31hden-41773 */
#include    "PolishDef.h"               /* @21119fmor-42375 */
/*@@@ @7818mshi-02 @* アプリケーションタスクＩＤ  */
/*@@@ @7818mshi-02 intl	ApplTask; */
/*@@@ @7818mshi-02 *@ */

extern  intl    ChkThroughputContiNEC( VOID );  /* @11126mshi-26334 */
extern  FILE    *TyFopen( intb *, intb *, intw );       /* @19a31hden-41773 */
extern  intl    CheckandMakeDir( intb * );              /* @19a31hden-41773 */
extern  intw    ltoa( intl, unsb *, intw );             /* @19a31hden-41773 */

VOID    NECConsectiveLotLotRsvd( VOID );    /* @11126mshi-26334 */
VOID    NECConsectiveLot1stWafLd( VOID );   /* @11126mshi-26334 */
VOID    NECConsectiveLot1stWafLkd( VOID );  /* @11126mshi-26334 */
VOID    NECConsectiveLotWafChgSnt( VOID );  /* @11126mshi-26334 */
VOID    NECConsectiveLotWafChgEnd( VOID );  /* @11126mshi-26334 */
VOID    NECConsectiveLotPmtRcvd( VOID );    /* @11126mshi-26334 */
VOID    NECConsectiveLotLotEndSeq( VOID );  /* @11126mshi-26334 */
VOID    NECConsectiveLotWafChgRpt( VOID );  /* @11126mshi-26334 */
VOID    NECConsectiveLotWafChgRcvd( VOID ); /* @11126mshi-26334 */
VOID    NECConsectiveLotLotEndSeq( VOID );  /* @11126mshi-26334 */
VOID    NECConsectiveLotPalnSnt( VOID );    /* @11128mshi-26334 */
VOID    NECConsectiveLotPalnEnd( VOID );    /* @11128mshi-26334 */
intl    NECConsectiveLotWaitWafChg( VOID ); /* @11126mshi-26334 */
intl    NECConsectiveLotGoNxtSeq( VOID );	/* @11126mshi-26334 */
intl    NECConsectiveLot1stWaf( VOID ); 	/* @11126mshi-26334 */
intl    NECConsectiveLotWaitPalnEnd( VOID );/* @11126mshi-26334 */
intl    ChkTptCntNECSuspendFLE( VOID );     /* @11315khir-27533 */
void    SetComNetFStat4( intl );            /* @11b17msai-28477 */
intl    GetComNetFStat4( void );            /* @11b17msai-28477 */
void    ClrComNetFStat4( unsl );            /* @11b17suem-28477 */
void    SetManualTestFlg( intl );           /* @14115msai-31693 */
intl    GetManualTestFlg( void );           /* @14115msai-31693 */
intl    GetDisPortFromFile( unsb * );                               /* @19a31hden-41773 */
intl    SetDisPortToFile( unsb * );                                 /* @19a31hden-41773 */
intl    IsPortNumberStringBase( unsb *, unsl * );                   /* @19a31hden-41773 */
intl    IsPluralityPortNumberStringBase( unsb *, unsl *, unsl * );  /* @19a31hden-41773 */
intl    ConvDisPortStrBitArray( unsb *, unsb *, intl );             /* @19a31hden-41773 */

extern unsb ApMasterFile[];      /* @02a24hkaw-8850 */
extern unsb ApWaferFile[];       /* @02a24hkaw-8850 */
extern unsb ApAlignFile[];       /* @02a24hkaw-8850 */
extern unsb ApProbeFile[];       /* @02a24hkaw-8850 */
extern unsb ApContactFile[];     /* @02a24hkaw-8850 */
extern unsb ApMarkFile[];        /* @02a24hkaw-8850 */
extern unsb ApOprFile[];         /* @08630nshi-20961 */
extern intl Sys_mode;            /* @11209khir-26334 */
extern unsl PreLotCancelState;   /* @11209khir-26334 */
extern intl InitWaferF;          /* @11209khir-26334 */
extern intl logMsg(intb *, ...); /* @11314mshi-27534 */
#if( P12XL )                              /* @14321jsai-31962 */
extern intl ContactManageSpecExe( void ); /* @14321jsai-31962 */
#endif                                    /* @14321jsai-31962 */

extern  unsb  ApWpxxMasterFile[];      /* @17111ykim-38258 */
extern  unsb  ApWpxxWaferFile[];       /* @17111ykim-38258 */
extern  unsb  ApWpxxAlignFile[];       /* @17111ykim-38258 */
extern  unsb  ApWpxxProbeFile[];       /* @17111ykim-38258 */
extern  unsb  ApWpxxContactFile[];     /* @17111ykim-38258 */
extern  unsb  ApWpxxMarkFile[];        /* @17111ykim-38258 */

/* アプリケーションメインシーケンスフラグ */
/*#================================================================#*/
/* アプケーション・実行状態を表すフラグ                             */
/*#================================================================#*/
unsl	ApplSeqF;
/**/
/* APLSEQ_IDLE		0x00000001	*//* マニュアル・アプリケーション 	*/
/* APLSEQ_SETUP		0x00000002	*//* セットアップ・アプリケーション */
/* APLSEQ_LOTSTART 	0x00000100	*//* ロット開始・アプリケーション 	*/
/* APLSEQ_LOAD		0x00000200	*//* ウエハ搬送・アプリケーション 	*/
/* APLSEQ_ALIGN     0x00000400  *//* アライメント　アプリケーション */
/* APLSEQ_TEST      0x00000800  *//* 測定・アプリケーション */
/* APLSEQ_LOTEND 	0x00010000	*//* ロット終了・アプリケーション 	*/
/**/

/*#----------------------------------------------------------------#*/
/* アイドルアプリケーション・シーケンスコントロール・フラグ         */
/*#----------------------------------------------------------------#*/
unsl	IdleSeqF;

/*#----------------------------------------------------------------#*/
/* ロット開始アプリケーション・シーケンスコントロール・フラグ       */
/*#----------------------------------------------------------------#*/
intl	LotStartSeqF;

/*#----------------------------------------------------------------#*/
/* ウエハ搬送アプリケーション・シーケンスコントロール・フラグ       */
/*#----------------------------------------------------------------#*/
intl	WafLoadSeqF;
intl	AW001SendF;
intl    AW001Status;                            /* @04809tsat-13010 */

/*#----------------------------------------------------------------#*/
/* アライメントアプリケーション・シーケンスコントロール・フラグ     */
/*#----------------------------------------------------------------#*/
intl	AlignSeqF;

/*#----------------------------------------------------------------#*/
/* 測定アプリケーション・シーケンスコントロール                     */
/*#----------------------------------------------------------------#*/
intl	WafTestSeqF;
/**/
/* BEF_TEST        0x00000001  *//* 測定開始前アプリ実行中         */
/* TEST_READY      0x00000002  *//* テスト準備完了イニシャルチップ */
                                 /* 処理完了                       */
/* AFT_TEST        0x00000004  *//* 測定後アプリケーション実行中   */
/* WAIT_GP         0x00000100  *//* ＧＰＩＢコマンド待ち           */
/**/

intl	ApTestFlag;
/**/
/* APTEST_BEFORE   0x00000001  *//* チップのテスト前               */
/* APTEST_START    0x00000002  *//* チップのテストスタート出力     */
/* APTEST_END      0x00000004  *//* チップのテスト終了             */
/**/

/*#----------------------------------------------------------------#*/
/* ロット終了アプリケーション・シーケンスコントロール               */
/*#----------------------------------------------------------------#*/
intl	LotEndSeqF;
intl    lotEndMapSendFNEC;                       /* @06119nhir-7986 */
intl    SuspendRetestFlg;                        /* @06119nhir-7986 */

/*==================================================================*/
/* 測定マスタータスクとの通信フラグ                                 */
/*==================================================================*/
unsl	ComMasterF;
/**/
/* MASTER_REQL     0x00000001  *//* マスターからの搬送コマンド要求 */
/* MASTER_SENDL    0x00000002  *//* マスターに搬送コマンド送信済み */
/* MASTER_REQT     0x00000004  *//* マスターからの測定コマンド要求 */
/* MASTER_SENDT    0x00000008  *//* マスターに測定コマンド送信済み */
/* MASTER_STOP     0x00000100  *//* マスターからストップ受信     */
/**/

/*==================================================================*/
/* ＧＰマスタータスクとの通信フラグ                                 */
/* ComGpibF のビット定義                                            */
/*==================================================================*/
unsl	ComGpibF;
/**/
/* GP_REQ_ON       0x00000001  *//* ＧＰＩＢからの要求受信済み      */
/* GP_REQ_EXE      0x00000002  *//* ＧＰＩＢコマンド実行中          */
/**/

/*------------------------------------------------------------------*/
/* ＧＰＩＢのインスペクションモードを設定しておく               */ 
/*------------------------------------------------------------------*/
intl	GpInspMode=0;
/*------------------------------------------------------------------*/
/* ＧＰＩＢタスクからのメッセージを格納しておくエリア               */ 
/*------------------------------------------------------------------*/
unsb	GpMsgBak[128];
unsb    StopGpMsg[128];    /* 96/08/09 M.Fukui(TSP) */   
unsb    GpStopBak[128];    /* @15326ykaz-33655 */
/*------------------------------------------------------------------*/
/* マスタータスクへ送信したメッセージを格納しておくエリア           */
/*------------------------------------------------------------------*/
unsb	SendMsgBakL[128];	/* 搬送系コマンド */
unsb	SendMsgBakT[128];	/* 測定系コマンド */
/*------------------------------------------------------------------*/ /* @02806amat-8200 */
/* Tester's QCRL procedure end Flag for PSC GCS_3                   */ /* @02806amat-8200 */
/* =1 : already end.                                                */ /* @02806amat-8200 */
/*------------------------------------------------------------------*/ /* @02806amat-8200 */
intl    QCRLendF=0;                                                    /* @02806amat-8200 */

/*------------------------------------------------------------------*/
/*	ウエハテスト開始前プロセスに関するアプリケーション制御用データ  */
/*------------------------------------------------------------------*/
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02 intl	BTestPrTbl[128];	* プロセス制御テーブル *@ */
intl	BTestPrTbl[BTEST_MAX];	/* R9.00 */	
intl	BTestPNo;			/* プロセスＮｏ．*/
intl	BTestPNoPointer;	/* プロセス制御テーブルのポインター */ 
intl    BTestPrTblNPW[BTEST_MAX_NPW];   /* @9324tara-01 */
intl    BTestPrTbIcc[BTEST_MAX_ICC];    /* @18601annt-40845 */

/*------------------------------------------------------------------*/
/*	ウエハテスト中プロセスに関するアプリケーション制御用データ      */
/*------------------------------------------------------------------*/
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02 intl	DTestPrTbl[20][4];	* プロセス制御テーブル *@ */
intl	DTestPrTbl[DTEST_MAX][4];	/* R9.00 */
intl	DTestPNo;			/* プロセスＮｏ．*/
intl	DTestPNoPointer;	/* プロセス制御テーブルのポインター */ 
#if (HOSTFUNC && GEM)                   /* @06b22jhos-18738 */
intl    SendDieTest;                    /* @06b22jhos-18738 */
#endif                                  /* @06b22jhos-18738 */

unsb    kama;                           /* @14619sna2-31392 */

/*------------------------------------------------------------------*/
/*	ウエハテスト後プロセスに関するアプリケーション制御用データ      */
/*------------------------------------------------------------------*/
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02 intl	ATestPrTbl[30];		* プロセス制御テーブル *@ */
intl	ATestPrTbl[ATEST_MAX];		/* R9.00 */
intl	ATestPNo;			/* プロセスＮｏ．*/
intl	ATestPNoPointer;	/* プロセス制御テーブルのポインター */ 

/*------------------------------------------------------------------*/
/*  ウエハ搬送プロセスに関するアプリケーション制御用データ          */
/*------------------------------------------------------------------*/
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02 intl LApplPrTbl[30][4];	*プロセス制御テーブル*@	@* V8.02 20->30 *@ */
intl    LApplPrTbl[LAPPL_MAX][4];	/* R9.00 */
intl    LApplPNo;           /* プロセスＮｏ．*/
intl    LApplPNoPointer;    /* プロセス制御テーブルのポインター */

/*@@@ @7818mshi-02 @*  */
/*@@@ @7818mshi-02 intl	LTablePrTbl[10];* テーブル搬送プロセス制御テーブル *@ */
intl	LTablePrTbl[LTABLE_MAX];	/* R9.00 */
intl	LTablePNo;
intl	LTablePNoPointer;

/*------------------------------------------------------------------*/
/*  ロットエンドプロセスに関するアプリケーション制御用データ        */
/*------------------------------------------------------------------*/
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02 intl	LEndPrTbl[20];		* プロセス制御テーブル *@ */
intl	LEndPrTbl[LEND_MAX];	/* R9.00 */	
intl	LEndPNo;			/* プロセスＮｏ．*/
intl	LEndPNoPointer;		/* プロセス制御テーブルのポインター */
 
/*------------------------------------------------------------------*/
/*  チャック上のウエハ管理用データ                                  */
/*------------------------------------------------------------------*/
unsl	WafChuckF;
/**/
/* WAF_ON_CHUCK      0x00000001  *//* チャック上にウエハ有り        */

/*------------------------------------------------------------------*/
/*  マスタータスクからのイニシャルモード遷移報告フラグ              */
/*------------------------------------------------------------------*/
unsl	SysIniStatus;
/**/
/* SYS_MANUAL		0x00000001	*//* マニュアルモードに移行			*/
/* SYS_SETUP		0x00000002	*//* セットアップモードに移行		*/
/* SYS_AUTO			0x00000004	*//* オートモードに移行				*/
/* SYS_INIT_ALL		0x00000100	*//* 全イニシャル実行				*/
/* SYS_INIT_STAGE	0x00000200	*//* ステージイニシャル実行			*/
/* SYS_INIT_LOADER	0x00000400	*//* ローダーイニシャル実行			*/

/*------------------------------------------------------------------*/
/*  ローディングモードフラグ                                        */
/*------------------------------------------------------------------*/
unsl	SysLoadMode;
/**/
/* SYS_CAST_LOAD	0x00000001  *//* カセット搬送モード				*/
/* SYS_TABLE_LOAD	0x00000002  *//* ウエハテーブル搬送モード		*/
/* SYS_FRONT_LOAD	0x00000004	*//* フロント搬送モード				*/

/*==================================================================*/
/*  デモプロービング用パラメータスイッチ                            */
/*==================================================================*/
/*@@@ @7818mshi-02 @* R6.04 */
/*@@@ @7818mshi-02 unsl	DemoAlign;  */
/*@@@ @7818mshi-02 デモアライメントスイッチ[0]:通常モード、[1]:デモモード */
/*@@@ @7818mshi-02 unsl	DemoProbe; */
/*@@@ @7818mshi-02 デモプロービングスイッチ[0]:通常モード、[1]:デモモード */
/*@@@ @7818mshi-02 unsl	DemoLoader; */
/*@@@ @7818mshi-02 デモローディングスイッチ[0]:通常モード、[1]:デモモード */
/*@@@ @7818mshi-02 unsl	DemoCMap; */
/*@@@ @7818mshi-02 コントロールマップ使用スイッチ[0]:コントロールマップ使用、[1]:未使用 */
/*@@@ @7818mshi-02 unsl	AlignCheck; */
/*@@@ @7818mshi-02 アライメントチェックスイッチ[0]:通常モード、[1]:デモモード */
/*@@@ @7818mshi-02 unsl	DemoMain; */
/*@@@ @7818mshi-02 デモサイクルメイン・スイッチ[0]:通常モード、[1]:デモモード */
/*@@@ @7818mshi-02 *@ */
/*------------------------------------------------------------------*/
/* コンタクト状態を現すフラグ（０：セパレート、１：コンタクト）     */
/*------------------------------------------------------------------*/
/* @02124mshi-7649 intl	ContactStatus;	 */

/*------------------------------------------------------------------*/
/* アンロードストップカウンター                                     */
/*------------------------------------------------------------------*/
unsl	UnldStopC;

/*------------------------------------------------------------------*/
/* サスペンドフラグ                                                 */
/*------------------------------------------------------------------*/
unsl	SuspendF;
/*@@@ @7818mshi-02 @*  */
/*@@@ @7818mshi-02  * ０：通常状態               0: Normal (no suspend)                 */
/*@@@ @7818mshi-02  * １：測定終了後、強制終了   1: end lot After testing at wafer end  */
/*@@@ @7818mshi-02  * ２：測定を中断し、強制終了 2: stop immediatly                     */
/*@@@ @7818mshi-02  * ３：次のウエハに移行       3: load next wafer                     */
/*@@@ @7818mshi-02  *@ */
unsl    SuspendWaf = 0;     /* @01716mshi-6627 */

/*------------------------------------------------------------------*/
/* ステージで設定されたインカ位置登録データ                         */
/*------------------------------------------------------------------*/
intl	ApInkerData[4];
/*@@@ @7818mshi-02 @*  */
/*@@@ @7818mshi-02  * ０：インカー無し */
/*@@@ @7818mshi-02  * １：ブリッジインカー */
/*@@@ @7818mshi-02  * ２：アームインカー */
/*@@@ @7818mshi-02  * ３：同一エリアインカー１ */
/*@@@ @7818mshi-02  * ４：同一エリアインカー２ */
/*@@@ @7818mshi-02  * ５：同一エリアインカー３ */
/*@@@ @7818mshi-02  * ６：同一エリアインカー４ */
/*@@@ @7818mshi-02  *@ */

/*------------------------------------------------------------------*/
/* ストップー＞スタート後のＺコントロール送信フラグ                 */
/*------------------------------------------------------------------*/
/* @02124mshi-7649 intl	stopZCont; */
/*@@@ @7818mshi-02 @* １：ストップスタート後のＺアップ送信済み */
/*@@@ @7818mshi-02  * ０：Ｚアップ終了後にリセット */
/*@@@ @7818mshi-02  *@ */

/*------------------------------------------------------------------*/
/* アラームモニター制御用データ                                     */
/*------------------------------------------------------------------*/
tyLamp	LampData;

/*------------------------------------------------------------------*/
/* 連続フェイルフラグ                                               */
/*------------------------------------------------------------------*/
intl	CFailFlag;
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02  * ０：連続フェイルでは、ない */
/*@@@ @7818mshi-02  * ｎ：連続フェイルのＣＨ番号 */
/*@@@ @7818mshi-02  *@ */

intl	CFailStat;
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02  * Ｄ０：連続フェイル発生 */
/*@@@ @7818mshi-02  * Ｄ１：連続フェイルチェックバックテスト中 */
/*@@@ @7818mshi-02  * Ｄ２：連続フェイル停止中（ストップ） */
/*@@@ @7818mshi-02  * Ｄ３：連続フェイル停止中（チェックバック） */
/*@@@ @7818mshi-02  *@ */

/*------------------------------------------------------------------*/
/* イニシャルテストフラグ                                           */
/*------------------------------------------------------------------*/
intl	InitTestF;
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02  * ０：テスト前 */
/*@@@ @7818mshi-02  * １：テスト済み */
/*@@@ @7818mshi-02  *@ */

/*------------------------------------------------------------------*/
/* イニシャルマークフラグ                                           */
/*------------------------------------------------------------------*/
intl	InitMarkF;
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02  * ０：マーク前 */
/*@@@ @7818mshi-02  * １：マーク済み */
/*@@@ @7818mshi-02  *@ */
/*------------------------------------------------------------------*/
/* スタート時のチップ座標補正フラグ                                 */
/*------------------------------------------------------------------*/
intl	StartVerifyF;
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02  * ０：補正なし */
/*@@@ @7818mshi-02  * １：補正実行 */
/*@@@ @7818mshi-02  *@ */

/*------------------------------------------------------------------*/
/* ロット再開時のスタートウエハ番号                                 */
/*------------------------------------------------------------------*/
intl	ReStartCassette;
intl	ReStartSlot;
/* V1.30 */
intl	GpSendF = 0;
/* @11c09jsai-28805 unsb    GpibSBuffer[60];          */
unsb    GpibSBuffer[MSGBUFLIMIT]; /* @11c09jsai-28805 */
unsb    GpibSendBuf[MSGBUFLIMIT]; /* @17227sna2-38710 */

/*------------------------------------------------------------------*/
/* インクカウントウエハカウンター    V1.31                          */
/*------------------------------------------------------------------*/
unsl	IdcCntWaf;
/* R3.05 */
unsl	ApplSetUpF=0;
unsl    IdcPrintF=0;                  /* @00c18masw-5432 */

/*------------------------------------------------------------------*/
/* ＩＤＣ実行フラグ    V1.34                                        */
/*------------------------------------------------------------------*/
intl	IdcStatF;
/*@@@ @7818mshi-02 @*  */
/*@@@ @7818mshi-02  * ー１：測定終了直後（同時マークＩＤＣ用、暫定） */
/*@@@ @7818mshi-02  * ０：初回 */
/*@@@ @7818mshi-02  * １：通常 */
/*@@@ @7818mshi-02  * ２：終了 */
/*@@@ @7818mshi-02  *@ */

/*------------------------------------------------------------------*/
/*  IDC Auto Retry at New Seaquence for NEC & KNEC                  */
/*------------------------------------------------------------------*/
intl    IDCRetryCounterS = 0;              /* @04610nwat-12202 */

intl    IDCDoRetryInkS = 0;                /* @04610nwat-12202 */
/* 0... no, 1...IDC Retry                     @04610nwat-12202 */

/*------------------------------------------------------------------*/
/* ＰＭＩ実行フラグ    V4.00                                        */
/*------------------------------------------------------------------*/
intl	PmiStatF;
/*@@@ @7818mshi-02 @*  */
/*@@@ @7818mshi-02  * ０：ＰＭＩ実行しない */
/*@@@ @7818mshi-02  * １：初回ＰＭＩ */
/*@@@ @7818mshi-02  *@ */
intl	PmiChCount;
intl	Pmi1stContact;
/* */
/*@@@ @7818mshi-02 @* R10.01 */
/*@@@ @7818mshi-02 intl	PmiChipCount; */
/*@@@ @7818mshi-02 *@ */
/*@@@ @7818mshi-02 @* V1.55 */
/*@@@ @7818mshi-02  * ＰＭＩ実行チップフラグ */
/*@@@ @7818mshi-02  *@ */

intl	ApMmiMakeMapF;
intl    ApMmiAlinEndF = 0;               /* @13205ykas-29050 */

intl	PmiCntWaf;
/*@@@ @7818mshi-02 @* V1.56 */
/*@@@ @7818mshi-02  * ＰＭＩ実行ウェーハカウンター  */
/*@@@ @7818mshi-02  *@ */
intl    PmiReSetFlag = 0;                /* @16901annt-37461 */
intl    PmiStatSaveF;                    /* @16901annt-37461 */
intl    PmiStatSaveF2 = 0;               /* @18130hden-39466 */
/*------------------------------------------------------------------*/
/* ロットスタート時、受信したメッセージを格納しておくエリア V1.62   */
/*------------------------------------------------------------------*/
unsb	LotStartMsg[128];

/*------------------------------------------------------------------*/
/* プローブエリアファイルをリードするエリア                         */
/*------------------------------------------------------------------*/
/* @7c21yfuk-01 intw    ProbAreaData[603];  */

/*------------------------------------------------------------------*/
/* スキップエリアファイルをリードするエリア                         */
/*------------------------------------------------------------------*/
/* @7c21yfuk-01 intw    SkipAreaData[402];  */

/*------------------------------------------------------------------*/
/* ＩＤＩ実行フラグ    V6.05                                        */
/*------------------------------------------------------------------*/
intl	IdiStatF;
/*@@@ @7818mshi-02 @*  */
/*@@@ @7818mshi-02  * ０：ＩＤＩ実行しない */
/*@@@ @7818mshi-02  * １：抜き取りチェックモード */
/*@@@ @7818mshi-02  * ２：スタートチェックモード */
/*@@@ @7818mshi-02  * ３：エンドチェックモード */
/*@@@ @7818mshi-02  * ４：タイムアップチェックモード */
/*@@@ @7818mshi-02  * ５：連続フェイルチェックモード */
/*@@@ @7818mshi-02  *  */
/*@@@ @7818mshi-02  *@ */

intl	IdiExecF;
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02  * Ｄ０：スタートチェック実行済みフラグ */
/*@@@ @7818mshi-02  *       ０＝未実行、１＝実行済み */
/*@@@ @7818mshi-02  *@ */


intl	IdiRndChkCnt[4];
/* ＩＤＩ抜き取りチェック用インクカウント */

intl	IdiSttChkCnt;
/* ＩＤＩスタートチェックインクカウント（インカ単位ではない）*/

intl	IdiEndChkCnt;
/* ＩＤＩエンドチェックインクカウント（インカ単位ではない）*/

intl	IdiContFCnt[4];
/* ＩＤＩ連続フェイルチェック用カウント */

intl	IdiCntWaf;
/* ＩＤＩ実行ウェーハカウンター  */

unsb	IdiInkNo[4];

intl    GpIdiResultF;    /* @7716ttan-01 */

intl    GpPciResultF=1;  /* @03901cheo-8268 */
/*---------------------------------------------------------*/
/* チップ座標検索報告指定パラメータ                        */
/*---------------------------------------------------------*/
intl	NDieStartDir;

/*---------------------------------------------------------*/
/* マークカウント処理フラグ                                */
/*---------------------------------------------------------*/
intl	MkCountStatF;

/*---------------------------------------------------------*/
/* ＩＤＩタイムアップチェック用ワークエリア                */
/*---------------------------------------------------------*/
struct	tm	ApLastInkTm[4];
struct	tm	ApNowInkTm[4];

intl	ApExeInkF[4];

/*---------------------------------------------------------*/
/* ネットワークタスクとのコミュニケーションフラグ１        */
/*   ステータス                                            */
/*---------------------------------------------------------*/
intl	ComNetFStat;
/*---------------------------------------------------------*/
/* ネットワークタスクとのコミュニケーションフラグ２        */
/*   データ                                                */
/*---------------------------------------------------------*/
intl	ComNetFData;

/* R9.02M */
/*---------------------------------------------------------*/
/* ネットワークからの応答ステータスフラグ                  */
/*---------------------------------------------------------*/
unsl    ComNetACKStat;

intl	ApHvFdErrFlag = 0;	/* V1.79 */
/* R7.11 */
struct 	tm 	wStart;
struct 	tm 	wEnd;
struct 	tm 	wStop;
struct 	tm 	wReSt;
intl	stopTime=0;

/* %7116.mark */
/*---------------------------------------------------------*/
/* ネットワークのステータスフラグ                          */
/*---------------------------------------------------------*/
intl RsnetCurrentState;

/* %7116tara */
/*---------------------------------------------------------*/
/* ＦＴＰ失敗ウェーハリスト ( wafer number : "xx" )        */
/*---------------------------------------------------------*/
intl    PosFtpFailCnt = 0;
unsb    PosFtpFailList[25][3];

/* V8.33 */
/*------------------------------------------------------------------*/
/*  ロットスタートプロセスに関するアプリケーション制御用データ      */
/*------------------------------------------------------------------*/
/*
intl	LStartPrTbl[20];		* プロセス制御テーブル */
intl	LStartPrTbl[LSTART_MAX];	/* R9.00 */
intl	LStartPNo;				/* プロセスＮｏ．*/
intl	LStartPNoPointer;		/* プロセス制御テーブルのポインター */
 
intl	GpRdpTIDFlag;			/* GPIB or RDP Task ID 格納 *//* V8.51 */

/* V9.00 */
/*------------------------------------------------------------------*/
/* サンプル測定に関する制御用データ                                 */
/*------------------------------------------------------------------*/
intl	ApSmplPtr;				/* サンプルステップポインター       */
intl	ApSmplModeF;			/* サンプル動作モードフラグ         */
intw	ApSmplMonPtr[2];		/* モニターチップサンプルポインター */
intl	PciStatF;	/* R9.00 */
intl    SampleOnlySample;       /* @04514nhir-12481 */
intl    SampleTestNextMode; /* @07810ssas-16511 */
/* V9.01 */
/*------------------------------------------------------------------*/
/* ブローに関する制御用データ                                       */
/*------------------------------------------------------------------*/
/*@@@ @7818mshi-02 @* R9.00-11 */
/*@@@ @7818mshi-02 intl	CBlowCnt;	* カードブロー実行カウンター       * */
/*@@@ @7818mshi-02 intl	WBlowCnt;	* ウェーハブロー実行カウンター     *@ */
intl	ApCBlowFlag;			/* カードブロー制御フラグ           */
intl	ApWBlowFlag;			/* ウェーハブローフラグ             */
intl    ApChBlowFlag;       /* @9426mshi-347 */
/*------------------------------------------------------------------*/
/* ＢＩＮ連続フェイル制御データ                                     */
/*------------------------------------------------------------------*/
intl	BCFailStat;

/*------------------------------------------------------------------*/
/* スプーリング関係データ  kawaragi@tsp 960606 add                  */
/*------------------------------------------------------------------*/
intl	SpoolStatF ;            /* スプーリングステータスフラグ　   */
intb	WafSpoolStat[60] ;      /* ウェハスプーリング状態           */
intl	ForceLotEnd ;           /* ロット終了状態                   */
intl	SpoolCancelFlag ;       /* スプーリングキャンセル           */
intl	SDSpoolDataPRTbl[20] ;  /*スプーリングデータ送信制御テーブル*/
intl	SDSpoolPNo ;            /* プロセス番号                     */
intl	SDSpoolDataPNoPointer ; /* 制御テーブルのポインタ           */
intl	SpDataSendCounter ;     /* ウェハカウンタ                   */
intb    svWaferStartTM[13] ;    /* ウェハスタート時間 スプーリング中 */
intb    svWaferEndTM[13] ;      /* ウェハエンド時間（スプーリング中） */
intl    HvPcSpoolDataExist ;    /* For only HITACHI @01315tsat-3282 */
intl    GMSHDFirstTestFlag;     /* 初回測定 Flag    @03129masw-9665 */
intl    FtpSpoolFlag = 0;       /* NOT_SPOOL(0) or ON_SPOOL(1) */   /* @02118yita-6957 */

#if (P1200)                     /* @06718tyui-18117 */
intl    SkipMapSpoolFlg = 0;    /* TOSHIBA Skip map spool flg for Precio @06718tyui-18117 */
#endif                          /* @06718tyui-18117 */

/*------------------------------------------------------------------*/
/* Intel Communication Flag V9.22                                   */
/*------------------------------------------------------------------*/
intl    ComIntelF;              /* Interl Commnunication Flag.      */
/*---------------------------------------------------------*/
/* ネットワークタスクとのコミュニケーションフラグ１        */
/*   ステータス  V9.23                                     */
/*---------------------------------------------------------*/
intl	ComNetFStat2;

/*---------------------------------------------------------*/
/* ネットワークタスクとのコミュニケーションフラグ１        */
/*   ステータス  @10a04fmor-26334 Add.                     */
/*---------------------------------------------------------*/
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02  *	連続ロットのスプールにて使用	%6b11 hkaw */
/*@@@ @7818mshi-02  *	ContLotSpoolFlag, Sv1stLotName */
/*@@@ @7818mshi-02  *@ */
intl    ContLotSpoolFlag = 0 ;      /* 連続ロットスプールフラグ     */
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02  *	０：通常ロット処理 */
/*@@@ @7818mshi-02  *	１：１ロット目データスプール済み */
/*@@@ @7818mshi-02  *	２：２ロット目スプールデータ送信終了 */
/*@@@ @7818mshi-02  *@ */

#if (LOTNAME32)                          /* @02227wmot-6955 */
intb    Sv1stLotName[26+LOTNAME_ADDED] ; /* @02227wmot-6955 */
#else                                    /* @02227wmot-6955 */
intb    Sv1stLotName[26] ;          /* 連続ロットスプールロット名   */
#endif                                   /* @02227wmot-6955 */

/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02  *	連続ロットスプールの１ロット目ロット名 */
/*@@@ @7818mshi-02  *@ */

/*------------------------------------------------------------------*//* @11b17suem-28477 */
/* NEC Cleaning sheet management 用 Networkとの通信フラグ           *//* @11b17suem-28477 */
/*------------------------------------------------------------------*//* @11b17suem-28477 */
intl    ComNetFStat4;                                                 /* @11b17suem-28477 */

/*------------------------------------------------------------------*/
/* ＩＤＣ自動インキング機能における実行要求フラグ                   */
/*                                                   R10 %6b22mfuk  */
/*------------------------------------------------------------------*/
intl    AutoInkF;
/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02 * =0:対象チップに対し、自動インキング実行しない */
/*@@@ @7818mshi-02 * =1:対象チップに対し、自動インキング実行を要求する。 */
/*@@@ @7818mshi-02 * =2:移動後の測定チップに対し、それまでの自動インキング */
/*@@@ @7818mshi-02 *    実行要求フラグのクリアを要求する。 */
/*@@@ @7818mshi-02 *@ */
intl    doAutoInk;
/*@@@ @7818mshi-02 @*  */
/*@@@ @7818mshi-02 * =0:通常のＩＤＣ実行を要求する。 */
/*@@@ @7818mshi-02 * =1:対象チップに対し、もう一度ＩＤＣ実行を要求する。 */
/*@@@ @7818mshi-02 *@ */
/*@@@ @7818mshi-02 @* R9.02G */
/*@@@ @7818mshi-02  * カセット番号セーブエリア（三菱高知のみ使用） */
/*@@@ @7818mshi-02 *@ */
intl    NCRSDCassNo = 0;

/*@@@ @7818mshi-02 @* */
/*@@@ @7818mshi-02  * セルコントローラ用スプーリングモードフラグ */
/*@@@ @7818mshi-02  *   ０　　：スプーリングモードでない */
/*@@@ @7818mshi-02  *   ０以外：：スプーリングモード中 */
/*@@@ @7818mshi-02  *@ */
intl    CellSpoolMode ;             /* @7a08habe-01 */

/*------------------------------------------------------------------*/
/* ＦＭＩマップデータファイル名 @7703tyui-01                        */
/*------------------------------------------------------------------*/
unsb	FMIMapName[ 64 ];

/* 三菱高知ロットエンドフラグ  ０：送信しない  １：送信する @7710tyui-05 */
intl    KMITLotEndFlag = 0;

/*-------------------------------------------------------------------*/
/* 測定前ＰＭＩ処理フラグ　For HISEE   @7731mfuk                     */
/*-------------------------------------------------------------------*/
intl    PmiBefTestF;                /* @7731mfuk */

/*-------------------------------------------------------------------*/
/* 東芝連続ロット対応ランランプ点灯点滅フラグ @7731mfuj              */
/*-------------------------------------------------------------------*/
intl    BrinkFlag = 0;

/*------------------------------------------------------------------*/
/* ＦＴＰパラメータ（ＦＭＩ）                             @7730tara */
/*------------------------------------------------------------------*/
unsb    FtpHostIp[ 20 ];   /* ＩＰアドレス */          /* @7730tara */
unsb    FtpUserId[ 20 ];   /* ログイン名   */          /* @7730tara */
unsb    FtpPasswd[ 20 ];   /* パスワード   */          /* @7730tara */
unsb    FtpMapDownDir[ 256 ]; /* ダウンロードdir */    /* @7730tara */
unsb    FtpMapUpDir[ 256 ];   /* アップロードdir */    /* @7730tara */
unsb    FtpLogUpDir[ 256 ];   /* Log Send dir for Siemens */ /* @9625tmat-899 */
unsb    FtpOcrUpDir[ 256 ] ; /* OCR upload dir for MITSUBISHI @01a15khir-6452 */
unsb    FtpMapFileHostIp[ 20 ];    /* IP Address          @01828yita-6387 */
unsb    FtpMapFileUserId[ 20 ];    /* User Name           @01828yita-6387 */
unsb    FtpMapFilePasswd[ 20 ];    /* Pass Word           @01828yita-6387 */
unsb    FtpMapFileDownDir[ 256 ];  /* Map File DownDir    @01828yita-6387 */
unsb    FtpMapFileUpDir[ 256 ];    /* Map File UpDir      @01828yita-6387 */

unsb    FtpImgFileHostIp[ 20 ];    /* IP Address          @03a23khir-9970 */
unsb    FtpImgFileUserId[ 20 ];    /* User Name           @03a23khir-9970 */
unsb    FtpImgFilePasswd[ 20 ];    /* Pass Word           @03a23khir-9970 */
unsb    FtpImgFileUpDir[ 256 ];    /* Map File UpDir      @03a23khir-9970 */

/* Compound Log Data                                      @05222khir-13731 */
unsb    FtpCmpLogFileHostIp[ 20 ] ;     /* IP Address     @05222khir-13731 */
unsb    FtpCmpLogFileUserId[ 20 ] ;     /* User Name      @05222khir-13731 */
unsb    FtpCmpLogFilePasswd[ 20 ] ;     /* Pass Word      @05222khir-13731 */
unsb    FtpCmpLogFileUpDir[ 256 ] ;     /* Log File UpDir @05222khir-13731 */

/* PCI Log Data                                           @05421ayos-13410 */
unsb    FtpPciFileHostIp[ 20 ];    /* IP Address          @05421ayos-13410 */
unsb    FtpPciFileUserId[ 20 ];    /* User Name           @05421ayos-13410 */
unsb    FtpPciFilePasswd[ 20 ];    /* Pass Word           @05421ayos-13410 */
unsb    FtpPciFileUpDir[ 256 ];    /* Log File UpDir      @05421ayos-13410 */

unsb    FtpLotMngHostIp[ 20 ];   /* IP Address       @07713fmor-19508 */
unsb    FtpLotMngUserId[ 20 ];   /* User Name        @07713fmor-19508 */
unsb    FtpLotMngPasswd[ 20 ];   /* Pass Word        @07713fmor-19508 */
unsb    FtpLotMngDownDir[ 256 ]; /* Map File DownDir @07713fmor-19508 */

/* Event Log Ftp Setting                                  @08b19tyam-23129 */
unsb    FtpEvtLogFileHostIp[ 20 ];    /* IP Address       @08b19tyam-23129 */
unsb    FtpEvtLogFileUserId[ 20 ];    /* User Name        @08b19tyam-23129 */
unsb    FtpEvtLogFilePasswd[ 20 ];    /* Pass Word        @08b19tyam-23129 */
unsb    FtpEvtLogFileUpDir[ 256 ];    /* Log File UpDir   @08b19tyam-23129 */

/* "All Log" Ftp send Setting                             @09828jjea-24187 */
unsb    FtpFullLogFileHostIp[ 20 ];   /* IP Address       @09828jjea-24187 */
unsb    FtpFullLogFileUserId[ 20 ];   /* User Name        @09828jjea-24187 */
unsb    FtpFullLogFilePasswd[ 20 ];   /* Pass Word        @09828jjea-24187 */
unsb    FtpFullLogFileUpDir[ 256 ];   /* Log File UpDir   @09828jjea-24187 */

/* Ink Map Data                                               @09825sna2-23633 */
unsb    FtpInkMapFileHostIp[ 20 ];    /* IP Address           @09825sna2-23633 */
unsb    FtpInkMapFileUserId[ 20 ];    /* User Name            @09825sna2-23633 */
unsb    FtpInkMapFilePasswd[ 20 ];    /* Password             @09825sna2-23633 */
unsb    FtpInkMapFileUpDir[ 256 ];    /* Ink Map File UpDir   @09825sna2-23633 */
unsb    FtpInkMapFileDownDir[ 256 ];  /* Ink Map File DownDir @09825sna2-23633 */

#if( !P1200 )                                                 /* @12330rshi-29082 (tish-27639) */
/* Alignment Assist Log Data                                     @12330rshi-29082 (tish-27639) */
unsb    FtpAlnAstFileHostIp[ 20 ];   /* IP Address               @12330rshi-29082 (tish-27639) */
unsb    FtpAlnAstFileUserId[ 20 ];   /* User Name                @12330rshi-29082 (tish-27639) */
unsb    FtpAlnAstFilePasswd[ 20 ];   /* Pass Word                @12330rshi-29082 (tish-27639) */
unsb    FtpAlnAstFileUpDir[ 256 ];   /* Aln Ast Log File UpDir   @12330rshi-29082 (tish-27639) */
#endif                                                        /* @12330rshi-29082 (tish-27639) */

/* Log Transmission Data                                 @12911sna2-28058 */
unsb FtpTransLogHostIpAddr[ IPADDR_MAX_CHAR + 1 ];    /* @12911sna2-28058 */
unsb FtpTransLogUserName[ FTP_USER_MAX_CHAR +1 ];     /* @12911sna2-28058 */
unsb FtpTransLogPasswd[ FTP_PASSWD_MAX_CHAR +1];      /* @12911sna2-28058 */
unsb FtpTransLogMode;                                 /* @12911sna2-28058 */
unsb FtpTransLogAllUploadDir[ FTP_DIR_MAX_CHAR +1 ];  /* @12911sna2-28058 */
unsb FtpTransLogHSMSUploadDir[ FTP_DIR_MAX_CHAR +1 ]; /* @12911sna2-28058 */
unsb FtpTransLogPrbUploadDir[ FTP_DIR_MAX_CHAR +1 ];  /* @12911sna2-28058 */
unsb FtpTransLogAliUploadDir[ FTP_DIR_MAX_CHAR +1 ];  /* @12911sna2-28058 */
unsb FtpTransLogGPIBUploadDir[ FTP_DIR_MAX_CHAR +1 ]; /* @12911sna2-28058 */
unsb FtpTransLogERRUploadDir[ FTP_DIR_MAX_CHAR +1 ]; /* @13a30suem-31250 */
unsb FtpTransLogPMIUploadDir[ FTP_DIR_MAX_CHAR +1 ];  /* @14618tsan-32698 */
unsb FtpTransLogCMPUploadDir[ FTP_DIR_MAX_CHAR +1 ];  /* @14618tsan-32698 */
unsb FtpTransLogMMIUploadDir[ FTP_DIR_MAX_CHAR +1 ];  /* @14618tsan-32698 */
unsb FtpTransLogPOLUploadDir[ FTP_DIR_MAX_CHAR + 1 ]; /* @14918ykas-32796 */
unsb FtpTransLogCpresUploadDir[ FTP_DIR_MAX_CHAR + 1 ];     /* @19920jjea-41770 */
unsb FtpTransLogPRHUploadDir[ FTP_DIR_MAX_CHAR + 1 ];       /* @15303tsan-33923 */
unsb FtpTransLogPDMSProbeUploadDir[ FTP_DIR_MAX_CHAR + 1 ]; /* @15513jpoc-34663 */
unsb FtpTransLogPDMSWaferUploadDir[ FTP_DIR_MAX_CHAR + 1 ]; /* @15513jpoc-34663 */
unsb FtpTransLogAsLZHUploadDir[ FTP_DIR_MAX_CHAR + 1 ];     /* @19920jjea-41770 */

/* two hosts "Wafer File Management"                     @13121sna2-29050 */
unsb FtpWafHostIp2[ IPADDR_MAX_CHAR +1 ];             /* @13121sna2-29050 */
unsb FtpWafUserName2[ FTP_USER_MAX_CHAR +1 ];         /* @13121sna2-29050 */
unsb FtpWafPasswd2[ FTP_PASSWD_MAX_CHAR +1 ];         /* @13121sna2-29050 */
unsb FtpWafDownDir2[ FTP_DIR_MAX_CHAR +1 ];           /* @13121sna2-29050 */
unsb FtpWafUpDir2[ FTP_DIR_MAX_CHAR +1 ];             /* @13121sna2-29050 */

/* iProber PMI Data                                      @13611sna2-30881 */
unsb IProPmiFtpHostIp[ IPADDR_MAX_CHAR + 1 ];         /* @13611sna2-30881 */
unsb IProPmiFtpUserId[ FTP_USER_MAX_CHAR + 1 ];       /* @13611sna2-30881 */
unsb IProPmiFtpPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];     /* @13611sna2-30881 */
unsb IProPmiFtpUpDir[ FTP_DIR_MAX_CHAR + 1 ] ;        /* @13611sna2-30881 */

/* WPD file specification.                               @13626ykas-30517 */
unsb FtpWPDHostIp[ IPADDR_MAX_CHAR + 1 ];             /* @13626ykas-30517 */
unsb FtpWPDUserId[ FTP_USER_MAX_CHAR + 1 ];           /* @13626ykas-30517 */
unsb FtpWPDPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];         /* @13626ykas-30517 */
unsb FtpWPDDownDir[ FTP_DIR_MAX_CHAR + 1 ];           /* @13626ykas-30517 */

/* コンタクト管理サーバ                                  @14321jsai-31962 */
#if( P12XL )                                          /* @14321jsai-31962 */
unsb FtpCntMngHostIp[ IPADDR_MAX_CHAR + 1 ];          /* @14321jsai-31962 */
unsb FtpCntMngUserId[ FTP_USER_MAX_CHAR + 1 ];        /* @14321jsai-31962 */
unsb FtpCntMngPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];      /* @14321jsai-31962 */
unsb FtpCntMngDownDir[ FTP_DIR_MAX_CHAR + 1 ];        /* @14321jsai-31962 */
#endif                                                /* @14321jsai-31962 */

/* Micron/IMFT Log FTP server                                  @14214ytsu-31003 */
unsb    FtpIMFTLogFtp[ 9 ];             /* FTP enable/disable. 1=enable @14214ytsu-31003 */
unsb    FtpIMFTLogHostIp[ 9 ][ 20 ];    /* IP Address          @14214ytsu-31003 */
unsb    FtpIMFTLogUserId[ 9 ][ 20 ];    /* User Name           @14214ytsu-31003 */
unsb    FtpIMFTLogPasswd[ 9 ][ 20 ];    /* Pass Word           @14214ytsu-31003 */
unsb    FtpIMFTLogUpDir[ 9 ][ 256 ];    /* Map File UpDir      @14214ytsu-31003 */
intl	MicronLogTemp[2];				/* For Temperature Log @14214ytsu-31003 */

/* PASSWORD PROTECT Data.                                @14929yyab-32143 */
unsb FtpPasProHostIp[ IPADDR_MAX_CHAR + 1 ];          /* @14929yyab-32143 */
unsb FtpPasProUserId[ FTP_USER_MAX_CHAR + 1 ];        /* @14929yyab-32143 */
unsb FtpPasProPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];      /* @14929yyab-32143 */
unsb FtpPasProUpDir[ FTP_DIR_MAX_CHAR + 1 ];          /* @14929yyab-32143 */

unsb FtpWappMngHostIp[ IPADDR_MAX_CHAR +1 ];             /* @15115suem-33805 */
unsb FtpWappMngUserName[ FTP_USER_MAX_CHAR +1 ];         /* @15115suem-33805 */
unsb FtpWappMngPasswd[ FTP_PASSWD_MAX_CHAR +1 ];         /* @15115suem-33805 */
unsb FtpWappMngDownDir[ FTP_DIR_MAX_CHAR +1 ];           /* @15115suem-33805 */
unsb FtpWappMngUpDir[ FTP_DIR_MAX_CHAR +1 ];             /* @15115suem-33805 */
unsb FtpCardMngHostIp[ IPADDR_MAX_CHAR +1 ];             /* @15115suem-33805 */
unsb FtpCardMngUserName[ FTP_USER_MAX_CHAR +1 ];         /* @15115suem-33805 */
unsb FtpCardMngPasswd[ FTP_PASSWD_MAX_CHAR +1 ];         /* @15115suem-33805 */
unsb FtpCardMngDownDir[ FTP_DIR_MAX_CHAR +1 ];           /* @15115suem-33805 */
unsb FtpCardMngUpDir[ FTP_DIR_MAX_CHAR +1 ];             /* @15115suem-33805 */
unsb FtpWappMngHostIpG[ IPADDR_MAX_CHAR +1 ];            /* @15115suem-33805 */
unsb FtpWappMngUserNameG[ FTP_USER_MAX_CHAR +1 ];        /* @15115suem-33805 */
unsb FtpWappMngPasswdG[ FTP_PASSWD_MAX_CHAR +1 ];        /* @15115suem-33805 */
unsb FtpWappMngDownDirG[ FTP_DIR_MAX_CHAR +1 ];          /* @15115suem-33805 */
unsb FtpWappMngUpDirG[ FTP_DIR_MAX_CHAR +1 ];            /* @15115suem-33805 */
unsb FtpCardMngHostIpG[ IPADDR_MAX_CHAR +1 ];            /* @15115suem-33805 */
unsb FtpCardMngUserNameG[ FTP_USER_MAX_CHAR +1 ];        /* @15115suem-33805 */
unsb FtpCardMngPasswdG[ FTP_PASSWD_MAX_CHAR +1 ];        /* @15115suem-33805 */
unsb FtpCardMngDownDirG[ FTP_DIR_MAX_CHAR +1 ];          /* @15115suem-33805 */
unsb FtpCardMngUpDirG[ FTP_DIR_MAX_CHAR +1 ];            /* @15115suem-33805 */

/* Edit Mode Log.                                        @15415suem-32699 */
unsb FtpEditModeHostIp[ IPADDR_MAX_CHAR +1 ];         /* @15415suem-32699 */
unsb FtpEditModeUserName[ FTP_USER_MAX_CHAR +1 ];     /* @15415suem-32699 */
unsb FtpEditModePasswd[ FTP_PASSWD_MAX_CHAR +1 ];     /* @15415suem-32699 */
unsb FtpEditModeUpDir[ FTP_DIR_MAX_CHAR +1 ];         /* @15415suem-32699 */

/* Edit Mode ID & Password.                              @15415suem-32699 */
unsb FtpEMIdHostIp[ IPADDR_MAX_CHAR +1 ];             /* @15415suem-32699 */
unsb FtpEMIdUserName[ FTP_USER_MAX_CHAR +1 ];         /* @15415suem-32699 */
unsb FtpEMIdPasswd[ FTP_PASSWD_MAX_CHAR +1 ];         /* @15415suem-32699 */
unsb FtpEMIdDownDir[ FTP_DIR_MAX_CHAR +1 ];           /* @15415suem-32699 */
unsb FtpEMIdUpDir[ FTP_DIR_MAX_CHAR +1 ];             /* @15415suem-32699 */

tyFtpInfo  ftpInfo;                                    /* @03b07jjea-6815 */

/* 動作パラメータファイル送受信のネットワークパラメータ                   @14c13fmor-32700 */
unsb    FtpOpeParaHostIp[ IPADDR_MAX_CHAR + 1 ];     /* ＩＰアドレス      @14c13fmor-32700 */
unsb    FtpOpeParaUserId[ FTP_USER_MAX_CHAR + 1 ];   /* ログイン名        @14c13fmor-32700 */
unsb    FtpOpeParaPasswd[ FTP_PASSWD_MAX_CHAR + 1 ]; /* パスワード        @14c13fmor-32700 */
unsb    FtpOpeParaDownDir[ FTP_DIR_MAX_CHAR + 1 ];   /* ダウンロードdir   @14c13fmor-32700 */
unsb    FtpOpeParaUpDir[ FTP_DIR_MAX_CHAR + 1 ];     /* アップロードdir   @14c13fmor-32700 */

/* マシンデータファイル送受信のネットワークパラメータ                      @15823fmor-32701 */
unsb    FtpMachDataHostIp[ IPADDR_MAX_CHAR + 1 ];     /* ＩＰアドレス      @15823fmor-32701 */
unsb    FtpMachDataUserId[ FTP_USER_MAX_CHAR + 1 ];   /* ログイン名        @15823fmor-32701 */
unsb    FtpMachDataPasswd[ FTP_PASSWD_MAX_CHAR + 1 ]; /* パスワード        @15823fmor-32701 */
unsb    FtpMachDataDownDir[ FTP_DIR_MAX_CHAR + 1 ];   /* ダウンロードdir   @15823fmor-32701 */
unsb    FtpMachDataUpDir[ FTP_DIR_MAX_CHAR + 1 ];     /* アップロードdir   @15823fmor-32701 */

/* マニュアルFTP送受信のネットワークパラメータ                             @15823fmor-32701 */
unsb    FtpManualSRHostIp[ IPADDR_MAX_CHAR + 1 ];     /* ＩＰアドレス      @15823fmor-32701 */
unsb    FtpManualSRUserId[ FTP_USER_MAX_CHAR + 1 ];   /* ログイン名        @15823fmor-32701 */
unsb    FtpManualSRPasswd[ FTP_PASSWD_MAX_CHAR + 1 ]; /* パスワード        @15823fmor-32701 */
unsb    FtpManualSRDir[ FTP_DIR_MAX_CHAR + 1 ];       /* SRdir             @15823fmor-32701 */

/* 汎用送受信、チェック、一覧取得のパラメータ                             @14c13fmor-32700 */
tyFtpFTP05Data ftpFTP05Data;                                           /* @14c13fmor-32700 */

/* Matching Score Data                                 @15810yash-34919 */
unsb FtpMSLogHostIp[ IPADDR_MAX_CHAR + 1 ];         /* @15810yash-34919 */
unsb FtpMSLogUserId[ FTP_USER_MAX_CHAR + 1 ];       /* @15810yash-34919 */
unsb FtpMSLogPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];     /* @15810yash-34919 */
unsb FtpMSLogUpDir[ FTP_DIR_MAX_CHAR + 1 ] ;        /* @15810yash-34919 */

/* LOT DATA DOWNLOAD Data                              @15a26yyab-35188 */
unsb FtpLotDataHostIp[ IPADDR_MAX_CHAR + 1 ];       /* @15a26yyab-35188 */
unsb FtpLotDataUserId[ FTP_USER_MAX_CHAR + 1 ];     /* @15a26yyab-35188 */
unsb FtpLotDataPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];   /* @15a26yyab-35188 */
unsb FtpLotDataDownDir[ FTP_DIR_MAX_CHAR + 1 ];     /* @15a26yyab-35188 */
unsb FtpLotDataUpDir[ FTP_DIR_MAX_CHAR + 1 ];       /* @15a26yyab-35188 */

/* Pad File Ftp Setting                                @15a20tmiz-34897 */
unsb FtpPadFileHostIp[ IPADDR_MAX_CHAR + 1 ];       /* @15a20tmiz-34897 */
unsb FtpPadFileUserId[ FTP_USER_MAX_CHAR + 1 ];     /* @15a20tmiz-34897 */
unsb FtpPadFilePasswd[ FTP_PASSWD_MAX_CHAR + 1 ];   /* @15a20tmiz-34897 */
unsb FtpPadFileDownDir[ FTP_DIR_MAX_CHAR + 1 ];     /* @15a20tmiz-34897 */
unsb FtpPadFileUpDir[ FTP_DIR_MAX_CHAR + 1 ];       /* @16222sna2-31214 */

/* PMI Log Immediate Data                                  @15a22hden-36083 */
unsb FtpPMILogSPHostIp[ IPADDR_MAX_CHAR + 1 ];          /* @15a22hden-36083 */
unsb FtpPMILogSPUserId[ FTP_USER_MAX_CHAR + 1 ];        /* @15a22hden-36083 */
unsb FtpPMILogSPPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];      /* @15a22hden-36083 */
unsb FtpPMILogSPUpDir[ FTP_DIR_MAX_CHAR + 1 ];          /* @15a22hden-36083 */

unsb FtpTxtFileHostIpAddr[ IPADDR_MAX_CHAR +1 ];         /* @15622wken-35098 */
unsb FtpTxtFileuserName[FTP_USER_MAX_CHAR+1];            /* @15622wken-35098 */
unsb FtpTxtFilepasswd[ FTP_PASSWD_MAX_CHAR +1 ];         /* @15622wken-35098 */
unsb FtpTxtFileuploadDir1[ FTP_DIR_MAX_CHAR +1 ];        /* @15622wken-35098 */
unsb FtpTxtFileuploadDir2[ FTP_DIR_MAX_CHAR +1 ];        /* @15622wken-35098 */

/* Map Download Folder Sever Data                           @15c15yyab-32695 */
unsb FtpMapDwnHostIp[ 9 ][ IPADDR_MAX_CHAR + 1 ];        /* @15c15yyab-32695 */
unsb FtpMapDwnUserId[ 9 ][ FTP_USER_MAX_CHAR + 1 ];      /* @15c15yyab-32695 */
unsb FtpMapDwnPasswd[ 9 ][ FTP_PASSWD_MAX_CHAR + 1 ];    /* @15c15yyab-32695 */
unsb FtpMapDwnDownDir[ 9 ][ FTP_DIR_MAX_CHAR + 1 ];      /* @15c15yyab-32695 */
unsb FtpMapDwnUpDir[ 9 ][ FTP_DIR_MAX_CHAR + 1 ];        /* @15c15yyab-32695 */

/* Conv. Pin Coord.                                         @16222sna2-31214 */
unsb FtpConvPinHostIpAddr[ IPADDR_MAX_CHAR +1 ];         /* @16222sna2-31214 */
unsb FtpConvPinUserName[FTP_USER_MAX_CHAR+1];            /* @16222sna2-31214 */
unsb FtpConvPinPasswd[ FTP_PASSWD_MAX_CHAR +1 ];         /* @16222sna2-31214 */
unsb FtponvPinDownloadDir[ FTP_DIR_MAX_CHAR +1 ];        /* @16222sna2-31214 */

unsb FtpPMIHostIpAddr[ IPADDR_MAX_CHAR+1 ];                /* @16908lche-37461 */
unsb FtpPMIUserName[ FTP_USER_MAX_CHAR+1 ];                /* @16908lche-37461 */
unsb FtpPMIPasswd[ FTP_PASSWD_MAX_CHAR+1 ];                /* @16908lche-37461 */
unsb FtpPMIUploadDir[ FTP_DIR_MAX_CHAR+1 ];                /* @16908lche-37461 */
unsb FtpPMIDownloadDir[ FTP_DIR_MAX_CHAR+1 ];              /* @18607ljoe-40403 */

/* Sub Host2 Parameters                                    @15410sooy-31544 */
tyFtpInfo  SubHost2Params;                              /* @15410sooy-31544 */

unsb FtpEngModeHostIp[ IPADDR_MAX_CHAR +1 ];             /* @16510suem-32702 */
unsb FtpEngModeUserName[ FTP_USER_MAX_CHAR +1 ];         /* @16510suem-32702 */
unsb FtpEngModePasswd[ FTP_PASSWD_MAX_CHAR +1 ];         /* @16510suem-32702 */
unsb FtpEngModeDownDir[ FTP_DIR_MAX_CHAR +1 ];           /* @16510suem-32702 */
unsb FtpEngModeUpDir[ FTP_DIR_MAX_CHAR +1 ];             /* @16510suem-32702 */

/* Sky View System.                                         @17126sooy-36961 */
unsb FtpSkyViewHostIp[ IPADDR_MAX_CHAR + 1 ];            /* @17126sooy-36961 */
unsb FtpSkyViewUserName[ FTP_USER_MAX_CHAR + 1 ];        /* @17126sooy-36961 */
unsb FtpSkyViewPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];        /* @17126sooy-36961 */
unsb FtpSkyViewDownDir[ FTP_DIR_MAX_CHAR + 1 ];          /* @17126sooy-36961 */
unsb FtpSkyViewUpDir[ FTP_DIR_MAX_CHAR + 1 ];            /* @17126sooy-36961 */

unsb FtpCFCHostIpAddr[ IPADDR_MAX_CHAR+1 ];              /* @16a28lche-35401 */
unsb FtpCFCUserName[ FTP_USER_MAX_CHAR+1 ];              /* @16a28lche-35401 */
unsb FtpCFCPasswd[ FTP_PASSWD_MAX_CHAR+1 ];              /* @16a28lche-35401 */
unsb FtpCFCUploadDir[ FTP_DIR_MAX_CHAR+1 ];              /* @16a28lche-35401 */

/* GPIB : WPDN, WPUP */                               /* @17111ykim-38258 */
unsb FtpWpXxHostIp[ IPADDR_MAX_CHAR +1 ];             /* @17111ykim-38258 */
unsb FtpWpXxUserName[ FTP_USER_MAX_CHAR +1 ];         /* @17111ykim-38258 */
unsb FtpWpXxPasswd[ FTP_PASSWD_MAX_CHAR +1 ];         /* @17111ykim-38258 */
unsb FtpWpXxDownDir[ FTP_DIR_MAX_CHAR +1 ];           /* @17111ykim-38258 */
unsb FtpWpXxUpDir[ FTP_DIR_MAX_CHAR +1 ];             /* @17111ykim-38258 */

/* DBS Log Parameters                                       @16c23toku-38112 */
unsb FtpDbsLogHostIp[ IPADDR_MAX_CHAR + 1 ];             /* @16c23toku-38112 */
unsb FtpDbsLogUserId[ FTP_USER_MAX_CHAR + 1 ];           /* @16c23toku-38112 */
unsb FtpDbsLogPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];         /* @16c23toku-38112 */
unsb FtpDbsLogUploadDir[ FTP_DIR_MAX_CHAR + 1 ];         /* @16c23toku-38112 */

/* DBS Wafer File Upload Server                             @17220sooy-38653 */
unsb FtpDBSWafFileULHostIp[ IPADDR_MAX_CHAR + 1 ];       /* @17220sooy-38653 */
unsb FtpDBSWafFileULUserName[ FTP_USER_MAX_CHAR + 1 ];   /* @17220sooy-38653 */
unsb FtpDBSWafFileULPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];   /* @17220sooy-38653 */
unsb FtpDBSWafFileULUpDir[ FTP_DIR_MAX_CHAR + 1 ];       /* @17220sooy-38653 */

/* OCR Image Log Send                                       @17630rsas-39375 */
unsb FtpOCRImageLogHostIp[ IPADDR_MAX_CHAR + 1 ];        /* @17630rsas-39375 */
unsb FtpOCRImageLogUserName[ FTP_USER_MAX_CHAR + 1 ];    /* @17630rsas-39375 */
unsb FtpOCRImageLogPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];    /* @17630rsas-39375 */
unsb FtpOCRImageLogUpDir[ FTP_DIR_MAX_CHAR + 1 ];        /* @17630rsas-39375 */

/* ODTP Parameters                                                           */
unsb FtpOdtpIpAddress[ IPADDR_MAX_CHAR + 1 ];            /* @17922dari-39275 */
unsb FtpOdtpUsername[ FTP_USER_MAX_CHAR + 1 ];           /* @17922dari-39275 */
unsb FtpOdtpPassword[ FTP_PASSWD_MAX_CHAR + 1 ];         /* @17922dari-39275 */
unsb FtpOdtpDirectory[ FTP_DIR_MAX_CHAR + 1 ];           /* @17922dari-39275 */

/* Specified Alarm Define File                              @17a05cbel-39126 */
unsb FTPErrSaveLogHostIp[ IPADDR_MAX_CHAR +1 ];          /* @17a05cbel-39126 */
unsb FTPErrSaveLogUserName[ FTP_USER_MAX_CHAR +1 ];      /* @17a05cbel-39126 */
unsb FTPErrSaveLogPasswd[ FTP_PASSWD_MAX_CHAR +1 ];      /* @17a05cbel-39126 */
unsb FTPErrSaveLogDownDir[ FTP_DIR_MAX_CHAR +1 ];        /* @17a05cbel-39126 */
unsb FTPErrSaveLogUpDir[ FTP_DIR_MAX_CHAR +1 ];          /* @17a05cbel-39126 */

/* LotNoFile Download Server                                @18525rsas-39921 */
unsb FtpLotNoFileHostIp[ IPADDR_MAX_CHAR + 1 ];          /* @18525rsas-39921 */
unsb FtpLotNoFileUserName[ FTP_USER_MAX_CHAR + 1 ];      /* @18525rsas-39921 */
unsb FtpLotNoFilePasswd[ FTP_PASSWD_MAX_CHAR + 1 ];      /* @18525rsas-39921 */
unsb FtpLotNoFileDownDir[ FTP_DIR_MAX_CHAR + 1 ];        /* @18525rsas-39921 */

/* ProberFileManagementTable Download Server                @18525rsas-39921 */
unsb FtpPrbFileMngHostIp[ IPADDR_MAX_CHAR + 1 ];         /* @18525rsas-39921 */
unsb FtpPrbFileMngUserName[ FTP_USER_MAX_CHAR + 1 ];     /* @18525rsas-39921 */
unsb FtpPrbFileMngPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];     /* @18525rsas-39921 */
unsb FtpPrbFileMngDownDir[ FTP_DIR_MAX_CHAR + 1 ];       /* @18525rsas-39921 */

/* ECID/SVID query File                                     @18517cbel-40839 */
unsb FTPECSVIDqueryHostIp[ IPADDR_MAX_CHAR +1 ];         /* @18517cbel-40839 */
unsb FTPECSVIDqueryUserName[ FTP_USER_MAX_CHAR +1 ];     /* @18517cbel-40839 */
unsb FTPECSVIDqueryPasswd[ FTP_PASSWD_MAX_CHAR +1 ];     /* @18517cbel-40839 */
unsb FTPECSVIDqueryUpDir[ FTP_DIR_MAX_CHAR +1 ];         /* @18517cbel-40839 */

/* Polisher Hole Recognized Image Upload Server             @18521hden-37272 */
unsb FTPPolHoleSaveImgHostIp[ IPADDR_MAX_CHAR +1 ];      /* @18521hden-37272 */
unsb FTPPolHoleSaveImgUserName[ FTP_USER_MAX_CHAR +1 ];  /* @18521hden-37272 */
unsb FTPPolHoleSaveImgPasswd[ FTP_PASSWD_MAX_CHAR +1 ];  /* @18521hden-37272 */
unsb FTPPolHoleSaveImgUpDir[ FTP_DIR_MAX_CHAR +1 ];      /* @18521hden-37272 */

/* Probe Card Management Server                             @18820rsas-40333 */
unsb FtpPrbCardMngHostIp[ IPADDR_MAX_CHAR + 1 ];         /* @18820rsas-40333 */
unsb FtpPrbCardMngUserName[ FTP_USER_MAX_CHAR + 1 ];     /* @18820rsas-40333 */
unsb FtpPrbCardMngPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];     /* @18820rsas-40333 */
unsb FtpPrbCardMngDownDir[ FTP_DIR_MAX_CHAR + 1 ];       /* @18820rsas-40333 */
unsb FtpPrbCardMngUpDir[ FTP_DIR_MAX_CHAR + 1 ];         /* @18820rsas-40333 */

/* Test Requirement Server                                  @18820rsas-40333 */
unsb FtpTestReqHostIp[ IPADDR_MAX_CHAR + 1 ];            /* @18820rsas-40333 */
unsb FtpTestReqUserName[ FTP_USER_MAX_CHAR + 1 ];        /* @18820rsas-40333 */
unsb FtpTestReqPasswd[ FTP_PASSWD_MAX_CHAR + 1 ];        /* @18820rsas-40333 */
unsb FtpTestReqDownDir[ FTP_DIR_MAX_CHAR + 1 ];          /* @18820rsas-40333 */

/* Probing Path Download Server                                @18b28hden-39869 */
unsb FTPProbPathHostIp[ IPADDR_MAX_CHAR +1 ];               /* @18b28hden-39869 */
unsb FTPProbPathUserName[ FTP_USER_MAX_CHAR +1 ];           /* @18b28hden-39869 */
unsb FTPProbPathPasswd[ FTP_PASSWD_MAX_CHAR +1 ];           /* @18b28hden-39869 */
unsb FTPProbPathDownDir[ FTP_DIR_MAX_CHAR +1 ];             /* @18b28hden-39869 */

/* PMI Pad Image File Upload Server                            @19705yyab-41681 */
unsb FtpPmiPadHostIpAddr[ IPADDR_MAX_CHAR+1 ];              /* @19705yyab-41681 */
unsb FtpPmiPadUserName[ FTP_USER_MAX_CHAR+1 ];              /* @19705yyab-41681 */
unsb FtpPmiPadPasswd[ FTP_PASSWD_MAX_CHAR+1 ];              /* @19705yyab-41681 */
unsb FtpPmiPadUploadDir[ FTP_DIR_MAX_CHAR+1 ];              /* @19705yyab-41681 */

/* Reporbe Bin & Disable Dut for Tester Server                 @19523hden-41561 */
unsb FTPReprbBinDisDutHostIp[ IPADDR_MAX_CHAR +1 ];         /* @19523hden-41561 */
unsb FTPReprbBinDisDutUserName[ FTP_USER_MAX_CHAR +1 ];     /* @19523hden-41561 */
unsb FTPReprbBinDisDutPasswd[ FTP_PASSWD_MAX_CHAR +1 ];     /* @19523hden-41561 */
unsb FTPReprbBinDisDutUpDir[ FTP_DIR_MAX_CHAR +1 ];         /* @19523hden-41561 */

/* Full Die Image Grabbing                                     @18a19wsim-40021 */
unsb FTPFullDieImageHostIp[ IPADDR_MAX_CHAR +1 ];           /* @18a19wsim-40021 */
unsb FTPFullDieImageUserName[ FTP_USER_MAX_CHAR +1 ];       /* @18a19wsim-40021 */
unsb FTPFullDieImagePasswd[ FTP_PASSWD_MAX_CHAR +1 ];       /* @18a19wsim-40021 */
unsb FTPFullDieImageUpDir[ FTP_DIR_MAX_CHAR +1 ];           /* @21604latw-42732 */

/* WAPP Surface Level Accuracy Log Server                      @19722yjer-41530 */
unsb FTPWAPPSurfaceHostIp[ IPADDR_MAX_CHAR +1 ];            /* @19722yjer-41530 */
unsb FTPWAPPSurfaceUserName[ FTP_USER_MAX_CHAR +1 ];        /* @19722yjer-41530 */
unsb FTPWAPPSurfacePasswd[ FTP_PASSWD_MAX_CHAR +1 ];        /* @19722yjer-41530 */
unsb FTPWAPPSurfaceUpDir[ FTP_DIR_MAX_CHAR +1 ];            /* @19722yjer-41530 */

/* Disable FTP Port                                            @19a31hden-41773 */
unsb FTPDisPortHostIp[ IPADDR_MAX_CHAR +1 ];                /* @19a31hden-41773 */
unsb FTPDisPortUserName[ FTP_USER_MAX_CHAR +1 ];            /* @19a31hden-41773 */
unsb FTPDisPortPasswd[ FTP_PASSWD_MAX_CHAR +1 ];            /* @19a31hden-41773 */
unsb FTPDisPortDownDir[ FTP_DIR_MAX_CHAR +1 ];              /* @19a31hden-41773 */
unsb FTPDisPortUpDir[ FTP_DIR_MAX_CHAR +1 ];                /* @19a31hden-41773 */

/* Inspad File Download Server                                 @20309yjer-41225 */
unsb FTPInspadFileHostIp[ IPADDR_MAX_CHAR +1 ];             /* @20309yjer-41225 */
unsb FTPInspadFileUserName[ FTP_USER_MAX_CHAR +1 ];         /* @20309yjer-41225 */
unsb FTPInspadFilePasswd[ FTP_PASSWD_MAX_CHAR +1 ];         /* @20309yjer-41225 */
unsb FTPInspadFileDownDir[ FTP_DIR_MAX_CHAR +1 ];           /* @20309yjer-41225 */

/* Drive Log                                                   @20429hden-42046 */
unsb FTPDriveLogHostIp[ IPADDR_MAX_CHAR +1 ];               /* @20429hden-42046 */
unsb FTPDriveLogUserName[ FTP_USER_MAX_CHAR +1 ];           /* @20429hden-42046 */
unsb FTPDriveLogPasswd[ FTP_PASSWD_MAX_CHAR +1 ];           /* @20429hden-42046 */
unsb FTPDriveLogUpDir[ FTP_DIR_MAX_CHAR +1 ];               /* @20429hden-42046 */

/* Parameter Define File                                       @20408ljoe-40022 */
unsb FTPParFileHostIp[ IPADDR_MAX_CHAR +1 ];                /* @20408ljoe-40022 */
unsb FTPParFileUserName[ FTP_USER_MAX_CHAR +1 ];            /* @20408ljoe-40022 */
unsb FTPParFilePasswd[ FTP_PASSWD_MAX_CHAR +1 ];            /* @20408ljoe-40022 */
unsb FTPParFileDownDir[ FTP_DIR_MAX_CHAR +1 ];              /* @20408ljoe-40022 */

/* Wafer thickness Log                                         @20812cbel-42269 */
unsb FTPWafThickLogHostIp[ IPADDR_MAX_CHAR +1 ];            /* @20812cbel-42269 */
unsb FTPWafThickLogUserName[ FTP_USER_MAX_CHAR +1 ];        /* @20812cbel-42269 */
unsb FTPWafThickLogPasswd[ FTP_PASSWD_MAX_CHAR +1 ];        /* @20812cbel-42269 */
unsb FTPWafThickLogUpDir[ FTP_DIR_MAX_CHAR +1 ];            /* @20812cbel-42269 */

/* Manual PMI Upload Image                                     @20909syul-41647 */
unsb FTPPmiUploadImageHostIp[ IPADDR_MAX_CHAR +1 ];         /* @20909syul-41647 */
unsb FTPPmiUploadImageUserName[ FTP_USER_MAX_CHAR +1 ];     /* @20909syul-41647 */
unsb FTPPmiUploadImagePasswd[ FTP_PASSWD_MAX_CHAR +1 ];     /* @20909syul-41647 */
unsb FTPPmiUploadImageUpDir[ FTP_DIR_MAX_CHAR +1 ];         /* @20909syul-41647 */

/* Shape Model File                                            @20b20tka3-42458 */
unsb FTPShapeModelHostIp[ IPADDR_MAX_CHAR +1 ];             /* @20b20tka3-42458 */
unsb FTPShapeModelUserName[ FTP_USER_MAX_CHAR +1 ];         /* @20b20tka3-42458 */
unsb FTPShapeModelPasswd[ FTP_PASSWD_MAX_CHAR +1 ];         /* @20b20tka3-42458 */
unsb FTPShapeModelUpDir[ FTP_DIR_MAX_CHAR +1 ];             /* @20b20tka3-42458 */

/* Second Host Ip                                                         @20904ceri-42108 */
unsb FTPSecondMapFileHostIp[ IPADDR_MAX_CHAR +1 ];                     /* @20904ceri-42108 */
unsb FTPSecondHostIp[ IPADDR_MAX_CHAR +1 ];                            /* @20904ceri-42108 */
unsb FtpSecondMapDwnHostIp[ MAP_DOWNLOAD_MAX ][ IPADDR_MAX_CHAR + 1 ]; /* @20904ceri-42108 */

/* FTP Log Manager                                            @20b19cjus-41869 */
unsb FTPLogManHostIp[ IPADDR_MAX_CHAR +1 ];                /* @20b19cjus-41869 */
unsb FTPLogManUserName[ FTP_USER_MAX_CHAR +1 ];            /* @20b19cjus-41869 */
unsb FTPLogManPasswd[ FTP_PASSWD_MAX_CHAR +1 ];            /* @20b19cjus-41869 */
unsb FTPLogManUpDir[ FTP_DIR_MAX_CHAR +1 ];                /* @20b19cjus-41869 */

/* Wafer Stability Log                                        @20915lkay-41851 */
unsb FTPWafStbLogHostIp[ IPADDR_MAX_CHAR +1 ];             /* @20915lkay-41851 */
unsb FTPWafStbLogUserName[ FTP_USER_MAX_CHAR +1 ];         /* @20915lkay-41851 */
unsb FTPWafStbLogPasswd[ FTP_PASSWD_MAX_CHAR +1 ];         /* @20915lkay-41851 */
unsb FTPWafStbLogUpDir[ FTP_DIR_MAX_CHAR +1 ];             /* @20915lkay-41851 */

/* Basic Probe Alignment Log                                  @21218lkay-41608 */
unsb FTPBasicPrbAlnLogHostIp[ IPADDR_MAX_CHAR +1 ];        /* @21218lkay-41608 */
unsb FTPBasicPrbAlnLogUserName[ FTP_USER_MAX_CHAR +1 ];    /* @21218lkay-41608 */
unsb FTPBasicPrbAlnLogPasswd[ FTP_PASSWD_MAX_CHAR +1 ];    /* @21218lkay-41608 */
unsb FTPBasicPrbAlnLogUpDir[ FTP_DIR_MAX_CHAR +1 ];        /* @21218lkay-41608 */

/* ParameterList                                              @21308khos-42596 */
unsb FTPParameterListHostIp[ IPADDR_MAX_CHAR +1 ];         /* @21308khos-42596 */
unsb FTPParameterListUserName[ FTP_USER_MAX_CHAR +1 ];     /* @21308khos-42596 */
unsb FTPParameterListPasswd[ FTP_PASSWD_MAX_CHAR +1 ];     /* @21308khos-42596 */
unsb FTPParameterListDownDir[ FTP_DIR_MAX_CHAR + 1 ];      /* @21308khos-42596 */
unsb FTPParameterListUpDir[ FTP_DIR_MAX_CHAR +1 ];         /* @21308khos-42596 */

/* Shot Alignment Log                                         @20a23cbel-42254 */
unsb FTPShotAligLogHostIp[ IPADDR_MAX_CHAR +1 ];           /* @20a23cbel-42254 */
unsb FTPShotAligLogUserName[ FTP_USER_MAX_CHAR +1 ];       /* @20a23cbel-42254 */
unsb FTPShotAligLogPasswd[ FTP_PASSWD_MAX_CHAR +1 ];       /* @20a23cbel-42254 */
unsb FTPShotAligLogUpDir[ FTP_DIR_MAX_CHAR +1 ];           /* @20a23cbel-42254 */

/* Regular Log                                                @21409cjus-42418 */
unsb FTPRegularLogHostIp[ IPADDR_MAX_CHAR +1 ];            /* @21409cjus-42418 */
unsb FTPRegularLogUserName[ FTP_USER_MAX_CHAR +1 ];        /* @21409cjus-42418 */
unsb FTPRegularLogPasswd[ FTP_PASSWD_MAX_CHAR +1 ];        /* @21409cjus-42418 */
unsb FTPRegularLogUpDir[ FTP_DIR_MAX_CHAR +1 ];            /* @21409cjus-42418 */

/*------------------------------------------------------------------*/
/*         毎チップアライメント機能                                 */
/*         every chip align                           @7918sfuk-01  */
/*------------------------------------------------------------------*/
intl everyChipAlnStatus = 0;
/*@@@ @7818mshi-02 @* マイチップアライメント機能状態 */
/*@@@ @7818mshi-02  * 0 : no operation */
/*@@@ @7818mshi-02  * 1 : first chip at lot */
/*@@@ @7818mshi-02  * 2 : first chip after re-start */
/*@@@ @7818mshi-02  * 3 : 1 pin alignment */
/*@@@ @7818mshi-02  * 4 : total alignment */
/*@@@ @7818mshi-02  *@ */

intl everyChipAlnDoing = 0;
/*@@@ @7818mshi-02 @* マイチップアライメント実行中フラグ */
/*@@@ @7818mshi-02  * 1 : 実行中 */
/*@@@ @7818mshi-02  * 2 : 毎チップアライメントはスキップせよ */
/*@@@ @7818mshi-02  * 0 : 上記以外 */
/*@@@ @7818mshi-02  *@ */

intl everyChipAlnAssist = 0;
/* 毎ちっぷあらいめんとアシスト発生中 = 1 */

intl everyChipAlnWillEnd = 0;

intl everyChipAlnCounter = 0;
/* 終了条件連続カウント */

intl everyChipXYZSave[3];

time_t TEST_START_TIME;
/* チップ測定開始時間セーブエリア */

time_t TEST_CMPLT_TIME;
/* チップ測定終了時間セーブエリア */
 
intl EVERY_CHIP_DEBUG = 0;/* easy debug */
intl GCS3_AUTOR_DEBUG = 0;/* =1 : Forced Verify OK. (UD122&Prober's data) */ /* @02806amat-8200 */
 
/* Gateway address for NEC  @7a21hkaw */
unsb    NetGatewayAddr[20];                            /* @7a21hkaw */

/* 毎チップアライメント時間間隔可変用のSTEP数    @06620nwat-17000 */
intl EveryChipAlnStep = 0;                    /* @06620nwat-17000 */

intl NIndexChipAlnTime = 0;                             /* @10922tnak-26656 */

/* Step Interval by Index                               @11930twa3-27825 */
intl StepIdxCounter = 0;        /* step index counter   @11930twa3-27825 */
intl EveryChipAlnIdxStep = 0;   /* step index           @11930twa3-27825 */
intl LastAlignIndex = 0;        /* last align index     @11930twa3-27825 */
intl TotalStepsCnt = 0;         /* total index steps    @11930twa3-27825 */

/*------------------------------------------------------------------*/
/* 動パラ ランプコントロール追加                          @7a29mfuj */
/*------------------------------------------------------------------*/
unsb OnLamp = 0;                   /* 点灯しているランプ  @7a29mfuj */
unsb OnOffLamp = 0;                /* 点滅しているランプ  @7a29mfuj */
/*@@@ @7818mshi-02 @* ランプステータスフラグ（現在のランプの状態を表します） */
/*@@@ @7818mshi-02  * 0x10 : 赤ランプ */
/*@@@ @7818mshi-02  * 0x20 : 橙ランプ */
/*@@@ @7818mshi-02  * 0x40 : 緑ランプ */
/*@@@ @7818mshi-02  * 0x80 : オプション */
/*@@@ @7818mshi-02  * ランプコントロールタスクでのみ使用します。 */
/*@@@ @7818mshi-02  * 他のタスクから参照する場合はCheckLampControlTaskState()を */
/*@@@ @7818mshi-02  * 使用して下さい。 */
/*@@@ @7818mshi-02  *@ */
 
unsb OnLamp_WHITE = 0;                                 /* @21409etak-42611 */
unsb OnOffLamp_WHITE = 0;                              /* @21409etak-42611 */

unsb LampStatusModeOnOff[LMP_STATUS_ID_MAX];          /* @7a29mfuj */
/*@@@ @7818mshi-02 @* 動作実行モードフラグ */
/*@@@ @7818mshi-02  * ０   : 終了 */
/*@@@ @7818mshi-02  * ！０ : 開始 */
/*@@@ @7818mshi-02  * ランプコントロールタスクでのみ使用します。 */
/*@@@ @7818mshi-02 *他のタスクから参照する場合はCheckLampControlTaskState()を */
/*@@@ @7818mshi-02  * 使用して下さい。 */
/*@@@ @7818mshi-02  *@ */
 
unsb LampStatusMode[LMP_STATUS_ID_MAX];               /* @7a29mfuj */
/*@@@ @7818mshi-02 @* ＰＣで作成したランプ点灯条件の保存変数 */
/*@@@ @7818mshi-02  * 0x01 : 赤ランプ点滅 */
/*@@@ @7818mshi-02  * 0x02 : 橙ランプ点滅 */
/*@@@ @7818mshi-02  * 0x04 : 緑ランプ点滅 */
/*@@@ @7818mshi-02  * 0x08 : オプションランプ点滅 */
/*@@@ @7818mshi-02  * 0x10 : 赤ランプ点灯 */
/*@@@ @7818mshi-02  * 0x20 : 橙ランプ点灯 */
/*@@@ @7818mshi-02  * 0x40 : 緑ランプ点灯 */
/*@@@ @7818mshi-02  * 0x80 : オプションランプ点灯 */
/*@@@ @7818mshi-02  *@ */

intl PowerOnInitFlag = 0;                            /* @7a29mfuj */
/*@@@ @7818mshi-02 @* ０：パワーオンイニシャル未実行 */
/*@@@ @7818mshi-02    １：パワーオンイニシャル実行済 */
/*@@@ @7818mshi-02 *@ */
 
intl LampInitWafer = 0;                              /* @7a29mfuj */
/* ！０：イニシャルウェーハ処理中 */
/* アライメント終了時に０になります */
 
intl LampUnLoad = 0;                                 /* @7a29mfuj */
/* ！０：強制終了処理中 */

intl    LampStatusModeInitial = 0;      /* @06126ssas-16443 */
intl    LampStatusModeBeforeError = 0;  /* @06126ssas-16443 */

/*-------------------------------------------------------------------*/
/* Check Contact prior to test　For NEC           @8217mfuk          */  
/*-------------------------------------------------------------------*/
intl    GpContNECF = 0;                        /* @8217mfuk */
intw    MmiNECPosX = 0;       /* Stop Coodinate X @05412mfuk-14351 */
intw    MmiNECPosY = 0;       /* stop Coodinate Y @05412mfuk-14351 */
intl    MmiNECMovX;                            /* @05412mfuk-14351 */
intl    MmiNECMovY;                            /* @05412mfuk-14351 */

/*-------------------------------------------------------------------*/
/* Data log mode for FUJITSU                            @8401habe-01 */
/*-------------------------------------------------------------------*/
/*  DLMNORMALMODE  0x00000001 : Normal GPIB mode.                    */
/*  DLMWNEDMODE    0x00000002 : Wafer end mode. U,u,DLM receive mode.*/
/*  DLMEXECMODE    0x00000004 : Data log mode.                       */
unsl    DataLogModeF = 0;                            /* @8401habe-01 */

/* Display Stop Menu After PMI Judgment Error for SONY  @8c11mfuk */
intl ExePmiErrStop = 0;                              /* @8c11mfuk */
 
/* Inker Limit Warning for HITACHI MIP    @9205jsat-03 */
intl  HvInkChangeF = 0;                /* @9205jsat-03 */

/*-------------------------------------------------------------------*/
/* received data by H command (for TI)                @9329hkaw-1334 */
/*-------------------------------------------------------------------*/
tyHcommParamV HcommParamV;                         /* @9329hkaw-1334 */

/* GPIB C-Fail CheckBack Flag             @9330jsat-1723 */
intl  CFCheckBackF = 0;                /* @9330jsat-1723 */

/* @9528mfuk add for KLA */
/* @9b01ymiu-3012 This parameter is moving to aApllPara.c */
/* @9b01ymiu-3012 intl    STKeyChange = 1; @* @9528mfuk */

/* For AMD, Continuity Plate Check End ( Received CPE ) @9623jsat-0820 */
intl    GpCpeRecvF = 0;                              /* @9623jsat-0820 */

/* IBM RDP to support EXCSTMAP   @9701win-14 */
intl    CMapEFlag = 0;      /* @9701win-14 */

/*-------------------------------------------------------------------*/
/* PMI Unload Stop Flag                                @9917mfuk-563 */
/*-------------------------------------------------------------------*/
intl    PmiUnldStopF = 0;                           /* @9917mfuk-563 */

/* Stage Heat Saturation Timer Saved Data ( SPO_PC_BENDING ) @9b11jsat-2269 */
intl    SaveStgHeatSaturTime = 0;                         /* @9b11jsat-2269 */
intl    everyChipAlignNow = 0;                            /* @9c14jsat-2269 */

/*-------------------------------------------------------------------*/
/* GPIB FC command mode for SANSEI                    @9c21habe-2659 */
/*-------------------------------------------------------------------*/
/* GPFCMODEOFF    0x00000000 : Normal mode.           @9c21habe-2659 */
/* GPFCMODEON     0x00000001 : FC command mode.       @9c21habe-2659 */
unsl    GpFcComMode = 0;                           /* @9c21habe-2659 */

/*-------------------------------------------------------------------*/
/* Mark & IDI before marking for sharp          @9b26mfuk-1961       */
/*-------------------------------------------------------------------*/
intl    IdiBefMarkF = 0;                     /* @9b26mfuk-1961 */

intl    PassNetStopF = 0;                          /* @9c20tara-2130 */

/*-------------------------------------------------------------------*/
/* "First Wafer Unload Stop" is already executed      @00104mfuj-3240 */
/*-------------------------------------------------------------------*/
intl    Unload1stStopIsEnd = 0;                    /* @00104mfuj-3240 */

/* @05a12ttak-16320 @*-------------------------------------------------------------------*/
/* @05a12ttak-16320 @* Initial Die Moved Flag for StartPreHeat           @00117jsat-2962 */
/* @05a12ttak-16320 @*-------------------------------------------------------------------*/
/* @05a12ttak-16320 intl    InitDieMovedF = 0;                        @* @00117jsat-2962 */

/* @06620ttak-17003-4 intl    StartPreHeatDo = 0;                       @* @00117jsat-2962 */

/*---------------------------------------------------------*/
/* Remote Command Control Flag                             */
/*---------------------------------------------------------*/
intl    RemoteContFlag;                  /* @00204tara GEM */

/*-------------------------------------------------------------------*/
/* C-Fail CheckBack Limit Flag for MATSUSHITA        @00308jsat-3211 */
/*-------------------------------------------------------------------*/
intl    CFBackLimitF = 0;                         /* @00308jsat-3211 */

/* Fail-More flags for TOSHIBA                               @00328mfuk-3771 */
intl    FailMoreCount;                                    /* @00328mfuk-3771 */
intl    FailMoreDo;                                       /* @00328mfuk-3771 */

/* Multi FailMore Flags for TOSHIBA                          @06123cheo-15822 */
intl    FailMoreCountMul[64];                             /* @06123cheo-15822 */
unsb    MultiFailMorePF=0;                                /* @06123cheo-15822 */
unsb    MultiFailMoreOnWaf=0;                             /* @06123cheo-15822 */
unsb    MultiFailMoreBin[40];                             /* @06123cheo-15822 */
unsl    MulpassDut[3];                                    /* @06123cheo-15822 */
unsl    MulpassDutL[3];                                   /* @06123cheo-15822 */

intl    TosCatCount[3];                            /* @00113mfuk-2737 */

/* ST Wafer File Auto Recieve Flag                    @00516jsat-4013 */
intl    WafFileAutoRecvST = 0;                     /* @00516jsat-4013 */

/*------------------------------------------------------------------------*/
/* Recount ink dot after IDC verify check for SEIKO-EPSON @00613tsat-3445 */
/*------------------------------------------------------------------------*/
intl    InkRecountExec = 0;                            /* @00613tsat-3445 */
                            /* 0 : not exec               @00613tsat-3445 */
                            /* 1 : exec recount           @00613tsat-3445 */
/* @04b05ttak-12952-2 @*-------------------------------------------------------------------*/
/* @04b05ttak-12952-2 @* Execute all-pad PMI at 1st chip                                   */
/* @04b05ttak-12952-2 @*-------------------------------------------------------------------*/
/* @04b05ttak-12952-2 intl    Exe1stPMIDieF=0;                          @* @00921mfuk-3067 */

/*-------------------------------------------------------------------*/
/* polish wafer position for Special option "Needle Polish"          */
/*-------------------------------------------------------------------*/
unsl    PolWafPosF = 0x00000000;                  /* @00a02mfuj-4633 */
unsl    PolWafPosF2 = 0x00000000;                /* @06808yya2-17390 */
unsl    PolWafPosF3 = 0x00000000;                /* @06808yya2-17390 */

/*-------------------------------------------------------------------*/
/*  ChipInfo/RChipInfo malloc error                  @00a25jsat-3848 */
/*-------------------------------------------------------------------*/
intl    BootMallocError = 0;                      /* @00a25jsat-3848 */

/*------------------------------------------------------------------*/
/* Bin Limit Stop Status                            @01118rman-4490 */
/*------------------------------------------------------------------*/
intl    BLSStat;                                 /* @01118rman-4490 */
intl    BLSBackStop = 0;                         /* @01118rman-4490 */

#if ( P12XL )               /* @00a13mshi-4987 */
intl    SysBlpMode = 1;     /* @00a13mshi-4987 */
#endif                      /* @00a13mshi-4987 */

/*------------------------------------------------------------------------*/
/* Kind of AL201(wafer parameter change request)          @01315tsat-3282 */
/*------------------------------------------------------------------------*/
intl    KindOfAL201 = 0;                               /* @01315tsat-3282 */
             /* 1 : when HITACHI lot parameter inputed    @01315tsat-3282 */
             /* 0 : else above                            @01315tsat-3282 */

/* Check if retest result of C-Fail CheckBack Die changed.   @01413ttak-5649 */
intl    CFBResultChanged = 0;                             /* @01413ttak-5649 */

/* Save last "CFailData.failCount" for SPO_CFAIL_CHECKBACK.   @01530ttak-6386 */
unsl    CFailLastCount = 0;                                /* @01530ttak-6386 */

/* NEC Result Map FTP Send Flag                  *//* @01820ttak-5211 */
intl    NecFtpRMapSend = 0;                        /* @01820ttak-5211 */

/* NEC PC Network Error Check Flag               *//* @01910ttak-5211 */
intl    NecPCNetworkErr = 0;                       /* @01910ttak-5211 */

/*---------------------------------------------------------*/
/* Contact Count Limit Flag                                */
/*---------------------------------------------------------*/
intl    ContactLmtF = 0;                /* @02121masw-6910 */
intl    ContactCountResetFlag = 1;      /* @16811jpoc-36566 */
/*------------------------------------------------------------------------*/
/* Pop server RS232C communication for TOSHIBA            @02225habe-7452 */
/*------------------------------------------------------------------------*/
intl    PopContTask = 0;                               /* @02225habe-7452 */
intl    PopCommF = 0;                                  /* @02225habe-7452 */
intl    PopCommRetry = 0;                              /* @02225habe-7452 */
intl    PopDebug = 0;                                  /* @02225habe-7452 */

/*------------------------------------------------------------------------*/
/* RsEnhance Start cast No. For IBM   @02604habe-7981 */
/*------------------------------------------------------------------------*/
unsl    RsECastStart = 0;          /* @02604habe-7981 */

/*-------------------------------------------------------------------------*/
/* cassette end indicate( for HITACHI consective lot)      @02a17rnis-6077 */
/*-------------------------------------------------------------------------*/
intl    CstEndIndicate = 0;                             /* @02a17rnis-6077 */
             /* 0 : not indicate cassette end              @02a17rnis-6077 */
             /* 1 : indicate cassette end                  @02a17rnis-6077 */
             /* 2 : indicate cassette end on stop mesuring @02a17rnis-6077 */

/*-------------------------------------------------------------------------*/
/* Cassette status disply. ( for HITACHI consecutive lot)  @02a17rnis-6077 */
/*-------------------------------------------------------------------------*/
intl    CstDspSts[ 2 ];                                 /* @02a17rnis-6077 */
        /* CstDspSts[ 0 ] : Cassette 1                     @02a17rnis-6077 */
        /* CstDspSts[ 1 ] : Cassette 2                     @02a17rnis-6077 */
        /* BLANK( = 0 )      : Blank                       @02a17rnis-6077 */
        /* WAITING( = 1 )    : Waiting                     @02a17rnis-6077 */
        /* PROCESSING( = 2 ) : Processing                  @02a17rnis-6077 */
        /* Completed( = 3 )  : Completed                   @02a17rnis-6077 */

/* @13c03kish-30987 #if ( HOSTFUNC && GEM && P12XL )            @* @01309mshi-4925 */
#if ( HOSTFUNC && GEM )                     /* @13c03kish-30987 */
unsl    NetCMSFlag = 0;                     /* @01309mshi-4925 */
unsl    CMSReserveF = 0;                	/* @01309mshi-4925 */
#endif                                      /* @01309mshi-4925 */

intl    GEMChangeNameFlag = 0;     /* @01621hkaw-5522 */
/* If this flag is ON, it means prober is switching   */
/* wafer setup files. This sequence will be triggered */
/* by the remote command PP-SELECT.                   */

/*-------------------------------------------------------------------*/
/* Execute Continuity check                          @03124myam-8139 */
/*-------------------------------------------------------------------*/
intl ContiCheckExeFlag = 0;                       /* @03124myam-8139 */
            /* 0 : not execute continuity check      @03124myam-8139 */
            /* 1 : execute continuity check          @03124myam-8139 */

/*------------------------------------------------------------------*/
/* まーくでーたちぇっく判定結果データエリア   @03413mwat-9082       */
/*------------------------------------------------------------------*/
unsl    markCateData = 0x0000000;       /* @03413mwat-9082 */
unsl    markTotalData = 0x0000000;       /* @03413mwat-9082 */
unsl    markFirstData = 0x0000000;       /* @03413mwat-9082 */
unsl    markMapExist  = 0x0000000;       /* @04c24tsat-12601 */

/*------------------------------------------------------------------------*/
/* Result for polish sheet layout check assist            @03327myam-8248 */
/*------------------------------------------------------------------------*/
intl    LayoutCheckResult = 0;                         /* @03327myam-8248 */
             /* ONE_POLSHEET : one polish sheet           @03327myam-8248 */
             /* TWO_POLSHEET_VERTI : two sheets of vertical form    @03327myam-8248 */
             /* TWO_POLSHEET_HORIZ : two sheets of horizontal form  @03327myam-8248 */
/*------------------------------------------------------------------------*/
/* Polish mode flag whether polish sheet layout is checked  @03327myam-8248 */
/*------------------------------------------------------------------------*/
intl    NotLayoutCheckPolMode = 0;                     /* @03327myam-8248 */
             /* 0 : contain polish mode "polisher"        @03327myam-8248 */
             /* 1 : not contain polish mode "polisher"    @03327myam-8248 */

/* GMS mapfile WaferID  For PSC */             /* @02b18yita-8196 */
intb GmsMapFileWaferID[32];                    /* @02b18yita-8196 */

#if( PC300 )                                           /* @03527skoj-9478 */
/*------------------------------------------------------------------------*/
/* "Before Last Wafer Unload Stop" is already exec.       @03527skoj-9478 */
/*------------------------------------------------------------------------*/
intl    BefLastWafUnldEnd = 0;                         /* @03527skoj-9478 */
#endif                                                 /* @03527skoj-9478 */

/* UD102 Lot End  S6F9,S12F15 Send or not. */  /* @02806tkob-8199 */
intl Ud102WafEndNotSend = 0;                   /* @02806tkob-8199 */
intl Ud102EndOnly = 0;                         /* @02806tkob-8199 */

/* Q/R Flag  For PSC UD108,UD109 Commnad */    /* @02806tkob-8199 */
intb QRFlagLot[16];                            /* @02806tkob-8199 */
intb QRFlagLotMessage[65];                     /* @02806tkob-8199 */
intb QRFlagWafer[16];                          /* @02806tkob-8199 */
intb QRFlagWaferMessage[65];                   /* @02806tkob-8199 */

/* Sence SW For PSC UD101 Command */           /* @02806tkob-8199 */
intb SenceSW[6];                               /* @02806tkob-8199 */

unsl ReProbeAlignFlag = 0; /* Alignment flag before ReProbe @03430kisi-10273 @03604mtog-10273 */

#if( PC300 )                                                  /* @02409myam-6959 */
/* Authorized BIN Number Convert Data */                      /* @02409myam-6959 */
unsb AuthorizedBinNumberConv[ AUTHORIZED_BIN_NUM_CONV_SIZE ]; /* @02409myam-6959 */
unsb UnRetestBinNumberConv[ AUTHORIZED_BIN_NUM_CONV_SIZE ];   /* @15907tsan-31943 */

/* Reprobe by BIN Number Convert Data */                      /* @02409myam-6959 */
/* @04c05tkan-11633 unsb ReprobeBinNumberConv[ REPROBE_BIN_NUM_SIZE ][ AUTHORIZED_BIN_NUM_CONV_SIZE ]; @* @02409myam-6959 */
unsb ReprobeBinNumberConv[ SIZE_513_GROUP ][ AUTHORIZED_BIN_NUM_CONV_SIZE ]; /* @04c05tkan-11633 */

#if ( P12XL )                                                                 /* @09730fmor-22426 */
unsb ReprobeBinNumberConv2[ SIZE_513_GROUP ][ AUTHORIZED_BIN_NUM_CONV_SIZE ]; /* @09730fmor-22426 */
#endif                                                                        /* @09730fmor-22426 */

/* Consecutive Fail by BIN Number Convert Data */             /* @02409myam-6959 */
unsb CFailBinNumberConv[ CFAIL_BIN_NUM_SIZE ][ AUTHORIZED_BIN_NUM_CONV_SIZE ]; /* @02409myam-6959 */
unsb CFailRepBinNumberConv[ CFAIL_BIN_NUM_SIZE ][ AUTHORIZED_BIN_NUM_CONV_SIZE ]; /* @12810tsan-29342 */
#endif                                                        /* @02409myam-6959 */

/* Reprobe by Fail Sum BIN Number Convert Data */                                         /* @20603lkay-41796 */
unsb ReprobeFailSumBinNumberConv[ REPROBE_BIN_NUM_SIZE ][ AUTHORIZED_BIN_NUM_CONV_SIZE ]; /* @20603lkay-41796 */

/* Ftp File Table @02a24hkaw-8850 */
#if (WAFERNAME32)                                                                                                  /* @03724jsat-8850 */
unsb WaferNameDir[ WAFERNAME_SIZE ];                                                                               /* @03724jsat-8850 */
#else                                                                                                              /* @03724jsat-8850 */
unsb WaferNameDir[21];                                                                                             /* @02a24hkaw-8850 */
#endif                                                                                                             /* @03724jsat-8850 */
tyFtpFileInfo FtpFileInfo[] = {                                                                                    /* @02a24hkaw-8850 */
/*      category   |   local dir | remote sub dir1.     2              3       |   ext. |  dest. name   */         /* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_MAS,     MAS_DIR, { SUBDIR_HD_MAS, SUBDIR_GC_MAS, WaferNameDir }, MAS_EXT, ApMasterFile },          @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_APL,     APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, APL_EXT, ApWaferFile },           @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_CMAP,    APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, CMAP_EXT, ApWaferFile },          @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_APL,     APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, DBS_EXT, ApWaferFile },           @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_WAF,     WAF_DIR, { SUBDIR_HD_WAF, SUBDIR_GC_WAF, WaferNameDir }, WAF_EXT, ApAlignFile },           @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_WAF,     WTN_DIR, { SUBDIR_HD_WTN, SUBDIR_GC_WAF, WaferNameDir }, WTN_EXT, ApAlignFile },           @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_PRB,     PRB_DIR, { SUBDIR_HD_PRB, SUBDIR_GC_PRB, WaferNameDir }, PRB_EXT, ApProbeFile },           @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_PRB,     PTN_DIR, { SUBDIR_HD_PTN, SUBDIR_GC_PRB, WaferNameDir }, PTN_EXT, ApProbeFile },           @* @02a24hkaw-8850 */
/* @04318yita-11618     { CATEGORY_PRB,     PIN_DIR, { SUBDIR_HD_PIN, SUBDIR_GC_PRB, WaferNameDir }, PNT_EXT, ApProbeFile },           @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_CON,     CON_DIR, { SUBDIR_HD_CON, SUBDIR_GC_COT, WaferNameDir }, CON_EXT, ApContactFile },         @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_MAK,     MAK_DIR, { SUBDIR_HD_MAK, SUBDIR_GC_MRK, WaferNameDir }, MAK_EXT, ApMarkFile },            @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_IDIIDC,  INP_DIR, { SUBDIR_HD_INP, SUBDIR_GC_WAF, WaferNameDir }, IDP_EXT, ApAlignFile },           @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_PMI,     INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, PMT_EXT, ApAlignFile },           @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_PMI,     INP_DIR, { SUBDIR_HD_INP, SUBDIR_GC_WAF, WaferNameDir }, PDT_EXT, ApAlignFile },           @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_PMI,     INP_DIR, { SUBDIR_HD_INP, SUBDIR_GC_WAF, WaferNameDir }, PMP_EXT, ApAlignFile },           @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_KLAOCR,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, KLA_EXT, NULL },                  @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_ACUOCR,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, ACU_EXT, NULL },                  @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_ACUOCR,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, PRJ_EXT, NULL },                  @* @02a24hkaw-8850 */
/* 08b05kisi-23604    { CATEGORY_ISOCR,   OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, IS_EXT,  NULL },                  @* @02716tsat-8466 */
/* 08b05kisi-23604    { CATEGORY_CMAP,    APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, MCD_EXT, ApWaferFile },           @* @03724jsat-8850 */
/* 08b05kisi-23604    { CATEGORY_IDIIDC,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, IDI_EXT, ApWaferFile },           @* @04810msai-12039 */
/* 08b05kisi-23604    { CATEGORY_IDIIDC,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, IDC_EXT, ApWaferFile },           @* @04810msai-12039 */
/* 08b05kisi-23604    { CATEGORY_CMAP,    APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, BOT_EXT, ApWaferFile }, @* @04b04tkaj-11633 */
/* 08b05kisi-23604    { CATEGORY_PMI,     INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, PMI_EXT, ApAlignFile },           @* @04318yita-11618 */
/* 08b05kisi-23604    { CATEGORY_PIN,     PIN_DIR, { SUBDIR_HD_PIN, SUBDIR_GC_PRB, WaferNameDir }, PNT_EXT, ApProbeFile },           @* @04318yita-11618 */
/* 08b05kisi-23604  { CATEGORY_OPR,     OPR_DIR, { SUBDIR_HD_OPR, SUBDIR_GC_OPR, WaferNameDir }, OPR_EXT, ApOprFile },@* @08625nshi-20961 */
/* 08b05kisi-23604    { NULL,             NULL,    { NULL,          NULL,          NULL         }, NULL,    NULL }   @* TERMINATOR *//* @02a24hkaw-8850 */

/*      category   |   group     | local dir | remote sub dir1.     2              3       |   ext. |  dest. name   */         /* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_MAS,     GROUP_NOT,  MAS_DIR, { SUBDIR_HD_MAS, SUBDIR_GC_MAS, WaferNameDir }, MAS_EXT, ApMasterFile },          @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_APL,     GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, APL_EXT, ApWaferFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_CMAP,    GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, CMAP_EXT, ApWaferFile },          @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_APL,     GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, DBS_EXT, ApWaferFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_WAF,     GROUP_WAF,  WAF_DIR, { SUBDIR_HD_WAF, SUBDIR_GC_WAF, WaferNameDir }, WAF_EXT, ApAlignFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_WAF,     GROUP_WTN,  WTN_DIR, { SUBDIR_HD_WTN, SUBDIR_GC_WAF, WaferNameDir }, WTN_EXT, ApAlignFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_PRB,     GROUP_PRB,  PRB_DIR, { SUBDIR_HD_PRB, SUBDIR_GC_PRB, WaferNameDir }, PRB_EXT, ApProbeFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_PRB,     GROUP_PTN,  PTN_DIR, { SUBDIR_HD_PTN, SUBDIR_GC_PRB, WaferNameDir }, PTN_EXT, ApProbeFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_CON,     GROUP_CON,  CON_DIR, { SUBDIR_HD_CON, SUBDIR_GC_COT, WaferNameDir }, CON_EXT, ApContactFile },         @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_MAK,     GROUP_NOT,  MAK_DIR, { SUBDIR_HD_MAK, SUBDIR_GC_MRK, WaferNameDir }, MAK_EXT, ApMarkFile },            @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_IDIIDC,  GROUP_INP,  INP_DIR, { SUBDIR_HD_INP, SUBDIR_GC_WAF, WaferNameDir }, IDP_EXT, ApAlignFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_PMI,     GROUP_INS,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, PMT_EXT, ApAlignFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_PMI,     GROUP_INP,  INP_DIR, { SUBDIR_HD_INP, SUBDIR_GC_WAF, WaferNameDir }, PDT_EXT, ApAlignFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_PMI,     GROUP_INP,  INP_DIR, { SUBDIR_HD_INP, SUBDIR_GC_WAF, WaferNameDir }, PMP_EXT, ApAlignFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_KLAOCR,  GROUP_NOT,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, KLA_EXT, NULL },                  @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_ACUOCR,  GROUP_NOT,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, ACU_EXT, NULL },                  @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_ACUOCR,  GROUP_NOT,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, PRJ_EXT, NULL },                  @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_ISOCR,   GROUP_NOT,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, IS_EXT,  NULL },                  @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_CMAP,    GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, MCD_EXT, ApWaferFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_IDIIDC,  GROUP_INS,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, IDI_EXT, ApWaferFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_IDIIDC,  GROUP_INS,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, IDC_EXT, ApWaferFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_CMAP,    GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, BOT_EXT, ApWaferFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_PMI,     GROUP_INS,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, PMI_EXT, ApAlignFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_PIN,     GROUP_NOT,  PIN_DIR, { SUBDIR_HD_PIN, SUBDIR_GC_PRB, WaferNameDir }, PNT_EXT, ApProbeFile },           @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_PMI,     GROUP_NOT,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, PDS_EXT, ApWaferFile },           @* @09603tyam-24059 */
/* @17111ykim-38258     { CATEGORY_OPR,     GROUP_NOT,  OPR_DIR, { SUBDIR_HD_OPR, SUBDIR_GC_OPR, WaferNameDir }, OPR_EXT, ApOprFile },             @* 08b05kisi-23604 */
/* @17111ykim-38258     { CATEGORY_ISOCR,   GROUP_NOT,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, JOB_EXT,  NULL },                 @* @11224sna2-23950 */
/* @17111ykim-38258     { CATEGORY_CMAP,    GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, BNA_EXT, ApWaferFile },           @* @12c10ykas-29050 */
/* @17111ykim-38258     { CATEGORY_CMAP,    GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, SCMAP_EXT, ApWaferFile },         @* @14326sooy-32144 */
/* @17111ykim-38258     { NULL,             NULL,       NULL,    { NULL,          NULL,          NULL         }, NULL,    NULL }   @* TERMINATOR *@@* @17111ykim-38258 */
    
/*      category   |   group     | local dir | remote sub dir1.     2              3       |   ext. |  dest. name   |  GPIB WPXX Dest Name */   /* @17111ykim-38258 */
    { CATEGORY_MAS,     GROUP_NOT,  MAS_DIR, { SUBDIR_HD_MAS, SUBDIR_GC_MAS, WaferNameDir }, MAS_EXT,   ApMasterFile,   ApWpxxMasterFile    },  /* @17111ykim-38258 */
    { CATEGORY_APL,     GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, APL_EXT,   ApWaferFile,    ApWpxxWaferFile     },  /* @17111ykim-38258 */
    { CATEGORY_CMAP,    GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, CMAP_EXT,  ApWaferFile,    ApWpxxWaferFile     },  /* @17111ykim-38258 */
    { CATEGORY_APL,     GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, DBS_EXT,   ApWaferFile,    ApWpxxWaferFile     },  /* @17111ykim-38258 */
    { CATEGORY_WAF,     GROUP_WAF,  WAF_DIR, { SUBDIR_HD_WAF, SUBDIR_GC_WAF, WaferNameDir }, WAF_EXT,   ApAlignFile,    ApWpxxAlignFile     },  /* @17111ykim-38258 */
    { CATEGORY_WAF,     GROUP_WTN,  WTN_DIR, { SUBDIR_HD_WTN, SUBDIR_GC_WAF, WaferNameDir }, WTN_EXT,   ApAlignFile,    ApWpxxAlignFile     },  /* @17111ykim-38258 */
    { CATEGORY_PRB,     GROUP_PRB,  PRB_DIR, { SUBDIR_HD_PRB, SUBDIR_GC_PRB, WaferNameDir }, PRB_EXT,   ApProbeFile,    ApWpxxProbeFile     },  /* @17111ykim-38258 */
    { CATEGORY_PRB,     GROUP_PTN,  PTN_DIR, { SUBDIR_HD_PTN, SUBDIR_GC_PRB, WaferNameDir }, PTN_EXT,   ApProbeFile,    ApWpxxProbeFile     },  /* @17111ykim-38258 */
    { CATEGORY_CON,     GROUP_CON,  CON_DIR, { SUBDIR_HD_CON, SUBDIR_GC_COT, WaferNameDir }, CON_EXT,   ApContactFile,  ApWpxxContactFile   },  /* @17111ykim-38258 */
    { CATEGORY_MAK,     GROUP_NOT,  MAK_DIR, { SUBDIR_HD_MAK, SUBDIR_GC_MRK, WaferNameDir }, MAK_EXT,   ApMarkFile,     ApWpxxMarkFile      },  /* @17111ykim-38258 */
    { CATEGORY_IDIIDC,  GROUP_INP,  INP_DIR, { SUBDIR_HD_INP, SUBDIR_GC_WAF, WaferNameDir }, IDP_EXT,   ApAlignFile,    ApWpxxAlignFile     },  /* @17111ykim-38258 */
    { CATEGORY_PMI,     GROUP_INS,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, PMT_EXT,   ApAlignFile,    ApWpxxAlignFile     },  /* @17111ykim-38258 */
    { CATEGORY_PMI,     GROUP_INP,  INP_DIR, { SUBDIR_HD_INP, SUBDIR_GC_WAF, WaferNameDir }, PDT_EXT,   ApAlignFile,    ApWpxxAlignFile     },  /* @17111ykim-38258 */
    { CATEGORY_PMI,     GROUP_INP,  INP_DIR, { SUBDIR_HD_INP, SUBDIR_GC_WAF, WaferNameDir }, PMP_EXT,   ApAlignFile,    ApWpxxAlignFile     },  /* @17111ykim-38258 */
    { CATEGORY_KLAOCR,  GROUP_NOT,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, KLA_EXT,   NULL,           NULL                },  /* @17111ykim-38258 */
    { CATEGORY_ACUOCR,  GROUP_NOT,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, ACU_EXT,   NULL,           NULL                },  /* @17111ykim-38258 */
    { CATEGORY_ACUOCR,  GROUP_NOT,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, PRJ_EXT,   NULL,           NULL                },  /* @17111ykim-38258 */
    { CATEGORY_ISOCR,   GROUP_NOT,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, IS_EXT,    NULL,           NULL                },  /* @17111ykim-38258 */
    { CATEGORY_CMAP,    GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, MCD_EXT,   ApWaferFile,    ApWpxxWaferFile     },  /* @17111ykim-38258 */
    { CATEGORY_IDIIDC,  GROUP_INS,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, IDI_EXT,   ApWaferFile,    ApWpxxWaferFile     },  /* @17111ykim-38258 */
    { CATEGORY_IDIIDC,  GROUP_INS,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, IDC_EXT,   ApWaferFile,    ApWpxxWaferFile     },  /* @17111ykim-38258 */
    { CATEGORY_CMAP,    GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, BOT_EXT,   ApWaferFile,    ApWpxxWaferFile     },  /* @17111ykim-38258 */
    { CATEGORY_PMI,     GROUP_INS,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, PMI_EXT,   ApAlignFile,    ApWpxxAlignFile     },  /* @17111ykim-38258 */
    { CATEGORY_PIN,     GROUP_NOT,  PIN_DIR, { SUBDIR_HD_PIN, SUBDIR_GC_PRB, WaferNameDir }, PNT_EXT,   ApProbeFile,    ApWpxxProbeFile     },  /* @17111ykim-38258 */
    { CATEGORY_PMI,     GROUP_NOT,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, PDS_EXT,   ApWaferFile,    ApWpxxWaferFile     },  /* @17111ykim-38258 */
    { CATEGORY_OPR,     GROUP_NOT,  OPR_DIR, { SUBDIR_HD_OPR, SUBDIR_GC_OPR, WaferNameDir }, OPR_EXT,   ApOprFile,      NULL                },  /* @17111ykim-38258 */  /* GPIB WPUP doesn't send Operation File */
    { CATEGORY_ISOCR,   GROUP_NOT,  OCR_DIR, { SUBDIR_HD_OCR, SUBDIR_GC_APL, WaferNameDir }, JOB_EXT,   NULL,           NULL                },  /* @17111ykim-38258 */
    { CATEGORY_CMAP,    GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, BNA_EXT,   ApWaferFile,    ApWpxxWaferFile     },  /* @17111ykim-38258 */
    { CATEGORY_CMAP,    GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, SCMAP_EXT, ApWaferFile,    ApWpxxWaferFile     },  /* @17111ykim-38258 */
    { CATEGORY_APL,     GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, ICC_APL_EXT, ApWaferFile,  NULL                },  /* @19322clcr-41513 */
    { CATEGORY_CMAP,    GROUP_NOT,  APL_DIR, { SUBDIR_HD_APL, SUBDIR_GC_APL, WaferNameDir }, ICC_MAP_EXT, ApWaferFile,  NULL                },  /* @19322clcr-41513 */
    { CATEGORY_WAF,     GROUP_WAF,  WAF_DIR, { SUBDIR_HD_WAF, SUBDIR_GC_WAF, WaferNameDir }, ICC_WAF_EXT, ApAlignFile,  NULL                },  /* @19322clcr-41513 */
    { CATEGORY_WAF,     GROUP_WTN,  WTN_DIR, { SUBDIR_HD_WTN, SUBDIR_GC_WAF, WaferNameDir }, ICC_WTN_EXT, ApAlignFile,  NULL                },  /* @19322clcr-41513 */
    { CATEGORY_CON,     GROUP_CON,  CON_DIR, { SUBDIR_HD_CON, SUBDIR_GC_COT, WaferNameDir }, ICC_CON_EXT, ApContactFile,NULL                },  /* @19322clcr-41513 */
    { CATEGORY_PMI,     GROUP_INP,  INP_DIR, { SUBDIR_HD_INP, SUBDIR_GC_WAF, WaferNameDir }, PDI_EXT,   ApAlignFile,    NULL                },  /* @19b25wrob-41737 */
    { CATEGORY_PMI,     GROUP_INP,  INP_DIR, { SUBDIR_HD_INP, SUBDIR_GC_WAF, WaferNameDir }, VPD_EXT,   ApAlignFile,    NULL                },  /* @20407lrya-41903 */
    { CATEGORY_PMI,     GROUP_INS,  INS_DIR, { SUBDIR_HD_INS, SUBDIR_GC_WAF, WaferNameDir }, AUTOLIGHT_EXT, ApAlignFile,NULL                },  /* @20c10yyab-41849 */
    { NULL,             NULL,       NULL,    { NULL,          NULL,          NULL         }, NULL,      NULL,           NULL                }   /* TERMINATOR *//* @17111ykim-38258 */

/* Improtant note : please read... @11a24ytsu-28481 */
/* When you add new category, please change code/appli/aMotoFtpSR.c - biggining of aMotoFtpSR() and also checkLastWaferExt().    */
/* For batch file sending/receiving, please make last file extension in above area. At last extension, prober will quit from ftp */
/* 重要メモ（読んでください！）：　カテゴリーを追加する場合、FTPの一括送受信時に、FTP最後のファイルを判断する箇所を変更する必要があります。*/
/* code/appli/aMotoFtpSR.c - aMotoFtpSR()内の最初の方と、checkLastWaferExt()内です。最後のファイルの定義を誤ると、FTPでQUITしません。*/

};                                                                                                                 /* @02a24hkaw-8850 */

/*-------------------------------------------------------------------*/
/*  HITACH skip-P-ken with GEM                       @03715tsat-9180 */
/*-------------------------------------------------------------------*/
intl    HvRetest = 0;           /* 0:no, 1:yes */ /* @03715tsat-9180 */
intl    HvDataServerSetup = 0;  /* 0:no, 1:yes */ /* @03715tsat-9180 */

/* Data loading Flag for Needle-Polish-Wafer                @03808skoj-10746 */
/* bit0-3 : Polish.which temporary save area.               @03808skoj-10746 */
/* bit4   : Judgment bit during execution.                  @03808skoj-10746 */
/* bit5   : Judgment bit re-load measurement wafer.         @03808skoj-10746 */
/* bit6   : Judgment bit stop request.                      @03808skoj-10746 */
/* bit7   : None                                            @03808skoj-10746 */
unsb    NPWinTest = 0;                                   /* @03808skoj-10746 */

/* The flag for polish by wafer after a stop.               @03808skoj-10746 */
unsb    NPWPolAfterStop = 0;                             /* @03808skoj-10746 */

intl    NPWAlignError = 0;                               /* @03808skoj-10746 */

/*-------------------------------------------------------------------------*/
/* test mode. (for HITACHI )                              @03922tsat-10226 */
/*-------------------------------------------------------------------------*/
intl    HvTestMode = 0;                                /* @03922tsat-10226 */
/* @04c24tsat-12601            @* 0 : normal(1st test or skip-P-ken)         @03922tsat-10226 */
/* @04c24tsat-12601            @* 1 : retest                                 @03922tsat-10226 */
            /* 0 : normal(1st test or skip-P-ken               @04c24tsat-12601 */
            /* 2 : retest                                      @04c24tsat-12601 */
intl    HvTestChip = 0;                                     /* @04c24tsat-12601 */
            /* 0: pass chip                                    @04c24tsat-12601 */
            /* 1: fail chip                                    @04c24tsat-12601 */
            /* 2: category                                     @04c24tsat-12601 */
unsb    HvTestCats[95];                                     /* @04c24tsat-12601 */

/*--------------------------------------------------------------------------------------*/
/* ISS Status                                                           @03909skoj-8858 */
/*--------------------------------------------------------------------------------------*/
#if ( PC300 )                                                        /* @03909skoj-8858 */
intl    ICPWafGapCnt;                                                /* @03909skoj-8858 */
intl    ICPStatus;                                                   /* @03909skoj-8858 */
	/* ICP_WAFGAP      0x1         :  ウェーハ間隔中                @03909skoj-8858 */
	/* ICP_LSTART      0x2         :  ロット開始時                  @03909skoj-8858 */
	/* ICP_LEND        0x4         :  ロット終了時                  @03909skoj-8858 */
	/* ICP_TEST        0x8         :  測定中                        @03909skoj-8858 */
	/* ICP_TRAY        (ICP_WAFGAP|ICP_LSTART|ICP_LEND)             @03909skoj-8858 */
	/* Calibration by One Die Tray                                  @03909skoj-8858 */
	/* ICP_CHUCK       (ICP_WAFGAP|ICP_LSTART|ICP_LEND|ICP_TEST)    @03909skoj-8858 */
	/* Calibration by IC Chuck                                      @03909skoj-8858 */
intl ICCContactCnt = 0;                                          /* @17227sna2-38710 */
intl ICP_FUNCTION_FLG = 0;                                       /* @17227sna2-38710 */
#endif                                                               /* @03909skoj-8858 */

/*------------------------------------------------------------------------*/
/* Re-Test mode function with WITS for NEC                @04527ttak-4895 */
/*------------------------------------------------------------------------*/
/* Is the specified map data from G/C received? */     /* @04527ttak-4895 */
/* (0:successful, 1:failed) */                         /* @04527ttak-4895 */
intl MapRecvERR = 0;                                   /* @04527ttak-4895 */

/* Chip Out Map existence check flag                      @04604skoj-12578 */
intl    OKIChipOutMap = 0;                             /* @04604skoj-12578 */
             /* 0 : Chip Out Map does not exist.          @04604skoj-12578 */
             /* 1 : Chip Out Map exists.                  @04604skoj-12578 */

/*------------------------------------------------------------------*/
/* Reject Signal Flag from TTL/GPIB (T_REJECT/u)   @04707nwat-11854 */
/*------------------------------------------------------------------*/
intl RejectSignalF = 0;                         /* @04707nwat-11854 */

/*------------------------------------------------------------------------*/
/* Multi Parts for TOYODIODE  @04708noot-12591                            */
/*------------------------------------------------------------------------*/
intl    GpMpSNFlg = 0;                                /* @04708noot-12591 */
intl    GpMpSNCnt = 0;                                /* @04708noot-12591 */
intl    GpMpSlot = 0;                                 /* @04708noot-12591 */
intl    GpMpCst = 0;                                  /* @04708noot-12591 */

tyBrushLimitOver BrushLimitOver;                         /* @04827rnis-11563 */

/* For polish limit assit timing ( Set Polish Media )       @04827rnis-12778 */
intl PolLimitOverMedia = 0;			            	     /* @04827rnis-12778 */
                                   /* 0x0001 : WAPP	        @04827rnis-12778 */
                                   /* 0x0010 : Polish wafer @04827rnis-12778 */
                                   /* 0x0100 : Brush	    @04827rnis-12778 */
                                   /* 0x0200 : Brush_A	    @04827rnis-12778 */
                                   /* 0x0400 : Brush_B	    @04827rnis-12778 */
                                   /* 0x0800 : Brush_C	    @04827rnis-12778 */

/*-------------------------------------------------------------------*/
/*  Online Table Test Mode                          @04b24cheo-12206 */
/*-------------------------------------------------------------------*/
intl    OnlinePN300TbMode = 0;                   /* @04b22cheo-12206 */

/*------------------------------------------------------------------------*/
/* WAPP planarity adjustment check                       @04b25yya2-12167 */
/*------------------------------------------------------------------------*/
intl    AssistPolDevice = 0;                          /* @04b25yya2-12167 */
intl    WAPPPlanarityAdjustmentResult = 0;            /* @04b25yya2-12167 */
intl    WAPPPlanarityAdjustmentResultArea2 = 0;       /* @05221myam-14244 */

/*--------------------------------------------------------------------------*/
/* GPIB Contact Check Stop Spec Flag                       @05330cheo-14051 */
/*--------------------------------------------------------------------------*/
intl    GpCntChkStop = 0;                               /* @05330cheo-14051 */

/*--------------------------------------------------------------------------*/
/* GPIB Contact Check Special 'e' command                  @06a17tkaw-18029 */
/*--------------------------------------------------------------------------*/
intl    GpCntChkSp_e_cmdF = 0;                          /* @06a17tkaw-18029 */

/*------------------------------------------------------------------------*/
/* Polish log execute status.                            @05421rman-14496 */
/*------------------------------------------------------------------------*/
intl PolishLogSaveOn = POLLOG_DATA_SAVE_ON;  
         /* = 1 Polish log data is saved.  */
         /* if PolishLogSaveOn is 0, data is not saved.  */

/*------------------------------------------------------------------------*/
/* Polish log data table.                                @05421rman-14496 */
/*------------------------------------------------------------------------*/
tyPolishLog PolishLog;

/*------------------------------------------------------------------------*/
/* Polish log files.                                     @05421rman-14496 */
/*------------------------------------------------------------------------*/
/* Polish log files list.  */
tyPolishFileList polDataList[ POLLOG_NUM ] = {
    { "/sd1/polish/pollog/polish0.txt",  "/fd0/polish0.txt",  "/sd1/mmi/log/logtemp/polish0.txt",     0x0001 },
    { "/sd1/polish/pollog/polish1.txt",  "/fd0/polish1.txt",  "/sd1/mmi/log/logtemp/polish1.txt",     0x0002 },
    { "/sd1/polish/pollog/polish2.txt",  "/fd0/polish2.txt",  "/sd1/mmi/log/logtemp/polish2.txt",     0x0004 },
};

/* Polish execute log toggle files list.  */
tyPolLogToggle polExeLogSelect[ POLLOG_EXE_FILE_NUM ] = {
    { "/sd1/polish/pollog/polish0.txt",  "/sd1/polish/pollog/polish1.txt" },
    { "/sd1/polish/pollog/polish1.txt",  "/sd1/polish/pollog/polish2.txt" },
    { "/sd1/polish/pollog/polish2.txt",  "/sd1/polish/pollog/polish0.txt" },
};

/*------------------------------------------------------------------------*/
/* Contact log files.                                    @05519msai-13217 */
/*------------------------------------------------------------------------*/
FILE    *ContLogFp;       /* Contact log file pointer             @05519msai-13217 */
intl    ContLogOpFlg = 0; /* Contact log file open flg 0:OFF 1:ON @05519msai-13217 */
intl    ContLogCnt = 0;   /* Contact log count                    @05519msai-13217 */

/*------------------------------------------------------------------------*/
/* Polish error parameter index                          @05526yya2-15119 */
/*------------------------------------------------------------------------*/
intl    PolErrParaIndex = 0;
/*------------------------------------------------------------------------*/
/* 2nd PMI Flag                                         @04a14mwat-12917  */
/*------------------------------------------------------------------------*/
intl PrbArea2ndFlag = 0;                         /* @04a14mwat-12917 */

/*---------------------------------------------------------------------------*/
/* Flags for ProbeAlignment Bofore 1stContact After Polish. @04417thon-11917 */
/*---------------------------------------------------------------------------*/
intl    ExePrbAlnBef1stCont = 0;                         /* @04417thon-11917 */
intl    PrbAlnBef1stNowExe = 0;                          /* @04417thon-11917 */

#if( P12XL )                            /* @05314nhir-13537 */
/*-------------------------------------------------------------------------*/
/* LOTREQ & RQALLFILE message from APPLI_TASK Flg for NEC @05314nhir-13537 */
/*-------------------------------------------------------------------------*/
intl    RequestFromAplNEC = 0;                         /* @05314nhir-13537 */
         /* 0 : from MMI_XXXX                             @05314nhir-13537 */
         /* 1 : from APPLI_TASK                           @05314nhir-13537 */
         /* 2 : from APPLI_TASK ( Case of Error Assist )  @05314nhir-13537 */

/*-------------------------------------------------------------------------*/
/* Polish limit assist send Flg for NEC                   @05314nhir-13537 */
/*-------------------------------------------------------------------------*/
intl    PolLimitAssistFlg = 0;                         /* @05314nhir-13537 */
         /* 0 : no limit                                  @05314nhir-13537 */
         /* 1 : limit assist sending                      @05314nhir-13537 */

/*-------------------------------------------------------------------------*/
/* G/C Reqesting Flg ( from LotReqest to WafFile FTP end ) for NEC @05314nhir-13537 */
/*-------------------------------------------------------------------------*/
intl    GCRequestingNEC = 0;                           /* @05314nhir-13537 */

/*-------------------------------------------------------------------------*/
/* For old and new file name comparison (continuous lot for NEC) @05314nhir-13537 */
/*-------------------------------------------------------------------------*/
tyWafNameNEC    ContinuousLotWafName;                  /* @05314nhir-13537 */

unsb LotPrqWfName[ 32 ];                               /* @10a04fmor-26334 */

#endif                                                 /* @05314nhir-13537 */

/* StartSlot Designation Screen Display Flag for TSMC   @04603thon-12499 */
intl TSMCStartSlotDesigMode = 0;                     /* @04603thon-12499 */

/*--------------------------------------------------------------------------*/
/* Stop at last die function enhance                       @05720ssas-14575 */
/*--------------------------------------------------------------------------*/
intl    EndChipStopCheckF = 0;                          /* @05720ssas-14575 */

/*--------------------------------------------------------------------------*/
/* Don't write GMS result map twice in one wafer.          @04616tsat-12714 */
/*--------------------------------------------------------------------------*/
intl WriteGMSResultMap = 0;                             /* @04616tsat-12714 */

/*--------------------------------------------------------------------------*/
/* Don't write GMS Wafer file & Lot file  twice in one wafer.  @09519mich-24314*/
/*--------------------------------------------------------------------------*/
intl GmsHDWtFileFlg = 0;                             /* @09519mich-24314 */

/*------------------------------------------------------------------------*/
/* PADS PTPA Mode Flag                                   @04525mwat-11923 */
/*------------------------------------------------------------------------*/
intl PADSPTPAEXE = 0;                                 /* @04525mwat-11923 */
   /* 0: Before Pre Image                       @07xxxytam-20216 */
   /* 1: After Pre Image                 @07xxxytam-20216 */
   /* 2: After Image             @07xxxytam-20216 */
/* @08904ttak-22819 intl PADSPTPASTOPEXE = 0;                             @* @04525mwat-11923 */
/* @08904ttak-22819 intl PADSPTPACHIPCOUNT = 0;                           @* @04525mwat-11923 */
/* @07xxxytam-20216 BOOL PADSPTPOEnabled = FALSE;  @* to mask PTPO function  @07322jkam-19300 */
intl PTPOIndexCnt = 0;                                /* @08627ttak-22119 */

/* FPMI (micro) status flag */
intl FPMIStatF = 0;                                   /* @04721hkaw-FPMI-01 */

/* FPMI retry flag *//* 0 : Not retrying 1 : retrying */
intl FPMIRetryFlag = 0;                                    /* @05627hkaw-FPMI */

/* FPMI Start delay for waiting delete result data on TELPADS */    /* @07225tyam-19300 */
intl FPMIStartDelay = 0;                                            /* @07225tyam-19300 */

/* PADS PC Status Error */
BOOL isPADSPCStatusError = FALSE;                     /* @09702skam-28442 */

#if( PADSOPT )                                        /* @08904ttak-22819 */
/* FPMI Inspection Result Data from TELPADS */        /* @08904ttak-22819 */
tyFPMIResult FPMIResult;                              /* @08904ttak-22819 */
#endif( PADSOPT )                                     /* @08904ttak-22819 */

/* @09401ttak-22819 #if( P12XL )                                            @* @09220takt-24235 */
#if( PADSOPT )                                          /* @09401ttak-22819 */
/* Acceptable flg to push STOP on camera view in PADS insp @09220takt-24235 */
intl FPMIAcceptStopButtonFlg = FPMI_REJECT_STOP_BUTTON; /* @09220takt-24235 */
/*   Value : FPMI_REJECT_STOP_BUTTON (default)             @09220takt-24235 */
/*           FPMI_ACCEPT_STOP_BUTTON                       @09220takt-24235 */
/*   To access, must use Get(Set)FPMIAcceptStopButtonFlg() @09220takt-24235 */
/* @09401ttak-22819 #endif ( P12XL )                                        @* @09220takt-24235 */
#endif( PADSOPT )                                       /* @09401ttak-22819 */

/*------------------------------------------------------------------------*/
/* Pice Chip Tray Test Chip Counter                      @05208tkam-13460 */
/*------------------------------------------------------------------------*/
/* *** @05c03tkam-16795 intl    PieceChipEndCounter = 0;                      @* @05208tkam-13460 */

/*-------------------------------------------------------------------*//* @05a12ttak-16320 */
/* BT1stChipMoveF                                                    *//* @05a12ttak-16320 */
/*-------------------------------------------------------------------*//* @05a12ttak-16320 */
intl    BT1stChipMoveF = 0;                                            /* @05a12ttak-16320 */
        /* BT1STCHIP_NEEDMOVE     0x10000000                         *//* @05a12ttak-16320 */

#if( PC300 )                                            /* @05a07rnis-15623 */
/*--------------------------------------------------------------------------*/
/* Switch Probe Contact Count Limit Status.                @05a07rnis-15623 */
/*--------------------------------------------------------------------------*/
intl    SWProbeLimitOverStatus = 0;                     /* @05a07rnis-15623 */
#endif                                                  /* @05a07rnis-15623 */

/*-------------------------------------------------------------------------*/
/* GJG parameter batch setting for TOSHIBA.               @05c22nhir-15591 */
/*-------------------------------------------------------------------------*/
intl    GJGParaBatchSetting = 0;                       /* @05c22nhir-15591 */
intl    TOSHIBA_GJGConnectRetry = 0;                   /* @05c22nhir-15591 */

/*-------------------------------------------------------------------------*/
/* Polish sheet surface height check for Z-WAPP           @05c05yya2-13334 */
/*-------------------------------------------------------------------------*/
intl    ZWAPPToleranceDiff = 0;                        /* @05c05yya2-13334 */

/*-------------------------------------------------------------------------*/
/*  IDC Recount Flg for HITACH                            @06118msai-13753 */
/*-------------------------------------------------------------------------*/
intl    IDCRecountExec = 0;                            /* @06118msai-13753 */
/* 0... no, 1...IDC Recount                               @06118msai-13753 */ 

/*-------------------------------------------------------------------------*/
/* GPIB aPC/aPCS Command Status                                            */
/*    -> aPC  Command Executed : 0x01                                      */
/*    -> aPCS Command Executed : 0x02                                      */
/*-------------------------------------------------------------------------*/
intl GPaPCCmdExe = 0;                                  /* @06424cheo-17752 */

/*-------------------------------------------------------------------------*/
/* Polish Wafer OriFlat Rotation (degree)                 @06621ayok-17390 */
/*-------------------------------------------------------------------------*/
intl  PolWafRotation = 0;                              /* @06621ayok-17390 */

/*-------------------------------------------------------------------*/
/* For using two polish wafer with one lot.         @03a27rman-10690 */
/*-------------------------------------------------------------------*/
intl PolWafDualUse = POLWAF_DUAL_NOT;  /* = 0x00000000 */

/*-------------------------------------------------------------------*/
/* For polishing lot start by polish wafer status.  @04123rman-11450 */
/*-------------------------------------------------------------------*/
intl LotStartPolWaf = 0;

intl PolSameTiming = 0;                          /* @06808yya2-17390 */

/*--------------------------------------------------------------------------*/
/* Polish Wafer Exchange Operation Status                  @06627mfai-17390 */
/*       0x00 : None, default                              @06627mfai-17390 */
/*       0x01 : Polish Wafer Exchange( Wafer Unload )      @06627mfai-17390 */
/*       0x02 : Polish Wafer Exchange( Wafer Load )        @06627mfai-17390 */
/*--------------------------------------------------------------------------*/
intl PolWafExchgSetup = 0x00;                           /* @06627mfai-17390 */


/* @06807rnis-17390 */
/*--------------------------------------------------------------------------*/
/* Which wafer( For 3buffer ).                                              */
/*       0x0001( WAFER_USE_1 ) : Wafer 1                                    */
/*       0x0002( WAFER_USE_2 ) : Wafer 2                                    */
/*       0x0004( WAFER_USE_3 ) : Wafer 3                                    */
/*--------------------------------------------------------------------------*/
intl WhichPolWaf = 0x0000;                               /* @06807rnis-17390 */

/*-------------------------------------------------------------------------*/
/* 50mmPolisher flatness check                            @06705jnis-15963 */
/*-------------------------------------------------------------------------*/
intl    Pl50FlatToleranceDiff = 0;                     /* @06705jnis-15963 */

/*--------------------------------------------------------------------------*/
/* GPIB Contact Check Skip Status -> 0 : Execution, 2 : Skip                */
/*--------------------------------------------------------------------------*/
intl    GpCntChkSkipTiming = 0;                                 /* @06607cheo-16559 */
/*--------------------------------------------------------------------------*/
/* WaferName of Last Tested                                                 */
/*--------------------------------------------------------------------------*/
#if( WAFERNAME32 )                                              /* @06607cheo-16559 */
intb    GpCntChkWafName[WAFERNAME_SIZE];                        /* @06607cheo-16559 */
#else                                                           /* @06607cheo-16559 */
intb    GpCntChkWafName[21];                                    /* @06607cheo-16559 */
#endif                                                          /* @06607cheo-16559 */

/*--------------------------------------------------------------------------*/
/* FTP parameter fot TSBATC                                                 */
/*--------------------------------------------------------------------------*/
tyTsbatcFtpPara  tsbaFtpPara[ 4 ];                      /* @06721hash-18117 */

/*  Lot Counter */
intl    ReContPosChkExe = 0;            						/* @06712cheo-17433 */

/* Lot Couunter */
unsl    ContChkLotCounter = 1;  						/* @06818cheo-18615 */

/*----------------------------------------------------------------------------------*/
/* Preheat Before First Contact Status -> OFF : Execution failure, ON : Execution OK */
/*----------------------------------------------------------------------------------*/
intl    FirstContPHExeFlag = 0;                                 /* @06a20ytam-17047 */
intl    FirstContPHExeWafCounter = 0;                           /* @06a20ytam-17047 */
intl    WafCounterAtFirstContPH = 0;                            /* @06a20ytam-17047 */

/*-------------------------------------------------------------------*/
/*  bridge inker z position                         @06c14jnis-18667 */
/*-------------------------------------------------------------------*/
intl    InkerMicroPosZ = 0;                      /* @06c14jnis-18667 */

intl    SendEndFileNo = 0;  /* End file no. */  /* @06c26fmor-19443 */
intl    ApNetFileNo = 0;                        /* @06c26fmor-19443 */
intl    HsmsFtpSendFlag = 0;                    /* @06c26fmor-19443 */

/*------------------------------------------------------------------------*/
/* First Contact Overdrive Flag                          @07116khir-18660 */
/*------------------------------------------------------------------------*/
intl    FirstContactF = 0;                            /* @07116khir-18660 */

/*------------------------------------------------------------------------*/
/* Wait Before Unload Flag                               @07130tish-17201 */
/*------------------------------------------------------------------------*/
intl    WaitBeforeUnloadF = 0;                        /* @07130tish-17201 */

/*--------------------------------------------------------------------------*/
/* Execution processing from "Media parameter"            @07323tma2-17122 */
/*       1 : Execution                                    @07323tma2-17122 */
/*       0 : None                                         @07323tma2-17122 */
/*--------------------------------------------------------------------------*/
intl PolWafFromMedia = 0;                              /* @07323tma2-17122 */

intl ForcedDispFlag = 0;                         /* @07419aoma-18791 */
intl insStateCharColor = 0;                      /* @07419aoma-18791 */
intl insStateBgColor = 0;                        /* @07419aoma-18791 */
intb installStateBuff[30];                       /* @07419aoma-18791 */

/*-------------------------------------------------------------------------*/
/* Check Chuck Ink Particle Exec Flag                     @07509aoma-17457 */
/*       0 : Particle Check No Use                        @07509aoma-17457 */
/*       1 : Camera                                       @07509aoma-17457 */
/*       2 : Visual                                       @07509aoma-17457 */
/*-------------------------------------------------------------------------*/
intl chkChuckModeFlag = 0;

/*------------------------------------------------------------------------*/
/* Gpib "p" command status                               @07525mfai-17549 */
/*           0 : init value                              @07525mfai-17549 */
/*  0x00000008 : "p3" cmd                                @07525mfai-17549 */
/*  0x00000010 : "p4" cmd                                @07525mfai-17549 */
/*  0x10000000 : Chk focus                               @07525mfai-17549 */
/*------------------------------------------------------------------------*/
unsl    GpibPCmdMode = 0;                             /* @07525mfai-17549 */


/*--------------------------------------------------------------------*/
/* Gpib "p3x" command Select Buffer Table Number.   @14a21kaib-33722 */
/*           0: init ( non-select )                 @14a21kaib-33722 */
/*          >0: select buffer table.                @14a21kaib-33722 */
/*--------------------------------------------------------------------*/
unsb    GpibP3xBTblSel = 0;                      /* @14a21kaib-33722 */

intl LTI_ExecFlag = 0;      /* @07618aoma-17226 */
/* 0 = None */
/* 1 = Exec */
/* 2 = End  */

/* Sample Test BIN Number Convert Data */                                           /* @07810ssas-16511 */
unsb    SampleTestBinNumberConv[ SIZE_513_GROUP ][ AUTHORIZED_BIN_NUM_CONV_SIZE ];  /* @07810ssas-16511 */

/*------------------------------------------------------------------------*/
/* Polish before PC Bending status                       @07718mfai-20122 */
/*    0 : polish before PC Bending has not been executed @07718mfai-20122 */
/*    1 : executed.                                      @07718mfai-20122 */
/*------------------------------------------------------------------------*/
intl    ExecutedPolBefPCBending = 0;                  /* @07718mfai-20122 */

/*------------------------------------------------------------------------*/
/* WAPP marking data                                     @07820yya2-17811 */
/*------------------------------------------------------------------------*/
time_t WAPPMarkTime = (time_t)0;                      /* @07820yya2-17811 */
intl   WAPPMarkExeF = WAPPMARK_NOT;                   /* @07820yya2-17811 */

/*------------------------------------------------------------------------*/
/* Presence of operation status transmission             @07531kaki-20028 */
/* when assistance is generated.                         @07531kaki-20028 */
/*------------------------------------------------------------------------*/
intl    AssistFromS1F80 = 0;                          /* @07531kaki-20028 */

/*------------------------------------------------------------------------*/
/*  Cal-Comp Signal wait flag.                        @* @08304cheo-21942 */
/*------------------------------------------------------------------------*/
intl    TifCalCompFlag = 0;                           /* @08304cheo-21942 */

/*------------------------------------------------------------------------*/
/*  StampLot Name                                     @* @08305tish-21942 */
/*------------------------------------------------------------------------*/
intb    StampLot[21] = "";                            /* @08305tish-21942 */


/*------------------------------------------------------------------------*/
/* FUJITSU Auto run mode                                 @08408tyui-22485 */
/*------------------------------------------------------------------------*/
/* Auto Run PreHeat Stop flag @08408tyui-22485 */
intl    ARunPreHeatStop = 0;                                /* @08408tyui-22485 */

/* Auto Run Match flag @08408tyui-22485 */
intl    ARMatchWait = 0;                                    /* @08408tyui-22485 */

/*------------------------------------------------------------------------*/
/* For M0050 => Detail Screen                            @08430rnis-22440 */
/*------------------------------------------------------------------------*/
intl    SimultaneousRangeOver = 0;                    /* @08430rnis-22440 */

/*------------------------------------------------------------------------*/
/* For SPO_STG_Z_SEPARATE_ASSIST                         @08c10yya2-23847 */
/*------------------------------------------------------------------------*/
intl    zPosAftAssistSetFlg = 0;                      /* @08c10yya2-23847 */

/*------------------------------------------------------------------------*/
/* Reprobe Phase save flag for Run time polish wafer     @08b10yya2-23804 */
/*------------------------------------------------------------------------*/
intl    ReprobePhaseSv = 0;                           /* @08b10yya2-23804 */

/*------------------------------------------------------------------------*/
/* FTP Ocr File Open Error                               @09204fmor-24103 */
/*------------------------------------------------------------------------*/
intl    FTPOcrFileOpenErrF = 0;                       /* @09204fmor-24103 */

/*------------------------------------------------------------------------*/
/* Manual OCR Setup Name                                 @09204fmor-24103 */
/*------------------------------------------------------------------------*/
#if (WAFERNAME32)                                     /* @09204fmor-24103 */
unsb    mnuOcrStupN[ 25 + WAFERNAME_ADDED ];          /* @09204fmor-24103 */
#else                                                 /* @09204fmor-24103 */
unsb    mnuOcrStupN[ 25 ];                            /* @09204fmor-24103 */
#endif                                                /* @09204fmor-24103 */

/*-------------------------------------------------------------------*//* @09303fmor-23133 */
/* Auto Run Restart Count flag                                       *//* @09303fmor-23133 */
/*-------------------------------------------------------------------*//* @09303fmor-23133 */
#if( PC300 )                                                           /* @09303fmor-23133 */
intl    ARunRestartStsCnt = 0;                                         /* @09303fmor-23133 */
#endif                                                                 /* @09303fmor-23133 */

/*-------------------------------------------------------------------*//* @09303fmor-23133 */
/* Auto Run Contact Check Send Recv Message Flag                     *//* @09303fmor-23133 */
/*-------------------------------------------------------------------*//* @09303fmor-23133 */
#if( PC300 )                                                           /* @09303fmor-23133 */
intl    ARunCntChkSRMsgFlg = 0;                                        /* @09303fmor-23133 */
#endif                                                                 /* @09303fmor-23133 */

/*------------------------------------------------------------------------*/
/* Advanced Log                                          @08b10tish-23129 */
/*------------------------------------------------------------------------*/
unsl    ContCountbyWafer = 0;                         /* @08b10tish-23129 */

/*------------------------------------------------------------------------*/
/* Occurred ERROR in AS000 at Before Test sequence.      @10623ykas-23921 */
/*------------------------------------------------------------------------*/
intl    AS000ErrFlg = FALSE;                          /* @10623ykas-23921 */
intl    OnWaferErrFlg = FALSE;                        /* @10623ykas-23921 */

/*------------------------------------------------------------------------*/
/* GPIB Contact Check Flag for TSMC.                     @10a01ykas-26226 */
/*------------------------------------------------------------------------*/
intl    GpContTSMCF = 0;                              /* @10a01ykas-26226 */

intl ULS_DEBUG = 0;                                     /* @10827tnak-26604 */

/*------------------------------------------------------------------------*/
/* iPAS Load Wafer Flg                                   @10720tish-26218 */
/*------------------------------------------------------------------------*/
intl    iPASLoadWaf = 0;                              /* @10720tish-20218 */

/*------------------------------------------------------------------------*/
/* For Quick StaticLoad Wafer lot start status.     @* @10720tish-26218-2 */
/*------------------------------------------------------------------------*/
intl    iPASWafPosF = 0;                            /* @10720tish-26218-2 */

/*------------------------------------------------------------------------*/
/* Single test skip chek                                 @10c16cheo-27118 */
/*------------------------------------------------------------------------*/
intl    SingleSkipChkF = 0;                           /* @10c16cheo-27118 */

/*----------------------------------------------------------------------------*/
/* Last wafer unload check flag ( Move from aLoadProcNew.c ) @11209khir-26334 */
/*----------------------------------------------------------------------------*/
intl    EndUnld = 0;        /* @11209khir-26334 */

/*----------------------------------------------------------------------------*/
/* For Seagate.                                                               */
/*----------------------------------------------------------------------------*/
intl    AbortSwFlg = 0;                                   /* @11b11cheo-28561 */
intl    GpEnableAMCflag = 0;                              /* @12420cheo-29458 */

intl    NextLotStart=0;     /* @11123mshi-26334 */
intl    START_CAST = 1;     /* @11208mshi-26334 */

/*--------------------------------------------------------------------------*/
/* ApplTaskがエラーメッセージ表示中（解除待ち）であるかどうかを示すフラグ   */
/*--------------------------------------------------------------------------*/
intl    ApErrMsgDispFlag = 0;                           /* @12512kish-29435 */

/*--------------------------------------------------------------------------*/
/* WAPP交換時のヒータ制御状態を表すフラグ                  @12831tkuw-29300 */
/*--------------------------------------------------------------------------*/
#if( P12XL )                                            /* @12831tkuw-29300 */
intl    AutoHeatUpSts = 0;                              /* @12831tkuw-29300 */
#endif                                                  /* @12831tkuw-29300 */

/*--------------------------------------------------------------------------*/
/* Chk whether RTWM must update for Strip Probing.                          */
/*--------------------------------------------------------------------------*/
intl    UpdatedRTWMforStripProbing = 0;                 /* @12807ykas-29925 */


/*----------------------------------------------------------------------------*/
/* For GPIB Command(aPTPO).                                                   */
/*----------------------------------------------------------------------------*/
#if( PADSOPT )                                            /* @12824jsai-29459 */
intl    GpPTPOFlag = 0;                                   /* @12824jsai-29459 */
intl    PADSPTPOExeDieAd[2];                              /* @12824jsai-29459 */
#endif                                                    /* @12824jsai-29459 */

intl    UglyTilePattern[ UGLY_BIN_KIND_MAX ] = { 19, 71, 79, 60, 62 };    /* @12c10ykas-29050 */
intl    UglyBINSetting[ UGLY_BIN_KIND_MAX ] = { -1, -1, -1, -1, -1 };     /* @12c10ykas-29050 */
intl    UglyBINKind = 0;                                                  /* @12c10ykas-29050 */

/* ファイルの送受信先の判定       @13121sna2-29050 */
/* 1:ホスト１,2:ホスト２          @13121sna2-29050 */
intl SendReceHostFlg = 0;      /* @13121sna2-29050 */

intl    TargetMoveLimitOver = 0;                          /* @13228ykas-30619 */
intl    WafAlignAfterPrbAlign = 0;                        /* @13228ykas-30619 */
intl    GpCPSFlg = 0;                                     /* @13726tnak-31063 */
intl    WafParGPIBContChk = 0;                            /* @13826sna2-31751 */
intl    MmiDiagPolOperat = 0;                             /* @13b08sna2-31071 */
intl    StatPreHtBefStartStopWith1stWaf = 0;              /* @14214ykas-31292 */

/*----------------------------------------------------------------------------*/
/* カードコンタクト回数 管理機能                                              */
/*----------------------------------------------------------------------------*/
#if( P12XL )                                              /* @14321jsai-31962 */
tyCntMngData CntMngData;                                  /* @14321jsai-31962 */
#endif                                                    /* @14321jsai-31962 */

/* Micron Log Ftp Func - Auto Delete 					   @14909ytsu-33262 */
#if( P12XL )											/* @14909ytsu-33262 */
unsl	MicronLogAutoDelete = 0;						/* @14909ytsu-33262 */
tyMcLogFtpRecord McLogFtpRecord;						/* @14909ytsu-33262 */
#endif													/* @14909ytsu-33262 */

intl SpGpLmpStatusSend = 0;								/* @14c12ytsu-32318 */

/*----------------------------------------------------------------------------*/
/* mPMS command flag                                      @* @11627pjan-27357 */
/*----------------------------------------------------------------------------*/
intl GpmPMSstatus = 0;                                    /* @11627pjan-27357 */

/*----------------------------------------------------------------------------*/
/* WLSLT manual test flag                                 @* @14115msai-31693 */
/*----------------------------------------------------------------------------*/
intl ManualTestFlg = 0;                                   /* @14115msai-31693 */

/*----------------------------------------------------------------------------*/
/* Result data check flag                                 @* @14626dhar-33268 */
/*----------------------------------------------------------------------------*/
intl ResultDataError = 0;                                 /* @14626dhar-33268 */

/*----------------------------------------------------------------------------*/
/* Preheat Parameter for GPIB Contact Check               @* @14b10toku-33897 */
/*----------------------------------------------------------------------------*/
tyGpibPreheatForContactCheck GpibPreheat;                 /* @14b10toku-33897 */

/*----------------------------------------------------------------------------*/
/* Command executing parameters for GPIB Contact Check    @* @14b10toku-33897 */
/*----------------------------------------------------------------------------*/
intl ApAlignDuringGpibContactCheck = 0;                   /* @14b10toku-33897 */
intl ApGpibDuringSmallzCommand = 0;                       /* @14b10toku-33897 */
intl AplGpibPreheatAsistMode = 0;                         /* @14b10toku-33897 */
intl BTestPNoPointerSave = 0;/* save ﾌﾟﾛｾｽ制御ﾃｰﾌﾞﾙﾎﾟｲﾝﾀ */ /* @14b10toku-33897 */ /* 2015.1.29 */
intl WafTestSeqFSave = 0;                                   /* @14b10toku-33897 */ /* 2015.1.29 */
intl ApGpibDuringSmallzStopCmd = 0;                         /* @14b10toku-33897 */ /* 2015.1.29 */

/*----------------------------------------------------------------------------*/
/* 送信ログファイル情報ステータス                            @15128kgom-34007 */
/*----------------------------------------------------------------------------*/
intl    UnsentGPIBLogSize  = 0; /* GPIBログ：更新量          @15128kgom-34007 */
intl    UnsentHSMSLogSize  = 0; /* HSMSログ                  @15128kgom-34007 */
unsl    WafAlignLogStat    = 0; /* ウェーハアライメントログ  @15128kgom-34007 */
unsl    PrbAlignLogStat    = 0; /* プローブアライメントログ  @15128kgom-34007 */
unsl    PolishLogStat      = 0; /* 針先研磨ログ              @15128kgom-34007 */
unsl    ErrLogFileStat     = 0; /* エラーログ                @15128kgom-34007 */
unsl    MmiLogFileStat     = 0; /* 操作ログ                  @15128kgom-34007 */
unsl    PreheatLogStat     = 0; /* プリヒートログ            @15303tsan-33923 */

/*----------------------------------------------------------------------------*/
/* Edit Mode Timer Paramete                                  @15415suem-32699 */
/*----------------------------------------------------------------------------*/
intl    editModeTimerResetFlg = 0;                        /* @15415suem-32699 */
intl    editModeTimerSetup = 1;                           /* @15415suem-32699 */
intl    editModeTimer = 5;                                /* @15415suem-32699 */

#if( P1200 )                                              /* @16510suem-32702 */
/*----------------------------------------------------------------------------*/
/* Engineer Mode Password Use : Yes or No                    @16510suem-32702 */
/*----------------------------------------------------------------------------*/
intl    engModePWUse = 1;                                 /* @16510suem-32702 */
unsb    engModePass[21];                                  /* @16510suem-32702 */
#endif                                                    /* @16510suem-32702 */

/*----------------------------------------------------------------------------*/
/* Engineer Mode FOUP Re-Placement Check : Yes or No         @16915suem-37940 */
/*----------------------------------------------------------------------------*/
intl    engModeFOUPChk = 0;                               /* @16915suem-37940 */

/*----------------------------------------------------------------------------*/
/* Engineer Mode Contact check GPIB USE.                     @16915suem-37940 */
/*----------------------------------------------------------------------------*/
intl    engModeCntGpFlg = 0;                              /* @16915suem-37940 */

/*----------------------------------------------------------------------------*/
/* ミツミ千歳 TTL/GPIB併用仕様時にAW005送信の有無を保持      @15309sooy-34715 */
/*----------------------------------------------------------------------------*/
intl    SendAw005F = 0;                                   /* @15309sooy-34715 */

/*----------------------------------------------------------------------------*/
/* ミツミ千歳 DPU/GMS切替仕様時に'C'コマンド受信の有無を保持 @15309sooy-34715 */
/*----------------------------------------------------------------------------*/
intl    RcvCCom = FALSE;                                  /* @15309sooy-34715 */

/* ASE専用実行研磨パターン判定フラグ                         @15727sna2-33510 */
intl   PolishExePattern = 0;                              /* @15727sna2-33510 */


/* USERFUNC.TXT 格納メモリポインタ                           @16204thos-36627 */
unsb *UFMemPtr    = NULL;                                 /* @16204thos-36627 */
unsb *UFMemPtrTmp = NULL;                                 /* @16204thos-36627 */

/*----------------------------------------------------------------------------*/
/* PMI Log upload parameter setting                          @15a22hden-36083 */
/*----------------------------------------------------------------------------*/
unsl    PMILogUploadStat     = 0; /* PMI Log upload          @15a22hden-36083 */

#if( !WDFDP )                                               /* @16315sooy-33345 */
/*------------------------------------------------------------------------------*/
/* SW Probe Air Blow 動作環境                                  @16315sooy-33345 */
/*------------------------------------------------------------------------------*/
/* SW Probe Air Blow 実行温度(初期値：76℃)                    @16315sooy-33345 */
/* @18711kisi-40973 intl    SWPAirBlowExeTemp  = 76;        @* @16315sooy-33345 */
intl    SWPAirBlowExeTemp  = 70;                            /* @18711kisi-40973 */
/* SW Probe Air Blow Onタイマー(初期値：300s)                  @16315sooy-33345 */
unsl    SWPAirBlowOnTimer  = 300;                           /* @16315sooy-33345 */
/* SW PRobe Air Blow Offタイマー(初期値：10s)                  @16315sooy-33345 */
unsl    SWPAirBlowOffTimer = 10;                            /* @16315sooy-33345 */
#endif                                                      /* @16315sooy-33345 */

#if( P1200 )                                              /* @16510suem-32702 */
/*----------------------------------------------------------------------------*/
/* Mode Change Flag [Manufacturing] <-> [Engineering]        @16510suem-32702 */
/*----------------------------------------------------------------------------*/
intl ENG_MODE_DEB = 0;                                    /* @16510suem-32702 */
intl engModeChgReqFlg = 0;                                /* @16510suem-32702 */
intl EngineeringModeFlg = 0;                              /* @16510suem-32702 */
#endif                                                    /* @16510suem-32702 */

#if ( P12XL && HOSTFUNC && GEM)                           /* @17224bwen-38567 */
/*---------------------------------------------------------- @17224bwen-38567 */
/* SVID 0x10057 check update                                 @17224bwen-38567 */
/*---------------------------------------------------------- @17224bwen-38567 */
intl SetWafinfoLWafStat;                                  /* @17224bwen-38567 */
#endif                                                    /* @17224bwen-38567 */

#if( Y_PLC && !WDFXLDP )                                  /* @17626jimc-39427 */
/*---------------------------------------------------------- @17626jimc-39427 */
/* EasyCool change wafer file flag                           @17626jimc-39427 */
/*---------------------------------------------------------- @17626jimc-39427 */
intl  EasyCoolWaferChangeFlag = 0;                        /* @17626jimc-39427 */
#endif  /*  Y_PLC && !WDFXLDP                                @17626jimc-39427 */

#if ( P12XL && HOSTFUNC && GEM)                           /* @17808clcr-39690 */
/*---------------------------------------------------------- @17808clcr-39690 */
/* RWAFERID format is no match status                        @17808clcr-39690 */
/* RWID_WITHOUT          0x01                                @17808clcr-39690 */
/* RWID_FORMAT_ERR       0x02                                @17808clcr-39690 */
/* RWID_CHKSUM_ERR       0x04                                @17808clcr-39690 */
/* RWID_CHKLOTNAME_ERR   0x08                                @17808clcr-39690 */
/*---------------------------------------------------------- @17808clcr-39690 */
intl  RWaferIDFormatStat = 0;                             /* @17808clcr-39690 */
#endif /* ( P12XL && HOSTFUNC && GEM)                        @17808clcr-39690 */

/*---------------------------------------------------------- @18621hbil-40289 */
/* For WAPP 2Sheet ICC, execute  polish before ICC or not    @18621hbil-40289 */
/* 0:Initial Value or Do not execute polish before ICC       @18621hbil-40289 */
/* 1:execute polish before ICC                               @18621hbil-40289 */
/*---------------------------------------------------------- @18621hbil-40289 */
intl  DoPolBefICC = 0;                                    /* @18621hbil-40289 */

#if( P1200 )                                                                                          /* @16510suem-32702 */
/* Engineering Mode File Table **********************************************************************//* @16510suem-32702 */
tyEngModeFileName EngModeFileName[ENGMODEFILEMAX] = {                                                 /* @16510suem-32702 */
    { ENGMODEFDDIR,           ENGMODEFDDIR,          ENGMODEFDDIR             }, /* Eng Mode FD Dir *//* @16510suem-32702 */
    { ENGMODEDIR,             ENGMODEDIR,            ENGMODEDIR               }, /* Eng Mode Dir    *//* @16510suem-32702 */
    { UFDATA_MFG,             UFDATA_ENG,            UFDATA_ENGDEF            }, /* USERFUNC        *//* @16510suem-32702 */
    { GPCNT_MFG_FILE,         GPCNT_ENG_FILE,        GPCNT_ENGDEF_FILE        }, /* GPIB Param      *//* @16510suem-32702 */
#if (HOSTFUNC && GEM)                                                                                 /* @16510suem-32702 */
    { GEM_RPTID_MDGFILE,      GEM_RPTID_ENGFILE,     GEM_RPTID_ENGDEFFILE     }, /* GEM RPTID       *//* @16510suem-32702 */
    { GEM_CEED_MFGFILE,       GEM_CEED_ENGFILE,      GEM_CEED_ENGDEFFILE      }, /* GEM CEED        *//* @16510suem-32702 */
    { ECV_MFG_FILE,           ECV_ENG_FILE,          ECV_ENGDEF_FILE          }, /* GEM ECV         *//* @16510suem-32702 */
    { SV_MFG_FILE,            SV_ENG_FILE,           SV_ENGDEF_FILE           }, /* GEM SV          *//* @16510suem-32702 */
    { ALMRPT_MFGFILE,         ALMRPT_ENGFILE,        ALMRPT_ENGDEFFILE        }, /* GEM ALMRPT      *//* @16510suem-32702 */
#endif                                                                                                /* @16510suem-32702 */
    { MMI_PARM_MFG_FILE,      MMI_PARM_ENG_FILE,     MMI_PARM_ENGDEF_FILE     }, /* MMI Param       *//* @16510suem-32702 */
    { MMI_MVLT_MFG_FILE,      MMI_MVLT_ENG_FILE,     MMI_MVLT_ENGDEF_FILE     }, /* Testing Lim     *//* @16510suem-32702 */
    { MMI_WFRPM_N_MFG_FILE,   MMI_WFRPM_N_ENG_FILE,  MMI_WFRPM_N_ENGDEF_FILE  }, /* Waf Param Mask  *//* @16510suem-32702 */
    { MMI_USRFNC_N_MFG_FILE,  MMI_USRFNC_N_ENG_FILE, MMI_USRFNC_N_ENGDEF_FILE }, /* Ope param Mask  *//* @16510suem-32702 */
#if (HOSTFUNC && GEM)                                                                                 /* @16915suem-37940 */
    { FOUPRPC_FILE,           FOUPRPC_FILE,          FOUPRPC_DEFFILE          }, /* FOUP Chk File   *//* @16915suem-37940 */
#endif                                                                                                /* @16915suem-37940 */
};                                                                                                    /* @16510suem-32702 */
#endif                                                                                                /* @16510suem-32702 */

/*----------------------------------------------------------------------------*/
/* PADS Debug Flag(for appli)                                @17215jsai-38237 */
/*----------------------------------------------------------------------------*/
intl AP_PADS_DEBUG = 0;                                   /* @17215jsai-38237 */

/*----------------------------------------------------------------------------*/
/* First Contact PMI Flag                                    @18801yjer-40681 */
/*----------------------------------------------------------------------------*/
intl FirstContactPMIFlag = 0;                             /* @18801yjer-40681 */

/*----------------------------------------------------------------------------*/
/* First Die Preheat Finished Flag                           @18c10yjer-41303 */
/*----------------------------------------------------------------------------*/
intl StartPreheatIsFinished = 0;                          /* @18c10yjer-41303 */

/*----------------------------------------------------------------------------*/
/* TSMC [NEW SETUP] Execute Mode Flag                        @18c10yjer-41303 */
/*----------------------------------------------------------------------------*/
intl TSMCNewSetupMode = 0;                                /* @18c10yjer-41303 */

/*----------------------------------------------------------------------------*/
/* Auto delete wafer file                                    @17403twa3-36286 */
/*----------------------------------------------------------------------------*/
intl MmiIntervalDelExe = 0;                               /* @17403twa3-36286 */
intl AUTODEL_DEBUG = 0;                                   /* @17403twa3-36286 */
intl AUTODEL_DELAY = 0;                                   /* @17403twa3-36286 */

/*----------------------------------------------------------------------------*/
/* Appli Interface Data.                                     @19225hara-40963 */
/*----------------------------------------------------------------------------*/
tyBiTcpAppli BiTcpAppli;                                  /* @19225hara-40963 */

/*------------------------------------------------------------------------------*/
/* Back LApplPNoPointer After Wafer Unload When Spec. Enable.  @19814hden-41686 */
/*------------------------------------------------------------------------------*/
intl LApplPNoPointerBack = 0;                               /* @19814hden-41686 */

/*----------------------------------------------------------------------------*/
/* Disable Ports.                                            @19a31hden-41773 */
/*----------------------------------------------------------------------------*/
unsl MaxDisPortCnt = 40;                                  /* @19a31hden-41773 */
unsb DisPortBitList[ MAXPORTARRAY ];                      /* @19a31hden-41773 */

/*--------------------------------------------------------------------------- */
/* Confirm EasyCool's state stable or activating Flag        @19916yjer-41153 */
/*--------------------------------------------------------------------------- */
intl EasyCoolStableFlag = 0;                              /* @19916yjer-41153 */
intl EasyCoolECOState = EZCL_ECO_BEF_WAIT;                /* @19916yjer-41153 */
/*----------------------------------------------------------------------------*/
/* Gpib "p3" command Flag                                    @20113cjoh-41953 */
/*           0 : init value                                  @20113cjoh-41953 */
/*           1 : "p3"   cmd                                  @20113cjoh-41953 */
/*           2 : "p3x"  cmd                                  @20113cjoh-41953 */
/*           3 : "p3xU" cmd                                  @20113cjoh-41953 */
/*----------------------------------------------------------------------------*/
intl    Gpibp3CmdF = 0;                                   /* @20113cjoh-41953 */
/*----------------------------------------------------------------------------*/
/* Remote Operation Disconnect Paramete                      @20807tish-42128 */
/*----------------------------------------------------------------------------*/
intl    RODisconnectTimerFlg = 0;                         /* @20807tish-42128 */
intl    RODisconnectMode = 0;                             /* @20807tish-42128 */
intl    RODisconnectTimer = 5;                            /* @20807tish-42128 */

/*----------------------------------------------------------------------------*/
/* Parameter List Debug Flag                                 @21308khos-42596 */
/*----------------------------------------------------------------------------*/
intl PARAM_LIST_DEBUG = 0;                                  /* @21308khos-42596 */

/*-----------------------------------------------------------@21119fmor-42375-*/
/* For WAPP Multi Point Flat Check.                          @21119fmor-42375 */
/*-----------------------------------------------------------@21119fmor-42375-*/
intl    MPointChkSWProbeZPosA[25];      /* Multi Point Flat Check WAPP Z Pos */  /* @21119fmor-42375 */
intl    MPointChkSWProbeZPosB[25];      /* Multi Point Flat Check WAPP Z Pos */  /* @21119fmor-42375 */
intl    MPointChkSWProbeStsA[25];       /* execute point Status WAPP(A)  */      /* @21119fmor-42375 */
intl    MPointChkSWProbeStsB[25];       /* execute point Status WAPPB    */      /* @21119fmor-42375 */
intl    WithBaseWappDetectZAdd = 0;     /* SWProbe Detect ZMAX add Data */       /* @21119fmor-42375 */

intl GetMPointCExeStatusData( intl, intl );                                      /* @21119fmor-42375 */
intl SetMPointCExeStatusData( intl, intl, intl );                                /* @21119fmor-42375 */
intl GetMPointCExeZPosData( intl, intl );                                        /* @21119fmor-42375 */
intl SetMPointCExeZPosData( intl, intl, intl );                                  /* @21119fmor-42375 */

/*------------------------------------------------------------------------*/     /* @21119fmor-42375 */

/*----------------------------------------------------------------------------*/
/* Cellcia GEM Conversion                                    @20306rsas-42006 */
/*----------------------------------------------------------------------------*/
intl CellReady2ZupF = 0;    /* 1:Wait Zup,      0:Zup Complete  @20306rsas-42006 */
intl CellTestEndF = 0;      /* 1:Wait End Test, 0:End Test Recv @20306rsas-42006 */
intl CellAS002TestEndF = 0; /* 1:Wait 1st Chip, 0:Next Chip     @20306rsas-42006 */

VOID    NECConsectiveLotLotRsvd( VOID )
{
    NextLotStart = LOT_RESERVED;
}
VOID    NECConsectiveLot1stWafLd( VOID )
{
    NextLotStart |= FSTWAF_LOADED;
}
VOID    NECConsectiveLot1stWafLkd( VOID )
{
    NextLotStart |= FSTWAF_LOCKED;
}
VOID    NECConsectiveLotPmtRcvd( VOID )
{
    NextLotStart |= PERMIT_RECEIVED;
	logMsg( "Start Permission Received!! 0x%x\n", NextLotStart );	/* @11314mshi-27534 */
    PreLotCancelState &= ~CANCEL_ENABLE;                            /* @11322jsai-27533 */
}
VOID    NECConsectiveLotWafChgSnt( VOID )
{
    NextLotStart |= WAFCHG_SENT;
}
VOID    NECConsectiveLotWafChgEnd( VOID )
{
    NextLotStart |= WAFCHG_END;
}
VOID    NECConsectiveLotWafChgRpt( VOID )
{
    NextLotStart |= WAFCHG_REPORT;
}
VOID    NECConsectiveLotWafChgRcvd( VOID )
{
    NextLotStart |= WAFCHG_RECEIVED;
}
VOID    NECConsectiveLotLotEndSeq( VOID )
{
    NextLotStart |= LOTEND_CONT;
}
VOID    NECConsectiveLotPalnSnt( VOID )
{
    NextLotStart |= PALN_SENT;
}
VOID    NECConsectiveLotPalnEnd( VOID )
{
    NextLotStart |= PALN_END;
}
intl    NECConsectiveLotGoNxtSeq( VOID )
{
    FAST    intl    ret = 0;
    if( NECConsectiveLot1stWaf() == 2 && ( NextLotStart & LOTEND_CONT ) && ( NextLotStart & PERMIT_RECEIVED ) == 0 )
	{
		ret = ERROR;
	}
    return( ret );
}
intl    NECConsectiveLotWaitWafChg( VOID )
{
    FAST    intl    ret = 0;
    if( NECConsectiveLot1stWaf() && ( NextLotStart & ( WAFCHG_SENT | WAFCHG_END ) ) == WAFCHG_SENT )
    {
        ret = ERROR;
    }
    return( ret );
}
intl    NECConsectiveLotWaitPalnEnd( VOID )
{
    FAST    intl    ret = 0;
    if( NECConsectiveLot1stWaf() && ( NextLotStart & ( WAFCHG_SENT | WAFCHG_END | PALN_SENT | PALN_END ) ) == ( WAFCHG_SENT | WAFCHG_END | PALN_SENT ) )
    {
        ret = ERROR;
    }
    return( ret );
}
/* @11121mshi-26334 */
intl    NECConsectiveLot1stWaf( VOID )
{
    FAST    intl    ret = 0;

    if( ChkThroughputContiNEC() != 0 )
    {
        if( NextLotStart & LOT_RESERVED )
        {
            ret++;
        	if( NextLotStart & FSTWAF_LOADED )
        	{
            	ret++;
        	}
        }
    }
    return( ret );
}

/* @11215jsai-26334 */
intl NECConsectiveLotStartWait( VOID )
{
    FAST    intl    ret = 0;

    if( ( NextLotStart & ( PRELOT_STARTWAIT | PERMIT_RECEIVED ) ) == PRELOT_STARTWAIT )
    {
        ret = ERROR;
    }
    return( ret );
}

/* @11209khir-26334 */
/*#=================================================================#*/
/*# Function : ChkPreLotNGStatus                                    #*/
/*# Summary  : Check pre-lot or cancel.                             #*/
/*# Argument : intl mode --- 1: Reserve NG 2: Cancel NG             #*/
/*#                          3: Reserve and Cancel NG               #*/
/*# Return   : intl OK : Possible    ERROR : Impossible             #*/
/*# Caution  : ChkThroughputContiNEC only use                       #*/
/*#=================================================================#*/
intl ChkPreLotNGStatus( unsl mode )
{
intl ret = OK;

#if( P12XL )    /* @11512khir-26334 */
    /* Reserve and Cancel NG */
    if( ( mode & CHK_PRELOT_NG ) != 0 )
    {
        /* Now reserve or cancel */
        /* Now end immediately */
        /* Pre-lot start recive */
        /* Now last wafer unload */
        if( ( ( PreLotCancelState & RESERVED_EXECUTE ) == RESERVED_EXECUTE )||
            ( ( NextLotStart != 0 )&&
              ( ( PreLotCancelState & CANCEL_EXECUTE ) != 0 ) )||
            ( ( Sys_mode & ( SYS_UNLDRPT | SYS_UNLD ) ) != 0 )||
            ( EndUnld == ERROR )||
            ( ( NextLotStart & PERMIT_RECEIVED ) == PERMIT_RECEIVED ) )
        {
            ret = ERROR;
        }
    }
    /* Reserve NG */
    if( ( mode & CHK_PRELOT_RES_NG ) == CHK_PRELOT_RES_NG )
    {
        /* Not send/recive lot start response */
        if( ( getNowNetWorkOnline() != NETWORK_SETTING_NOTHING )&&
            ( ( ComNetFStat & ( APNS_LOTSTART | APNR_LOTSTART ) ) != ( APNS_LOTSTART | APNR_LOTSTART ) ) )
        {
            ret = ERROR;
        }
    }
    /* Cancel NG */
    if( ( mode & CHK_PRELOT_CAN_NG ) == CHK_PRELOT_CAN_NG )
    {
        /* Continue lot on 1st wafer before not reserve pre-lot start */
        if( ( NECConsectiveLot1stWaf() == 2 )&&
            ( ( NextLotStart & PERMIT_RECEIVED ) == 0 )&&
            ( ( InitWaferF & ( INIT_WAFER | CAST_1ST_WAFER ) ) == ( INIT_WAFER | CAST_1ST_WAFER ) ) )
        {
            ret = ERROR;
        }
    }

    if( ret == ERROR )                                             /* @11315khir-27534 */
    {                                                              /* @11315khir-27534 */
        logMsg( "Lot Reserve or Cancel NG!\n" );                   /* @11315khir-27534 */
        logMsg( "PreLotCancelState[%d]\n", PreLotCancelState );    /* @11315khir-27534 */
        logMsg( "NextLotStart[%d]\n", NextLotStart );              /* @11315khir-27534 */
        logMsg( "Sys_mode[%d]\n", Sys_mode );                      /* @11315khir-27534 */
        logMsg( "EndUnld[%d]\n", EndUnld );                        /* @11315khir-27534 */
        logMsg( "ComNetFStat[%d]\n", ComNetFStat );                /* @11315khir-27534 */
        logMsg( "InitWaferF[%d]\n", InitWaferF );                  /* @11315khir-27534 */
    }                                                              /* @11315khir-27534 */
#endif    /* @11512khir-26334 */
    return( ret );
}

/* @11315khir-27533 */
/*#---------------------------------------------------------------------#*/
/*#    Function    : ChkTptCntNECSuspendFLE                             #*/
/*#    Summary     : Check SuspendF=1 Lot end.                          #*/
/*#    Argument    : VOID                                               #*/
/*#    Return      : 0: None Pre lot cancel  1: Pre lot cancel          #*/
/*#---------------------------------------------------------------------#*/
intl ChkTptCntNECSuspendFLE( VOID )
{
intl ret = 0;

    if( ( ChkThroughputContiNEC() != 0 )&&
        ( SuspendF == 1 )&&( NextLotStart != 0 ) )
    {
        ret = 1;
    }
    return( ret );
}

/* @11b17msai-28477 */
/*#---------------------------------------------------------------------#*/
/*#    Function    : SetComNetFStat4                                    #*/
/*#    Summary     : Set ComNetFStat4.                                  #*/
/*#    Argument    : intl( Set Bit )                                    #*/
/*#    Return      : void                                               #*/
/*#---------------------------------------------------------------------#*/
void SetComNetFStat4( intl val )
{
    ComNetFStat4 |= val;
}

/* @11b17msai-28477 */
/*#---------------------------------------------------------------------#*/
/*#    Function    : GetComNetFStat4                                    #*/
/*#    Summary     : Get ComNetFStat4.                                  #*/
/*#    Argument    : void                                               #*/
/*#    Return      : intl( ComNetFStat4 )                               #*/
/*#---------------------------------------------------------------------#*/
intl GetComNetFStat4( void )
{
    return( ComNetFStat4 );
}

/* @11b17suem-28477 */
/*#---------------------------------------------------------------------#*/
/*#    Function    : ClrComNetFStat4                                    #*/
/*#    Summary     : Clear ComNetFStat4.                                #*/
/*#    Argument    : opeStatFlag                                        #*/
/*#    Return      : void                                               #*/
/*#---------------------------------------------------------------------#*/
void ClrComNetFStat4( unsl opStatF )
{

    switch( opStatF )
    {
        case OPTSTATF_POLISH_CHG_START:
            ComNetFStat4 &= ~APNS_CLNSHEETMNG_START;
            break;

        case OPTSTATF_POLISH_CHG_SHEETS:
            ComNetFStat4 &= ~APNS_CLNSHEETMNG_SHEETS;
            break;

        case OPTSTATF_POLISH_CHG_END:
            ComNetFStat4 &= ~APNS_CLNSHEETMNG_END;
            break;

        default:
            break;
    }

    return;
}

#if( PADSOPT )                                            /* @12824jsai-29459 */
/* @12824jsai-29459 */
/*#--------------------------------------------------------------------------#*/
/*#        Function    : checkPADSPTPOExeDieAd()                             #*/
/*#        Summary     : Check if XYAd.chipAd is same as the die where PTPO  #*/
/*#                    : Imaging before contact has been executed on or not. #*/
/*#        Argument    : None                                                #*/
/*#        Return      : OK | ERROR                                          #*/
/*#        Caution     :                                                     #*/
/*#--------------------------------------------------------------------------#*/
STATUS  checkPADSPTPOExeDieAd( void )
{
    if(( XYAd.chipAd[X_AD] == PADSPTPOExeDieAd[X_AD] ) && ( XYAd.chipAd[Y_AD] == PADSPTPOExeDieAd[Y_AD] ))
    {
        return( OK );
    }

    return( ERROR );
}
#endif

/* @14115msai-31693 */
/*#---------------------------------------------------------------------#*/
/*#    Function    : SetManualTestFlg                                   #*/
/*#    Summary     : Set ManualTestFlg.                                 #*/
/*#    Argument    : intl( Set val )                                    #*/
/*#    Return      : void                                               #*/
/*#---------------------------------------------------------------------#*/
void SetManualTestFlg( intl val )
{
    ManualTestFlg = val;
}

/* @14115msai-31693 */
/*#---------------------------------------------------------------------#*/
/*#    Function    : GetManualTestFlg                                   #*/
/*#    Summary     : Get ManualTestFlg.                                 #*/
/*#    Argument    : void                                               #*/
/*#    Return      : intl( ManualTestFlg )                              #*/
/*#---------------------------------------------------------------------#*/
intl GetManualTestFlg( void )
{
    return( ManualTestFlg );
}

/* @17227sna2-38710 */
/*#---------------------------------------------------------------------#*/
/*#    Function    : ChkICPStatusCHIPPOL                                #*/
/*#    Summary     : 校正プレート前の針研かをチェック                   #*/
/*#    Argument    : void                                               #*/
/*#    Return      : 1:OK, 0:NG                                         #*/
/*#---------------------------------------------------------------------#*/
intl ChkICPStatusCHIPPOL( void )
{
    intl ret = 0;

    if(( ICPStatus & ICTRAY_CHIP )&&
       ( ICPStatus & ICTRAY_POL ))
    {
        ret = 1;
    }
    return( ret );
}

#if( P12XL )                                              /* @14321jsai-31962 */
/* @14321jsai-31962 */
/*#--------------------------------------------------------------------------#*/
/*#        Function    : CntMngDataCountUp()                                 #*/
/*#        Summary     : 特性プローブカード管理機能のコンタクト回数を        #*/
/*#　　　  　　　　　　　インクリメントします                                #*/
/*#        Argument    : None                                                #*/
/*#        Return      : None                                                #*/
/*#        Caution     :                                                     #*/
/*#--------------------------------------------------------------------------#*/
void  CntMngDataCountUp( void )
{
    tyCntMngData *ptr = &CntMngData;

    if( ContactManageSpecExe() == OK )
    {
        ptr->times++;
    }
}
#endif                                                    /* @14321jsai-31962 */

#if( !WDFDP )                                               /* @16315sooy-33345 */
/* @16315sooy-33345 */
/*#============================================================================#*/
/*#     Function    :   SetSWProbeAirBlowParam                                 #*/
/*#     Summary     :   Set SW Probe Air Blow Parameters.                      #*/
/*#     Argument    :   void                                                   #*/
/*#     Return      :   void                                                   #*/
/*#     Caution     :   Norn                                                   #*/
/*#============================================================================#*/
void SetSWProbeAirBlowParam( void )
{
    intl rtn = 0;
    intl inputSeqNo = 0;
    intl inputData = 0;
    intb charDt;
    intl tmpExeTemp = SWPAirBlowExeTemp;
    unsl tmpOnTimer = SWPAirBlowOnTimer;
    unsl tmpOffTimer = SWPAirBlowOffTimer;
    unsb tmpEndCmd[8];

    while( rtn == 0 )
    {
        printf( "\n==================================================\n" );
        printf( " Current Value\n" );
        printf( "   SW Probe Air Blow Execution Temperature < %d >\n", tmpExeTemp );
        printf( "   SW Probe Air Blow On Timer              < %d >\n", tmpOnTimer );
        printf( "   SW Probe Air Blow Off Timer             < %d >\n", tmpOffTimer );
        printf( "\n" );
        printf( " Please Select A Process\n" );
        printf( "   Input Execution Temperature :   0\n" );
        printf( "   Input On Timer              :   1\n" );
        printf( "   Input Off Timer             :   2\n" );
        printf( "   Exti Process                :   3\n" );
        printf( ">> Input Number (Cancel:  99)  >>>>> " );

        inputSeqNo = 0;

        /* Get Input */
        scanf( "%d", &inputSeqNo );

        /* 標準入力の残りがあれば読み捨て */
        while( ( ( charDt = getchar() ) != '\n' ) && ( charDt != EOF ) );

        if( inputSeqNo == 99 )
        {
            /* Exit From Loop */
            break;
        }

        switch( inputSeqNo )
        {
            case 0:
            case 1:
            case 2:
                /* Input Data */
                FOREVER
                {
                    if( inputSeqNo == 0 )
                    {
                        printf( "Please input SW Probe Air Blow Execution Temperature.\n" );
                    }
                    else if( inputSeqNo == 1 )
                    {
                        printf( "Please input SW Probe Air Blow On Timer.\n" );
                    }
                    else
                    {
                        printf( "Please input SW Probe Air Blow Off Timer.\n" );
                    }
                    printf( ">> Input Data( Exit: 99 )  >>>>> " );

                    /* Get Input Data */
                    scanf( "%d", &inputData );

                    /* 標準入力の残りがあれば読み捨て */
                    while( ( ( charDt = getchar() ) != '\n' ) && ( charDt != EOF ) );

                    if( inputData == 99 )
                    {
                        /* Exit */
                        break;
                    }

                    if( inputSeqNo == 0 )
                    {
                        tmpExeTemp = inputData;
                    }
                    else if( inputSeqNo == 1 )
                    {
                        tmpOnTimer = inputData;
                    }
                    else
                    {
                        tmpOffTimer = inputData;
                    }
                    break;
                }
                break;

            case 3:
                /* Exit */
                printf( "\nThe end of this function. Do you want to SAVE or EXIT?( y or n )\n");
                printf(">> INPUT >>>>> ") ;

                /* Get Input Strings */
                scanf( "%7s", &tmpEndCmd[0] );

                /* 標準入力の残りがあれば読み捨て */
                while( ( ( charDt = getchar() ) != '\n' ) && ( charDt != EOF ) );

                if( ( tmpEndCmd[0] == 'y' ) || ( tmpEndCmd[0] == 'Y' ) )
                {
                    /* Check whather parameter is changed or not */
                    if( tmpExeTemp != SWPAirBlowExeTemp )
                    {
                        printf( "Parameter Changed Execution Temperature.\n" );
                        SWPAirBlowExeTemp = tmpExeTemp;
                    }

                    if( tmpOnTimer != SWPAirBlowOnTimer )
                    {
                        printf( "Parameter Changed On Timer.\n" );
                        SWPAirBlowOnTimer = tmpOnTimer;
                    }

                    if( tmpOffTimer != SWPAirBlowOffTimer )
                    {
                        printf( "Parameter Changed Off Timer.\n" );
                        SWPAirBlowOffTimer = tmpOffTimer;
                    }
                    rtn = 1;
                }
                else if( ( tmpEndCmd[0] == 'n' ) || ( tmpEndCmd[0] == 'N' ) )
                {
                    rtn = 1;
                }
                break;

            default:
                printf("Illegal input process number. Please re-enter.\n");
                break;
        }
    }
}
#endif                                                      /* @16315sooy-33345 */

/* Added Function : GetDisPortFromFile                  @19a31hden-41773 */
/*#---------------------------------------------------------------------#*/
/*#    Function    : GetDisPortFromFile                                 #*/
/*#    Summary     : Get Disable Port From File.                        #*/
/*#    Argument    : unsb* strbuff : Port No. String.( output )         #*/
/*#    Return      : OK / ERROR                                         #*/
/*#---------------------------------------------------------------------#*/
intl GetDisPortFromFile( unsb* strbuff )
{
    intl retVal = OK;
    FILE *fp = NULL;
    unsb tempStr[ PORT_DATA_MAX_LEN ] = "";
    
    memset( DisPortBitList, 0, MAXPORTARRAY );
    if( ( fp = TyFopen( DISABLEPORT_FILE_PATH, "r", 1  ) ) != NULL )
    {
        memset( tempStr, 0, PORT_DATA_MAX_LEN );
        fgets( tempStr, PORT_DATA_MAX_LEN, fp );
        if( strlen( tempStr ) > 0 )
        {
            /* Check CRLF In tempStr */
            if(     ( strlen( tempStr ) > 1 )
                &&  ( ( tempStr[ strlen( tempStr ) - 2 ] ) == 0x0d )
              )
            {
                tempStr[ strlen( tempStr ) - 2 ] = '\0';
            }
            else if( ( tempStr[ strlen( tempStr ) - 1 ] ) == 0x0a )
            {
                tempStr[ strlen( tempStr ) - 1 ] = '\0';
            }
            sprintf( strbuff, "%s", tempStr );
            /* Trans string to bit array. */
            retVal = ConvDisPortStrBitArray( &DisPortBitList[0], strbuff, 1 );
        }
        fclose( fp );
    }
    else
    {
        logMsg( "GetDisPortFromFile() : File is not exist, %s\n", DISABLEPORT_FILE_PATH );
        retVal = ERROR;
    }
    
    return retVal;
} /* End of GetDisPortFromFile */

/* Added Function : SetDisPortToFile                    @19a31hden-41773 */
/*#---------------------------------------------------------------------#*/
/*#    Function    : SetDisPortToFile                                   #*/
/*#    Summary     : Set Disable Port To File.                          #*/
/*#    Argument    : unsb* strbuff : Port No. String.( input )          #*/
/*#    Return      : OK / ERROR                                         #*/
/*#---------------------------------------------------------------------#*/
intl SetDisPortToFile( unsb* strbuff )
{
    intl retVal = OK;
    FILE *fp = NULL;
    
    /* Make dir for disable port .def */
    retVal = CheckandMakeDir( DISABLEPOR_DIR );
    if( retVal == OK )
    {
        if( ( fp = TyFopen( DISABLEPORT_FILE_PATH, "w", 1  ) ) != NULL )
        {
            fputs( strbuff, fp );
            fclose( fp );
        }
        else
        {
            logMsg( "SetDisPortToFile() : Open File ERROR, %s\n", DISABLEPORT_FILE_PATH );
            retVal = ERROR;
        }
    }
    
    return retVal;
} /* End of SetDisPortToFile */

/* Added Function : IsPortNumberStringBase          @19a31hden-41773 */
/*#-----------------------------------------------------------------#*/
/*#   Function    : IsPortNumberStringBase                          #*/
/*#   Summary     : Interpretes given string, and judges whether    #*/
/*#                 it is string showing a port number.             #*/
/*#   Argument    : unsb *inStr  - string to read                   #*/
/*#   Return      : intl TRUE  - string is port number.             #*/
/*#                      FALSE - string isn't port number.          #*/
/*#   Caution     :                                                 #*/
/*#-----------------------------------------------------------------#*/
intl IsPortNumberStringBase( unsb *inStr, unsl *portNum )
{
    intl retVal = TRUE;
    unsl length = 0;
    unsl cnt = 0;
    unsb tmp[ 6 ];
    unsl maxNum = FTP_PORT_MAX;       /* Max ftp port number */
    unsl minNum = FTP_PORT_MIN;       /* Min ftp port number */
    
    length = strlen( inStr );
    if( length != 4 )
    {
        retVal = FALSE;
        goto FEND;
    }
    
    memset( tmp, 0, sizeof( tmp ) );
    strncpy( tmp, inStr, sizeof( tmp ) -1 );
    length = strlen( tmp );
    
    for( cnt = 0; cnt < length; cnt++ )
    {
        if( !isdigit( tmp[cnt] ) )
        {
            retVal = FALSE;
            goto FEND;
        }
    }
    
    *portNum = atol( tmp );
    
    if(    ( *portNum < minNum )
        || ( *portNum > maxNum )
      )
    {
        retVal = FALSE;
    }
FEND:
    return retVal;
} /* End of IsPortNumberStringBase */

/* Added Function : IsPluralityPortNumberStringBase @19a31hden-41773 */
/*#-----------------------------------------------------------------#*/
/*#   Function    : IsPluralityPortNumberStringBase                 #*/
/*#   Summary     : Interpretes given string, and judges whether    #*/
/*#                 it is string showing two or more port.          #*/
/*#   Argument    : unsb *inStr  - string to read                   #*/
/*#                 unsl *portFrom  - start of port when check ok.  #*/
/*#                 unsl *portTo  - end of port when check ok.      #*/
/*#   Return      : intl TRUE  - string is two or more .            #*/
/*#                      FALSE - string isn't two or more .         #*/
/*#   Caution     :                                                 #*/
/*#-----------------------------------------------------------------#*/
intl IsPluralityPortNumberStringBase( unsb *inStr, unsl *portFrom, unsl *portTo )
{
    intl retVal = TRUE;
    unsl length = 0;
    unsl cnt = 0;
    unsb tmp[ 11 ];
    
    length = strlen( inStr );
    if( length != 9 )
    {
        retVal = FALSE;
        goto FEND;
    }
    
    memset( tmp, 0, sizeof( tmp ) );
    strncpy( tmp, inStr, sizeof( tmp ) -1 );
    
    length = strlen( tmp );
    
    for( cnt = 0; cnt < length; cnt++ )
    {
        if( tmp[cnt] == '-' )
        {
            break;
        }
    }
    
    if( cnt != 4 )
    {
        retVal = FALSE;
    }
    else
    {
        tmp[cnt] = '\0';
        /* Check Format For Start Port Number And End Port Number */
        if(    ( IsPortNumberStringBase( tmp, portFrom ) == TRUE )
            && ( IsPortNumberStringBase( &tmp[ cnt + 1 ], portTo ) == TRUE )
          )
        {
            if( *portFrom > *portTo )
            {
                retVal = FALSE;
            }
        }
        else
        {
            retVal = FALSE;
        }
    }
    
FEND:
    return retVal;
} /* End of IsPluralityPortNumberStringBase */

/* Added Function : ConvDisPortStrBitArray                   @19a31hden-41773 */
/*#--------------------------------------------------------------------------#*/
/*#    Function    : ConvDisPortStrBitArray                                  #*/
/*#    Summary     : Converter Between Disable Port String And Bit Array.    #*/
/*#    Argument    : unsb *portBitArray : Port Bit Array( input and output ) #*/
/*#                  unsb *portStr : Port String( input and output )         #*/
/*#                  intl mode : 0 : Trans bit array to string.              #*/
/*#                              1 : Trans string to bit array.              #*/
/*#    Return      : OK / ERROR ( mode == 1 )                                #*/
/*#                  Port Count ( mode == 0 )                                #*/
/*#--------------------------------------------------------------------------#*/
intl ConvDisPortStrBitArray( unsb *portBitArray, unsb *portStr, intl mode )
{
    intl retVal = OK;
    unsl lenCheck = 0;
    unsl length = 0;
    unsl cnt = 0;
    unsl indexH = 0;
    unsl indexE = 0;
    unsb tmp[ 10 ] = "";
    unsl minNum = FTP_PORT_MIN;       /* Min port number */
    unsl portNum = 0;
    unsl portFrom = 0;
    unsl portTo = 0;
    unsb portBitTable[ MAXPORTARRAY ];
    unsl arrayIndex = 0;
    unsb bitIndex = 0;
    intb workData[ PORT_DATA_MAX_LEN ] = "";
    
    memset( portBitTable, 0, sizeof( portBitTable ) );
    
    /* Bit array to string, need to shift */
    if( mode == 0 )
    {
        memcpy( portBitTable, portBitArray, sizeof( portBitTable ) );
        /* Intiall data */
        memset( tmp, 0, sizeof( tmp ) );
        memset( workData, 0, sizeof( workData ) );
        portNum = portFrom = portTo = 0;
        /* Will return port count */
        retVal = 0;
        /* Update port setting and count, need to shif */
        for( arrayIndex = 0; arrayIndex < MAXPORTARRAY; arrayIndex++ )
        {
            for( bitIndex = 0; bitIndex < 8; bitIndex++ )
            {
                if( ( portBitTable[ arrayIndex ] ) & ( 1 << bitIndex ) )
                {
                    retVal++;
                    portNum = minNum + arrayIndex * 8 + bitIndex;
                    if( portFrom == 0 )
                    {
                        portFrom = portNum;
                    }
                    else
                    {
                        portTo = portNum;
                    }
                }
                else
                {
                    if( portFrom != 0 )
                    {
                        ltoa( portFrom, tmp, 10 );
                        strncat( workData, tmp, sizeof( workData ) - strlen( workData ) - 1 );
                        if( portTo != 0 )
                        {
                            strncat( workData, "-", sizeof( workData ) - strlen( workData ) - 1 );
                            ltoa( portTo, tmp, 10 );
                            strncat( workData, tmp, sizeof( workData ) - strlen( workData ) - 1 );
                        }
                        strncat( workData, ",", sizeof( workData ) - strlen( workData ) - 1 );
                        portFrom = portTo = 0;
                    }
                }
            }
        }
        
        if( portFrom != 0 )
        {
            ltoa( portFrom, tmp, 10 );
            strncat( workData, tmp, sizeof( workData ) - strlen( workData ) - 1 );
            if( portTo != 0 )
            {
                strncat( workData, "-", sizeof( workData ) - strlen( workData ) - 1 );
                ltoa( portTo, tmp, 10 );
                strncat( workData, tmp, sizeof( workData ) - strlen( workData ) - 1 );
            }
            strncat( workData, ",", sizeof( workData ) - strlen( workData ) - 1 );
            portFrom = portTo = 0;
        }
        
        if( strlen( workData ) > 0 )
        {
            /* Check last word ',' -> '\0' */
            if( workData[ strlen( workData ) - 1 ] == ',' )
            {
                workData[ strlen( workData ) - 1 ] = '\0';
            }
        }
        /* Update port setting */
        sprintf( portStr, "%s", workData );
    }
    /* String to bit array, need to shift */
    else
    {
        /* Will return OK/ERROR */
        retVal = OK;
        lenCheck = strlen( portStr );
        if( lenCheck > 0 )
        {
            /* Need to check until'\0', so + 1 */
            lenCheck++;
        }
        for( indexH = cnt = 0; cnt < lenCheck; cnt++ )
        {
            if(    ( portStr[cnt] == ',' )
                || ( portStr[cnt] == '\0' )
              )
            {
                /* Intiall data */
                memset( tmp, 0, sizeof( tmp ) );
                portFrom = 0;
                portTo = 0;
                indexE = cnt;
                length = indexE - indexH;
                if( length > 9 )
                {
                    retVal = ERROR;
                    break;
                }
                /* Catch string between ',' and ',' */
                strncpy( tmp, ( portStr + indexH ), length );
                /* Check Format */
                if( IsPortNumberStringBase( tmp, &portNum ) == TRUE )
                {
                    /* Singel port */
                    arrayIndex = ( portNum - minNum ) / 8;
                    bitIndex = ( portNum - minNum ) % 8;
                    portBitTable[ arrayIndex ] |= ( 1 << bitIndex );
                }
                else if( IsPluralityPortNumberStringBase( tmp, &portFrom, &portTo ) == TRUE )
                {
                    /* xxxx-yyyy */
                    for( portNum = portFrom; portNum <= portTo; portNum++ )
                    {
                        arrayIndex = ( portNum - minNum ) / 8;
                        bitIndex = ( portNum - minNum ) % 8;
                        portBitTable[ arrayIndex ] |= ( 1 << bitIndex );
                    }
                }
                else
                {
                    retVal = ERROR;
                    break;
                }
                indexH = cnt + 1;
            }
        }
        memcpy( portBitArray, portBitTable, MAXPORTARRAY );
    }
    
    return retVal;
} /* End of ConvDisPortStrBitArray */

/* @21119fmor-42375 */
/*===========================================================================
    Function    :   intl GetMPointCExeStatusData
    Summary     :   MPOINTの実行データ取得
    Argument    :   num   : Exec point(0-24)
                :   area  : 0(AREA_A)  1(AREA_B)
    Return      :   Point Status 0(Non Exec)  1(Exec) other(NG)
    Caution     :   must sts setting
==========================================================================#*/
intl GetMPointCExeStatusData( intl num, intl area )
{
    intl sts = 0;

    if( area == AREA_B )
    {
        sts = MPointChkSWProbeStsB[ num ];
    }
    else
    {
        sts = MPointChkSWProbeStsA[ num ];
    }
    return( sts );
}

/* @21119fmor-42375 */
/*===========================================================================
    Function    :   intl SetMPointCExeStatusData
    Summary     :   MPOINTの実行データセット
    Argument    :   num  : Exec point(0-24)
                :   area : 0(AREA_A)  1(AREA_B)
                :   val  : set data
    Return      :   OK
    Caution     :   must sts setting
==========================================================================#*/
intl SetMPointCExeStatusData( intl num, intl area, intl val )
{
    intl ret = 0;

    if( area == AREA_B )
    {
        MPointChkSWProbeStsB[ num ] = val;
    }
    else
    {
        MPointChkSWProbeStsA[ num ] = val;
    }
    return( ret );
}

/* @21119fmor-42375 */
/*===========================================================================
    Function    :   intl GetMPointCExeZPosData
    Summary     :   MPOINTのZPOSデータ取得
    Argument    :   num   : Exec point(0-24)
                :   area  : 0(AREA_A)  1(AREA_B)
    Return      :   data(Zpos)
    Caution     :
==========================================================================#*/
intl GetMPointCExeZPosData( intl num, intl area )
{
    intl pos = 0;

    if( area == AREA_B )
    {
        pos = MPointChkSWProbeZPosB[ num ];
    }
    else
    {
        pos = MPointChkSWProbeZPosA[ num ];
    }
    return( pos );
}

/* @21119fmor-42375 */
/*===========================================================================
    Function    :   intl SetMPointCExeZPosData
    Summary     :   MPOINTのZPOSデータセット
    Argument    :   num  : Exec point(0-24)
                :   area : 0(AREA_A)  1(AREA_B)
                :   val  : set data
    Return      :   OK
    Caution     :
==========================================================================#*/
intl SetMPointCExeZPosData( intl num, intl area, intl val )
{
    intl ret = 0;

    if( area == AREA_B )
    {
        MPointChkSWProbeZPosB[ num ] = val;
    }
    else
    {
        MPointChkSWProbeZPosA[ num ] = val;
    }
    return( ret );
}

/*
 * End of File
 */
